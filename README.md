# Projecttracker

A managing and project tracking web application.

Aplicación web de gestión y seguimiento de proyectos

## Requirements (Requisitos)

Projecttracker works with PHP >5.6 and MySQL > 5.0.

Projecttracker funciona con PHP >5.6 y MySQL > 5.0.

  * PHP 5.6.23
  * MySQL 5.0

## Documentation (Documentación)

You will find the install documentation [here](./doc/install.md).

You will find all the documentation [here](./doc/README.md).

You will find lots of examples [here](./examples/).

## Donate (Donaciones)

..

## Change log (Registro de cambios)

See the [./CHANGELOG.md](./CHANGELOG.md) file.

Ver el archivo [./CHANGELOG.md](./CHANGELOG.md).

## Help & Support (Ayuda y soporte)

For questions and bug reports, please use the GitHub/Bitbucket issues page.

Para realizar preguntas e informar de errores, por favor utiliza la sección de cuestiones/problemas de Github/Bitbucket.

## License (Licencia)

This program is distributed under the GPL License. For more information see the [./LICENSE.md](./LICENSE.md) file.

El código se distribuye bajo la Licencia GPL. Para más información ve el archivo [./LICENSE.md](./LICENSE.md).

Copyright 2008-2020 by Eriz Villalba

