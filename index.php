<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
// error_reporting(0);

    // Con el autoloader cargamos las librerias/modulos(.php) que necesitaremos para cargar la app completamente
    include_once("application/libs/autoloader.php");
    $modulos = array("bootstrap", "log", "controller", "model", "view", "database", "session", "paths", "mensajes", "utils", "email", "validator");
    		
    
    $autoloader = new Autoloader($modulos);
    Session::init();
    $app = new Bootstrap();
    
?>

