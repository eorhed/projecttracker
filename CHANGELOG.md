#Change log (Registro de cambios)

All notable changes to this project will be documented in this file.

Todos los cambios importantes de este proyecto serán documentados en este fichero.

## [unreleased]

## [Projecttracker v0.6.2 (2020-10-09)]
### Changed
    - Cambios menores en el código para mejorar la apariencia
    - Cambios menores en la hoja de estilos para mejorar la apariencia
    - Refactorizacion para escapar variables de salida a la hora de imprimir por pantalla
    - Cambiado ligeramente formato cabeceras del archivo Changelog siguiendo Codacy
    - Arreglada función enviar del controlador Contacto
    - Arreglada vista contacto, ahora renderiza los errores validación
    - Ahora la validación de formulario contacto la realiza la clase Validator

### Security
    - Todas las variables mostradas aparecen escapadas usando la función filter_var para escapar al imprimir variables

## [Projecttracker v0.6.1 (2020-09-20)][0.6.1]
### Added
    - Clase Validator que validará los campos de los formularios de la app
    - Clase State que contiene las constantes sobre el resultado de una operación en la BD
    - Validacion en agregar cliente usando la nueva clase Validator

### Changed
    - Sustitución de los codigos de estado de operacion por las constantes de estado de la clase State
    - Mensajes de notificación al usuario ahora va en header en vez de en cada vista
    - Formulario de agregar_cliente movido desde vista cliente a vista independiente
    - Cambios menores en reglas CSS en la hoja de estilos

### Security
    - Arreglos menores en guardar_sesion_tarea.php siguiendo Codacy

## [Projecttracker v0.6.0 (2020-09-15)][0.6.0]
### Added
    - Funcionalidad de enviar email de contacto al admin
    - Funcionalidad de actualizar preferencias de usuario
    - Documentación de funciones
    - Nuevos mensajes predefinidos (mensajes.php)
    - Añadida funcionalidad actualizarTiempoMaxSesion (funciones.js)
    - Nuevas reglas (contacto) en hoja de estilos
    - Notificaciones en nuevo fichero aparte a importar (notificaciones.php)

### Changed
    - Cambio estilo visual de la app
    - Simplificación de código en hoja de estilos css
    - Refactorización en función actualizarPrecioHora (funciones.js)

### Removed
    - Menú de la barra lateral (sidebar.php)

## [Projecttracker v0.5.3 (2020-09-10)][0.5.3]
### Added
    - Validación en la configuracion de parametros de instalación automática de la aplicación
    - Clase Util para funciones utiles como redirigir a una pagina, escapar variables a mostrar, etc.
    - Documentación funciones
    - Nuevos mensajes como constantes del archivo mensajes.php
    
### Changed
    - Algunos cambios menores en la hoja de estilos
    - Arreglado eliminar cliente añadiendo validacion de error de operacion cuando no se le pasa un idcliente

### Fixed
    - Quitado mensaje de error al listar clientes, mensaje de error cuando no hay parametros POST

### Removed
    - Clase Navigator. Su función de redirigir se ha movido a la clase Util

### Security
    - Todas las variables mostradas aparecen escapadas usando la función htmlspecialchars para tapar vulnerabilidades

## [Projecttracker v0.5.2 (2020-09-08)][0.5.2]
### Changed
    - Arreglo de errores (seguridad y escritura) y refactorización siguiendo recomendaciones de Codacy
    - Arreglos en el archivo Changelog para seguir recomendaciones de Codacy
    - Algunos cambios menores en la hoja de estilos

## [Projecttracker v0.5.1 (2020-08-19)][0.5.1]
### Added
    - Añadida columna coste por hora en la vista Sesiones
    - Nuevos mensajes de información en el archivo mensajes.php de carpeta config y vista trackear
    - Algunos cambios menores en la hoja de estilos

### Changed
    - Retoques en CSS
    - Mejorada escritura de código siguiendo Codacy
    - [config/mensajes] Cambiado contenido de algun mensaje
    - Arreglada vista de distribución de sesiones en Ver informe

### Fixed
    - Correcciones de seguridad segun Codacy (escapado en salida de variables y evitar uso de echo)
    - Correccion de errores menores

## [Projecttracker v0.5.0 (2020-08-06)][0.5.0]
### Added
    - Retoques en CSS
    - Añadidos nuevas funcionalidades (contadores de estadisticas) en Dashboard
    - Arreglada funcion eliminarProyecto, ahora lo elimina totalmente de la BD
    - [proyectos_model] Arreglada función asignarTareasAlProyecto

### Changed
    - Cambio formato changelog siguiendo guia estilo de keepachangelog.com
    - Mejoras en escritura de código siguiendo Codacy

### Fixed
    - Correcciones de seguridad segun Codacy (escapado en salida de variables y evitar uso de echo)

### Removed

## [Projecttracker v0.4.1 (2020-08-05)][0.4.1]
### Changed
    - Mejoras en escritura de código siguiendo Codacy
    - Retoques en CSS

### Fixed
    - Errores de escritura en alguna funcion

## [Projecttracker v0.4.0 (2020-05-26)][0.4.0]
### Added
    - README.md
    - CHANGELOG.md

### Changed
    - Refactorización a POO (clase MiDate) y cambio de nombre a func_fechas.php
    - Transformación visual completa del tema a una versión más moderna

## [Projecttracker v0.3.2 (2018-02-08)][0.3.2]
### Changed
    - [registrar_model] Al registrar nuevo usuario creamos un cliente con el mismo usuario para proyectos personales.

## [Projecttracker v0.3.1 (2020-02-07)][0.3.1]
### Changed
    - Arreglado al crear un nuevo proyecto salía un mensaje de error. 
    - El controlador(de proyectos) no capturaba bien el codigo de estado resultado de crear proyecto en la BD. 

## [Projecttracker v0.3.0 (2020-02-06)][0.3.0]
### Changed
    - Arreglado campo habilitado en tareas en views/tareas/index
    - Arreglado guardar sesión en agregar registro manualmente en public/php/guardar_sesion_tarea
    - Arreglado el guardar sesión en seguimiento automatico, no guardaba bien debido a mala validacion en public/js/funciones.js

[unreleased]: https://trello.com/b/H6Z7w9br/projecttracker
[0.6.1]: https://bitbucket.org/eorhed/projecttracker/commits/68f2f480a76da85f460c0d766d91d3e2a6733de5
[0.6.0]: https://bitbucket.org/eorhed/projecttracker/commits/8f50e712f78fb8bde30d458ebca9b71a219aa36d
[0.5.3]: https://bitbucket.org/eorhed/projecttracker/commits/9c9a79d3e20e6d6a1ea8ebf936d612c52bd62062
[0.5.2]: https://bitbucket.org/eorhed/projecttracker/commits/5de6bc80b4c57e2041baf65a41da0dad5d092e5a
[0.5.1]: https://bitbucket.org/eorhed/projecttracker/commits/c0c538167c33e998281271d5ba952dd65829a821
[0.5.0]: https://bitbucket.org/eorhed/projecttracker/commits/9661acd67ad4b6d635457559dde86fb747209a22
[0.4.1]: https://bitbucket.org/eorhed/projecttracker/commits/127f81a131f3dc5ee01af3aefd5eaf499400ec3d
[0.4.0]: https://bitbucket.org/eorhed/projecttracker/commits/3c06f160471f607f8032f009653fc33162c43d01
[0.3.2]: https://bitbucket.org/eorhed/projecttracker/commits/4ab561cc86e1d94ffe52b2ae73e9d487ddc77a4e
[0.3.1]: https://bitbucket.org/eorhed/projecttracker/commits/c5fcb2e0aa980247be17e35a5f503c8ab643c1f5
[0.3.0]: https://bitbucket.org/eorhed/projecttracker/commits/c5fcb2e0aa980247be17e35a5f503c8ab643c1f5