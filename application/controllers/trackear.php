<?php 
class Trackear extends Controller
{
	function __construct()
	{
		parent::__construct();	
		if (!Session::isLogged())
			Utils::redirect(BASE_URL . "identificarse/");
		else if (!Session::manageSession())
			Utils::redirect(BASE_URL . "identificarse/sesion_finalizada");	

		$this->view->pagina = titulo_pagina_trackear;
	}
	
	function index()
	{
		$this->view->proyectos = $this->model->getProyectosDelUsuario();

		if (isset($this->view->proyectos) && $this->view->proyectos == -1)
			$this->msj_error = msj_error_operacion;

		$this->view->render("trackear/index");
	}
}
?>