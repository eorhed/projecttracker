<?php
class Registrar extends Controller
{
	function __construct()
	{
		parent::__construct();

		$this->view->pagina = titulo_pagina_registrarse;
	}

	function index()
	{
		$this->view->render("registrar/index");
	}

	/**
	 * Registra (inserta) un nuevo usuario en la tabla usuarios de la BD
	 *
	 * @return int $codigo_estado
	 */
	public function run()
	{
		// Si existen usuario y clave llamamos al modelo de registrar para registrarlo en la BD
		if (!empty(filter_input(INPUT_POST,"usuario")) && !empty(filter_input(INPUT_POST,"clave")))
		{
			$this->estado = $this->model->run();

			//Si el registro se ha realizado correctamente redirigimos a registrar/exito
			if (isset($this->estado))
			{
				if ($this->estado == 1)
					Utils::redirect("../registrar/exito");
				else if ($this->estado == 0)
					Utils::redirect("../registrar/error/0");
				else
					Utils::redirect("../registrar/error/-1");
			}
			else
				Utils::redirect("../registrar/error/-1");
		}
		else
			Utils::redirect(BASE_URL);
	}

	/**
	 * Muestra formulario de registro mostrando mensaje indicando que el registro se ha realizado bien
	 *
	 * @return void
	 */
	public function exito()
	{
		$this->view->msj_exito = msj_usuario_registrado;
		$this->view->render("identificarse/index");
	}

	/**
	 * Muestra el mensaje de error correspondiente en la pantalla de registro
	 *
	 * @param int $error
	 * @return void
	 */
	public function error($error = null)
	{
		if (isset($error))
		{
			switch($error)
			{
				case 0:
					$this->view->msj_error = msj_error_existe_usuario;
					break;
				default:
					$this->view->msj_error = msj_error_operacion;
					break;
			}
		}
		$this->view->render("registrar/index");
	}
}
?>