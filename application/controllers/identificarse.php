<?php
class Identificarse extends Controller
{
	function __construct()
	{
		parent::__construct();

		$this->view->pagina = titulo_pagina_identificarse;
	}

	function index()
	{
		$this->view->render("identificarse/index");
	}

	function error($error = null)
	{
		$this->view->msj_error = msj_error_login;

		if (isset($error))
			$this->view->msj_error = msj_error_operacion;

		$this->view->render("identificarse/index");
	}

	function run()
	{
		$usuario = filter_input(INPUT_POST, "usuario", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		$clave = filter_input(INPUT_POST, "clave", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		if (!empty($usuario) && !empty($clave))
		{
			$this->datos = $this->model->run();

			if (isset($this->datos))
			{
				if (is_array($this->datos))
				{
					Session::set("logueado",true);
					Session::set("idusuario", $this->datos[0]["idusuario"]);
					Session::set("usuario", $this->datos[0]["usuario"]);
					Session::set("foto", $this->datos[0]["foto"]);
					Session::set("created", time());
					Session::set("last_activity", time());

					Utils::redirect("../dashboard");
				}
				else if ($this->datos === -1)
					Utils::redirect("../identificarse/error/-1");	
			}
			else
			{
				Session::destroy();
				Utils::redirect("../identificarse/error/");
			}
		}else
			Utils::redirect(BASE_URL."identificarse");
	}

	function sesion_finalizada()
	{
		$this->view->msj_aviso = msj_info_sesion_finalizada;
		$this->view->render("identificarse/index");
	}
}
?>