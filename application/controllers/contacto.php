<?php

class Contacto extends Controller
{
	function __construct()
	{
		parent::__construct();
		$this->view->pagina = titulo_pagina_contacto;
	}

	function index()
	{
		$this->view->render("contacto/index");
	}

	function error($num = null)
	{
		$this->view->pagina = "Contacto";
		$this->view->msj_error = msj_error_enviar_consulta;
		$this->view->render("contacto/index");
	}

	
	function enviar()
	{

		# Filtramos los datos de formulario
		$datos_email = array(
			"nombre" => [
							"valor" => filter_input(INPUT_POST, "contacto", FILTER_SANITIZE_FULL_SPECIAL_CHARS), 
							"required" => true, 
							"longitud_max" => "100",
							"tipo" => "string"
						],
			"empresa" => [
							"valor" => filter_input(INPUT_POST, "empresa", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
							"required" => true,
							"longitud_max" => "100",
							"tipo" => "string"
						 ],
			"email" => [
							"valor" => filter_input(INPUT_POST, "email"),
							"required" => true,
							"longitud_max" => "100",
							"tipo" => "email"
						],
			"mensaje" => [
							"valor" => filter_input(INPUT_POST, "mensaje", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
							"required" => true,
							"longitud_max" => "2000",
							"tipo" => "string"
			 			],
		);


		# Validamos que el formulario de contacto esté rellenado correctamente
		$validacion = Validator::validarForm($datos_email);
		
		if (!empty($validacion))
		{
			$num_errores = $validacion["num_errores"];

			if ($num_errores == 0)
			{
				$resultado = [];

				$email = new Email\Email();
				$estado = $email->enviarEmail($datos_email);

				if (is_bool($estado) && $estado == true)
				{
					$this->view->msj_exito = msj_exito_envio_email;
					$this->view->render("contacto/index");
				}
				else
				{
					if (class_exists("MiLog"))
						MiLog::getInstance()->add("[Email] Mail Send Internal Error: contacto (Linea 57) " . $estado);	// Anotamos en el Log el fallo
							
					$this->view->msj_error = msj_error_envio_email;
					$this->view->render("contacto/index");
				}	
			}
			else
			{
				# La validación ha fallado
				$this->view->errores_form = $validacion;
				$this->view->datos_form = $datos_email;
				$this->view->render("contacto/index");
			}
		}
		else
		{
			$this->view->msj_error = "Debido a un error interno no ha sido posible validar el formulario. Disculpe las molestias";
			if (class_exists("MiLog"))
				MiLog::getInstance()->add("[Email] Error al validar formulario de email");
		}	

		$this->view->render("contacto/index");
	}
}
?>