<?php
class PTError extends Controller
{
	function __construct()
	{
		parent::__construct();
		
		$this->view->pagina = titulo_pagina_error;
	}

	function index($error = false)
	{
		$this->view->msg = $error;
		
		$this->view->render("error/index");
	}
}
?>