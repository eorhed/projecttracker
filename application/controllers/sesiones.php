<?php
class Sesiones extends Controller
{
	function __construct()
	{
		parent::__construct();
		if (!Session::isLogged())
			Utils::redirect(BASE_URL . "identificarse/");
		else if (!Session::manageSession())
			Utils::redirect(BASE_URL . "identificarse/sesion_finalizada");

		$this->view->pagina = titulo_pagina_sesiones;
	}

	function index()
	{
		$this->view->sesiones = $this->model->getSesiones();
		if ($this->view->sesiones == -1)
			$this->view->msj_error = msj_error_obtener_sesiones;

		$this->view->render("sesiones/index");
	}
}
?>