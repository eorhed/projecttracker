<?php 
class Install extends Controller
{
	function __construct()
	{
		parent::__construct();

		$this->view->pagina = titulo_pagina_instalacion;

        if (file_exists("application/config/database.ini.php"))
			Utils::redirect(BASE_URL."index");
	}
	

	/**
	 * Método que llama a renderizar la vista de configuración de parámetros para generar e instalar la BD
	 *
	 * @return void
	 */
	function index()
	{
		$this->view->render("install/index");
	}

	/**
	 * Método que inicia la instalación de la app en en servidor DB con los datos proporcionados por el usuario
	 * a través del formulario de configuración
	 *
	 * @return void
	 */
	function run()
	{
		// Recogemos los datos del formulario
		$input = array(
						"servidor" => filter_input(INPUT_POST, "servidor", FILTER_SANITIZE_STRING),
						"tipoServidorDB" => filter_input(INPUT_POST, "tipoServidorDB", FILTER_SANITIZE_STRING),
						"puerto" => filter_input(INPUT_POST, "puerto", FILTER_VALIDATE_INT),
						"nombreDB" => trim(filter_input(INPUT_POST, "nombreDB", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)),
						"usuario" => trim(filter_input(INPUT_POST, "usuario", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)),
						"clave" => filter_input(INPUT_POST, "clave", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)
					);

	
		if (!empty($input["servidor"]) && !empty($input["tipoServidorDB"]) && !empty($input["puerto"]) && !empty($input["nombreDB"]) && !empty($input["usuario"]))
		{
			$dbname = $input["nombreDB"]."_".date("Y");
			$errores = [];	// Array de mensajes de error de validación

			# Validamos que el numero de puerto sea un integer entre 0 y el max 65535
			$puerto = filter_var($input["puerto"], FILTER_VALIDATE_INT, array("options" => ["min_range" => 0, "max_range" => 65535]));

			if (empty($puerto))
				$errores["puerto"] = "El puerto tiene que ser un numero entero entre 0 y 65535";
			

			# Validamos que el servidor es una direccion IP o localhost
			$ip_server = filter_var($input["servidor"],FILTER_VALIDATE_IP);
			if (empty($ip_server) && $input["servidor"] != "localhost")
				$errores["servidor"] = "Error: Debes indicar la IP del servidor de BD o poner localhost";

			# Validamos que usuario es un string y no tiene numeros
			if (!preg_match('/^[a-zA-Z]+$/', $input["usuario"]))
				$errores["usuario"] = "Error: El usuario no debe tener caracteres especiales y tampoco números";


			if (empty(errores))
				if ($this->model->install())
					$this->view->render("install/instalacion_correcta");
				else
					$this->view->render("install/instalacion_incorrecta");
			else
				$this->view->render("install/instalacion_incorrecta");

		}
		else{
			echo "Error: Uno o varios de los campos están vacíos";
		}
	}
}
?>