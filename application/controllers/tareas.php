<?php
class Tareas extends Controller
{
	function __construct()
	{
		parent::__construct();
		if (!Session::isLogged())
			Utils::redirect(BASE_URL . "identificarse/");
		else if (!Session::manageSession())
			Utils::redirect(BASE_URL . "identificarse/sesion_finalizada");

		$this->view->pagina = titulo_pagina_tareas;
	}

	function index()
	{
		$this->view->tareas = $this->model->getTareas();

		// Si se devuelve codigo de error -1 informamos de que no se ha podido realizar la operacion
		if (isset($this->view->tareas) && $this->view->tareas == -1)
		{
			$this->view->msj_error = msj_error_operacion;
			$this->view->tareas = null;
		}
		$this->view->render("tareas/index");
	}

	function error($error)
	{
		if (isset($error))
		{
			switch ($error)
			{
				case State::NO_SUCCESS: 
					$this->view->msj_error = msj_error_existe_tarea;
					break;
				case State::ERROR_DB:
					$this->view->msj_error = msj_error_operacion;
					break;
                // default:
				// 	$this->view->msj_error = msj_error_operacion;
				// 	break;
			}

			$this->view->tareas = $this->model->getTareas();
			$this->view->render("tareas/index");
		}
		else
			Utils::redirect(BASE_URL . "tareas");
	}

	function agregar()
	{
		//Limpiamos los datos pasados del formulario
		$tarea = array(
				"nombre" => filter_input(INPUT_POST, "nombre", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
				"comentarios" => filter_input(INPUT_POST, "comentarios", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
				"usuario" => Session::get("usuario"),
				"fecha_creacion" => date("Y-m-d H:i:s")
			);

		// Si se pasan nombre y cliente llamamos al modelo de clientes para insertarlo en la BD
		if (!empty($tarea["nombre"]))
		{
			$this->estado = $this->model->agregarTarea($tarea);

			//Si la insercion del proyecto se ha realizado correctamente redirigimos a tareas/exito
			if (isset($this->estado))
			{
				if ($this->estado == State::SUCCESS)
					Utils::redirect(BASE_URL . "tareas/exito");
				else if ($this->estado == State::NO_SUCCESS)
					Utils::redirect(BASE_URL . "tareas/error/0");
				else if ($this->estado == State::ERROR_DB)
					Utils::redirect(BASE_URL . "tareas/error/-1");
			}
		}	
	}

	function editar($idtarea = null)
	{
		$operacion = filter_input(INPUT_POST, "operacion", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		if (!empty($operacion))
		{	
			// Filtramos los datos del formulario y los almacenamos en el array tarea
			$tarea = array(
				"usuario" => Session::get("usuario"),
				"nombre" => filter_input(INPUT_POST, "nombre", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
				"idtarea" => $idtarea,
				"comentarios" => filter_input(INPUT_POST, "comentarios", FILTER_SANITIZE_FULL_SPECIAL_CHARS)
			);

			if ($operacion == "guardar")
			{
				$this->view->estado = $this->model->editarTarea($tarea);
				
				if ($this->view->estado == State::SUCCESS)
					$this->view->msj_exito = msj_tarea_editada;

				else if ($this->view->estado == State::NO_SUCCESS)
					$this->view->msj_error = msj_error_existe_tarea2;

				else if ($this->view->estado == State::ERROR_NO_PERMISSION)
					$this->view->msj_error = msj_error_operacion_no_permitida;

				else
					$this->view->msj_error = msj_error_operacion;
			}
			else if ($operacion == "habilitar")
			{
				$this->view->estado = $this->model->habilitarTarea($tarea);

				if ($this->view->estado == State::SUCCESS)
					$this->view->msj_exito = msj_tarea_habilitada;

				else if ($this->view->estado == State::NO_SUCCESS)
					$this->view->msj_error = msj_error_habilitar_tarea;

				else if ($this->view->estado == State::ERROR_DB)
					$this->view->msj_error = msj_error_operacion;
			}
			else if ($operacion == "deshabilitar")
			{
				$this->view->estado = $this->model->deshabilitarTarea($tarea);

				if ($this->view->estado == State::SUCCESS)
					$this->view->msj_exito = msj_tarea_deshabilitada;

				else if ($this->view->estado == State::NO_SUCCESS)
					$this->view->msj_error = msj_error_deshabilitar_tarea;

				else if ($this->view->estado == State::ERROR_DB)
					$this->view->msj_error = msj_error_operacion;
			}
		}
		
		$this->view->datos_tarea = $this->model->getTarea($idtarea);
		$this->view->render("tareas/editar");
	}

	function exito()
	{
			$this->view->msj_exito = msj_tarea_creada;
			$this->view->tareas = $this->model->getTareas();
			$this->view->render("tareas/index");
	}
}
?>