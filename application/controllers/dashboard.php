<?php 
class Dashboard extends Controller
{
	function __construct()
	{
		parent::__construct();	
		if (!Session::isLogged())
			Utils::redirect(BASE_URL . "identificarse/");
		else if (!Session::manageSession())
			Utils::redirect(BASE_URL . "identificarse/sesion_finalizada");	

		$this->view->pagina = titulo_pagina_dashboard;
	}
	
	function index()
	{

		$resultado = $this->model->getNumClientesUsuario();
		$this->view->num_clientes = $resultado[0]["num_clientes"];

		if (isset($this->view->num_clientes) && $resultado == -1)
			$this->msj_error = msj_error_operacion;


		$resultado = $this->model->getNumProyectosUsuario();
		$this->view->num_proyectos = $resultado[0]["num_proyectos"];

		if (isset($this->view->num_proyectos) && $resultado == -1)
			$this->msj_error = msj_error_operacion;



		$resultado = $this->model->getNumTareasUsuario();
		$this->view->num_tareas = $resultado[0]["num_tareas"];

		if (isset($this->view->num_tareas) && $resultado == -1)
			$this->msj_error = msj_error_operacion;


		$resultado = $this->model->getNumHorasTotalesYDineroGanado();
		$this->view->num_horas_totales = round($resultado[0]["duracion_segs"] / 60 / 60);
		$this->view->dinero_ganado = round($resultado[0]["total_ganado"],2);

		if (isset($this->view->num_horas_totales) && $resultado == -1)
			$this->msj_error = msj_error_operacion;

		$this->view->render("dashboard/index");
	}
}
?>