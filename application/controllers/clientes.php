<?php
class Clientes extends Controller
{
	function __construct()
	{
		parent::__construct();
		if (!Session::isLogged())
			Utils::redirect(BASE_URL."identificarse/");
		else if (!Session::manageSession())
			Utils::redirect(BASE_URL."identificarse/sesion_finalizada");

		$this->view->pagina = titulo_pagina_clientes;
	}

	/**
	 * Página por defecto de clientes donde se listan los clientes del usuario
	 *
	 * @return void
	 */
	function index()
	{
		$opEliminar = filter_input(INPUT_POST, "opEliminar", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		$idcliente = filter_input(INPUT_POST, "idcliente", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		if (!empty($opEliminar) && $opEliminar == "eliminar" && !empty($idcliente))
		{
			if ($this->eliminarCliente($idcliente) == State::SUCCESS)
				$this->view->msj_exito = msj_cliente_eliminado;
			else
				$this->view->msj_error = msj_error_eliminar_cliente;
		}
		
		$this->view->clientes = $this->model->getListaClientes();
		if ($this->view->clientes == State::ERROR_DB)
			$this->view->msj_error = msj_error_obtener_clientes;

		$this->view->render("clientes/index");
	}

	/**
	 * Página de error donde se muestran los errores en operaciones sobre clientes
	 *
	 * @return void
	 */
	function error($error)
	{
		if (isset($error))
		{
			switch ($error)
			{
				case State::NO_SUCCESS: 
					$this->view->msj_error = msj_error_existe_cliente;
					break;
				case State::ERROR_DB:
					$this->view->msj_error = msj_error_agregar_cliente;
					break;
				default:
					$this->view->msj_error = msj_error_operacion;
					break;
			}
		}

		$this->view->clientes = $this->model->getListaClientes();
		if ($this->view->clientes == -1)
			$this->view->msj_error = msj_error_obtener_clientes;

		$this->view->render("clientes/index");
	}
		
	/**
	 * Agrega un nuevo cliente a la BD con los datos enviados por el usuario a traves del formulario
	 * y redirige a una determinada página de información donde se indica el resultado de la operación
	 *
	 * @return void
	 */
	function agregar()
	{
		$opAgregar = filter_input(INPUT_POST, "opAgregar", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		if (!empty($opAgregar) && $opAgregar == "agregar")
		{
			// Filtramos los datos de cliente obtenidos del formulario
			$cliente = array(
				"contacto" => [
								"valor" => filter_input(INPUT_POST, "contacto", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
								"required" => true,
								"longitud_max" => "100",
								"tipo" => "string"
				],
				"empresa" => [
								"valor" => filter_input(INPUT_POST, "empresa", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
								"required" => false,
								"longitud_max" => "100",
								"tipo" => "string"
				],
				"email" => [
								"valor" => filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL),
								"required" => true,
								"longitud_max" => "100",
								"tipo" => "string"
				],
				"telefono" => [
								"valor" => filter_input(INPUT_POST, "telefono",  FILTER_SANITIZE_FULL_SPECIAL_CHARS),
								"required" => false,
								"longitud_max" => "20",
								"tipo" => "string"
				],
				"direccion" => [
								"valor" => filter_input(INPUT_POST, "direccion",  FILTER_SANITIZE_FULL_SPECIAL_CHARS),
								"required" => false,
								"longitud_max" => "300",
								"tipo" => "string"
				],
				"municipio" => [
								"valor" => filter_input(INPUT_POST, "municipio",  FILTER_SANITIZE_FULL_SPECIAL_CHARS),
								"required" => false,
								"longitud_max" => "50",
								"tipo" => "string"
				]
			);
		
			$this->view->errores = Validator::validarForm($cliente); // Validamos los datos de cliente
		
			if ($this->view->errores["num_errores"] == 0)
			{
				$this->view->estado = $this->model->agregarCliente();

				//Si el alta de cliente se ha realizado correctamente redirigimos a clientes/exito
				if (!empty($this->view->estado))
				{
					if ($this->view->estado == 0)
						Utils::redirect("../clientes/error/0");
					else if ($this->view->estado === -1)
						Utils::redirect("../clientes/error/-1");
					else
						Utils::redirect("../clientes/exito");
				}
			}
			else
			{
				$this->view->msj_error = msj_error_form;
								
				foreach ($this->view->errores as $campo => $valor)
					if (!empty([$campo]["errores"]))
						foreach($campo["errores"] as $error)
							$this->msj_error .= $error . "<br>";
									
					$this->view->render("clientes/agregar");
			}
		}
		else
			$this->view->render("clientes/agregar");
	}

	/**
	 * Se actualizan los nuevos datos de cliente enviados por formulario
	 *
	 * @return void
	 */
	function editar($cliente = null)
	{
		$opEditar = filter_input(INPUT_POST, "editar", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		$opEliminar = filter_input(INPUT_POST, "eliminar", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		if (!empty($opEditar) && $opEditar == "editar")
		{
			$nombre_anterior = filter_input(INPUT_POST,"nombre_anterior", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			$idcliente = filter_input(INPUT_POST,"idcliente", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

			if ($cliente == null && !empty($nombre_anterior) && !empty($idcliente) && $idcliente > 0)
			{
				$this->view->estado = $this->model->editarCliente();
				
				if ($this->view->estado == 1)
					$this->view->msj_exito = msj_cliente_editado;

				else if ($this->view->estado == 0)
					$this->view->msj_error = msj_error_existe_cliente;

				else
					$this->view->msj_error = msj_error_operacion;

				$this->view->datos_cliente = $this->model->getCliente(filter_var($_POST["contacto"], FILTER_SANITIZE_STRING));

				if ($this->view->datos_cliente == -1)
					$this->view->msj_error = "<br>" . msj_error_obtener_datos_cliente;  // Añadimos este mensaje al anterior (si lo hay)
			}
		}
		else if (!empty($cliente))
		{
			$this->view->datos_cliente = $this->model->getCliente($cliente);
			if ($this->view->datos_cliente == -1)
				$this->view->msj_error = msj_error_obtener_datos_cliente;
		}
		else
			Utils::redirect("../clientes");

			$this->view->render("clientes/editar");
	}
	
	/**
	 * Método que obtiene la lista de clientes y llama a renderizar la vista 
	 * donde se informa a usuario que el cliente ha sido agregado con exito
	 *
	 * @return void
	 */
	function exito()
	{
		$this->view->msj_exito = msj_cliente_agregado;

		$this->view->clientes = $this->model->getListaClientes();
		if ($this->view->clientes == -1)
			$this->view->msj_error = msj_error_obtener_clientes;

		$this->view->render("clientes/index");
	}

	/**
	 * Elimina el cliente del usuario pasado a través de formulario
	 * y redirige al usuario a la página de error en caso de producirse un error
	 *
	 * @return void
	 */
	private function eliminarCliente($idcliente)
	{
		if (!empty($idcliente))
			return $this->model->eliminarCliente($idcliente);
		else
			Utils::redirect("clientes/error/-2");
	}	
}
?>