<?php 
class Index extends Controller
{
	function __construct()
	{
		parent::__construct();

		$this->view->pagina = titulo_pagina_inicio;
	}
	
	/**
	 * Método que renderiza/muestra la página principal/inicial de la aplicación
	 *
	 * @return void
	 */
	function index()
	{
		$this->view->datos = $this->model->run();
		//$this->view->novedades = $this->model->novedades();
		$this->view->render("index/index");
	}


}
?>