<?php
class Salir extends Controller
{
	function __construct()
	{
		parent::__construct();

		$this->view->pagina = titulo_pagina_salir;
	}

	function index()
	{
		$this->view->msj = msj_desconectando;
		$this->view->render("salir/index");

		Session::destroy();
		/*header("Location: login");*/
	}
}
?>