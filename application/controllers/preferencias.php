<?php
class Preferencias extends Controller
{
	function __construct()
	{
		parent::__construct();

		if (!Session::isLogged())
			Utils::redirect(BASE_URL . "identificarse");
		else if (!Session::manageSession())
			Utils::redirect(BASE_URL . "identificarse/sesion_finalizada");

		$this->view->pagina = titulo_pagina_preferencias;
	}

	/**
	 * Método que llama a renderizar la vista de preferencias del usuario
	 *
	 * @return void
	 */
	function index()
	{
		$this->view->prefs = $this->model->getPreferenciasUsuario();
		$this->view->render("preferencias/index");
	}

	/**
	 * Método que llama a renderizar la vista donde se informa a usuario que 
	 * las prefererencias del usuario se han guardado correctamente en el sistema
	 *
	 * @return void
	 */
	function exito()
	{
		$this->view->pagina = "Preferencias";
		$this->view->msj_exito = msj_exito_guardar_preferencias;
		$this->view->prefs = $this->model->getPreferenciasUsuario();
		$this->view->render("preferencias/index");
	}

	/**
	 * Método que llama a renderizar la vista donde se informa a usuario que se ha producido algún
	 * error durante la operación de actualizar las preferencias del usuario
	 *
	 * @return void
	 */
	function error()
	{
		$this->view->pagina = "Preferencias";
		$this->view->msj_error = msj_error_guardar_preferencias;
		$this->view->prefs = $this->model->getPreferenciasUsuario();
		$this->view->render("preferencias/index");
	}

	/**
	 * Método que recoge las nuevas preferencias de usuario pasadas por formulario y las actualiza en la BD
	 * y dependiendo del resultado se redirige al usuario para informarle del resultado de la operación
	 *
	 * @return void
	 */
	function actualizar()
	{
		$preferencias = array(
								"idusuario" => [
													"valor" => Session::get("idusuario"),
													"required" => true,
													"tipo" => "string"
											   ],
								"usuario" => [
												"valor" => Session::get("usuario"),
												"required" => true,
												"tipo" => "string"
											 ],
								"email" => [
												"valor" => filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL),
												"required" => true,
												"tipo" => "email",
												"longitud_max" => "100"
											],
								"foto_perfil" => [
											"valor" =>  !empty($_FILES["foto_perfil"]) ? $_FILES["foto_perfil"] : null,
											"tipo" => "file"
										  ],
								"precio_hora" => [
													"valor" => filter_input(INPUT_POST, "precio_hora", FILTER_SANITIZE_NUMBER_INT),
													"required" => true,
													"tipo" => "int",
													"longitud_max" => "100"
												]
								,
								"moneda" => [
												"valor" => filter_input(INPUT_POST, "moneda"),
												"required" => true,
												"tipo" => "string",
												"longitud_max" => "5"
											]
								,
								"tiempo_max_sesion" => [
															"valor" => filter_input(INPUT_POST, "tiempo_max_sesion", FILTER_SANITIZE_NUMBER_INT),
															"required" => true,
															"tipo" => "int",
															"longitud_max" => "3"
														]
								,
								"idioma" => [
												"valor" => filter_input(INPUT_POST, "idioma", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
												"required" => true,
												"tipo" => "string",
												"longitud_max" => "50"
											],
							);


		if (!empty($_POST))
		{
			$this->error = $this->model->actualizarDatosUsuario($preferencias);
			
			if (!empty($this->enviado) && $this->enviado)
				Utils::redirect(BASE_URL . "preferencias/error");
			else
				Utils::redirect(BASE_URL . "preferencias/exito");
		}
		else
			Utils::redirect(BASE_URL . "preferencias");
	}
}
?>