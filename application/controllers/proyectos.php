<?php
class Proyectos extends Controller
{
	function __construct()
	{
		parent::__construct();
		if (!Session::isLogged())
			Utils::redirect(BASE_URL . "identificarse");
		else if (!Session::manageSession())
			Utils::redirect(BASE_URL . "identificarse/sesion_finalizada");

			$this->view->pagina = titulo_pagina_proyectos;
	}

	function index()
	{
		$this->view->proyectos = $this->model->getListaProyectos();
		if ($this->view->proyectos == -1)
			$this->view->msj_error = msj_error_operacion;

		
		$this->view->clientes = $this->model->getClientesDelUsuario();
		if ($this->view->clientes == -1)
			$this->view->msj_error = msj_error_operacion;
			
		$this->view->render("proyectos/index");
	}

	function error($error = null)
	{
			if (isset($error))
			{
				switch ($error)
				{
					case "0": 
						$this->view->msj_error = msj_error_existe_proyecto;
						break;
					case "-1":
						$this->view->msj_error = msj_error_agregar_proyecto;
						break;
					default:
						$this->view->msj_error = msj_error_operacion;
						break;
				}
			}

			$this->view->proyectos = $this->model->getListaProyectos();
			if ($this->view->proyectos == -1)
				$this->view->msj_error = msj_error_operacion;

			$this->view->clientes = $this->model->getClientesDelUsuario();
			if ($this->view->clientes == -1)
				$this->view->msj_error = msj_error_operacion;

			$this->view->render("proyectos/index");
	}

	function agregar_proyecto()
	{
		if (!empty($_POST))
		{
			// Filtramos los datos de formulario limpiando todos los caracteres especiales
			$proyecto = array(
								"nombre" => filter_input(INPUT_POST, "nombre", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
								"cliente" => filter_input(INPUT_POST, "cliente", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
								"comentarios" => filter_input(INPUT_POST, "comentarios", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
								"fecha_inicio" => date("Y-m-d H:i:s"),
								"usuario" => Session::get("usuario")
							);

			if (!empty($proyecto["nombre"]) && !empty($proyecto["cliente"])){
				$this->view->estado = $this->model->agregarProyecto($proyecto);
			}
				

			//Si la insercion del proyecto se ha realizado correctamente redirigimos a proyectos/exito
			if (isset($this->view->estado))
			{
				if ($this->view->estado === 0)
					Utils::redirect(BASE_URL . "proyectos/error/0");
				else if ($this->view->estado === -1)
					Utils::redirect(BASE_URL . "proyectos/error/-1");
				else
					Utils::redirect(BASE_URL . "proyectos/exito");
			}
			else
				Utils::redirect(BASE_URL . "proyectos/error/-1");
		}
		else
			Utils::redirect(BASE_URL);
	}

	function editar($proyecto = null)
	{
		if (!empty($_POST))
		{
			// Filtramos los datos de formulario limpiando todos los caracteres especiales
			$nombre_anterior = filter_input(INPUT_POST, "nombre_anterior", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			$nombre_nuevo = filter_input(INPUT_POST, "nombre", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

			if ($proyecto == null && !empty($nombre_anterior))
			{

				$this->view->estado = $this->model->editarProyecto();

				switch ($this->view->estado)
				{
					case 1: 
							$this->view->msj_exito = msj_proyecto_editado;
							$this->view->datos_proyecto = $this->model->getProyecto($nombre_nuevo);
							break;
					case 0:
							$this->view->msj_error = msj_error_existe_proyecto;
							$this->view->datos_proyecto = $this->model->getProyecto($nombre_anterior);
							break;
					default:
							$this->view->msj_error = msj_error_operacion;
							$this->view->datos_proyecto = $this->model->getProyecto($nombre_anterior);
							break;
				}
			}
		}
		else
			$this->view->datos_proyecto = $this->model->getProyecto($proyecto);

		
		if (!empty($this->view->datos_proyecto) && $this->view->datos_proyecto == -1)
			$this->view->msj_error = msj_error_operacion;

		
		$this->view->clientes = $this->model->getClientesDelUsuario();
		if (!empty($this->view->clientes) && $this->view->clientes == -1)
			$this->view->msj_error = msj_error_operacion;

		// $this->view->tareas = $this->model->getTareasDelProyecto();
		$this->view->render("proyectos/editar");
	}

	function ver($proyecto)
	{
		/* Si se envia un formulario se obtiene la variable operacion de POST que nos dice el tipo de operacion que hay que realizar
		 * con este proyecto: 
		 * finalizar: pondrá la fecha en la que finalizó el proyecto de tal forma que ya no se podrá seguir trackeando en este proyecto
		 * reabrir: quitará la fecha de finalización para que se pueda seguir trackeando con este proyecto
		 * eliminar: eliminará el proyecto de la BD (lo pone a 0 en el campo habilitado)
		 */
		$operacion = filter_input(INPUT_POST, "operacion", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		if (!empty($operacion))
		{
			if ($operacion == "finalizar")
			{
				$this->view->estado = $this->model->finalizar($proyecto);  // Finalizamos el proyecto en la BD a través del modelo

				// Dependiendo del código de estado de la operación ponemos un mensaje u otro.
				if ($this->view->estado == 1)
					$this->view->msj_exito = msj_proyecto_finalizado;
				else if ($this->view->estado == 0)
					$this->view->msj_advertencia = msj_proyecto_finalizado_anteriormente;
				else
					$this->view->msj_error = msj_error_cerrar_proyecto;
			}
			else if ($operacion == "reabrir")
			{
				$this->view->estado = $this->model->reabrir($proyecto);	  // Reabrimos el proyecto en la BD a través del modelo

				// Dependiendo del código de estado de la operación ponemos un mensaje u otro.
				if ($this->view->estado == 1)
					$this->view->msj_exito = msj_proyecto_abierto;
				else if ($this->view->estado == 0)
					$this->view->msj_advertencia = msj_info_proyecto_ya_abierto;
				else
					$this->view->msj_error = msj_error_abrir_proyecto;
			}
			else
			{
				$this->view->estado = $this->model->eliminarProyecto($proyecto);

				// Dependiendo del código de estado de la operación ponemos un mensaje u otro.
				if ($this->view->estado == 1)
					Utils::redirect(BASE_URL."proyectos/exito/1");
				else
					Utils::redirect(BASE_URL."proyectos/error/1");
					//$this->view->msj_error = msj_error_eliminar_proyecto;
			}
		}
		$this->view->pagina = titulo_pagina_ver_proyecto;
		$this->view->nombre_proyecto = $proyecto;

		$this->view->datos_proyecto = $this->model->getProyecto($proyecto);
		if (isset($this->view->datos_proyecto) && $this->view->datos_proyecto == -1)
			$this->view->msj_error = msj_error_operacion;

		$this->view->tareas_asignadas = $this->model->getTareasDelProyecto($proyecto);
		if (isset($this->view->tareas_asignadas) && $this->view->tareas_asignadas == -1)
			$this->view->msj_error = msj_error_obtener_asignadas;

		$this->view->tareas_no_asignadas = $this->model->getTareasNoAsignadasAlProyecto($proyecto);
		if (isset($this->view->tareas_no_asignadas) && $this->view->tareas_no_asignadas == -1)
			$this->view->msj_error = msj_error_obtener_noasignadas;

		$this->view->render("proyectos/ver");
	}

	function exito($codigo = null)
	{
		if (isset($codigo)){
			if ($codigo == 1)
				$this->view->msj_exito = msj_proyecto_eliminado;	
		}
		else
			$this->view->msj_exito = msj_proyecto_creado;


		$this->view->proyectos = $this->model->getListaProyectos();
		if ($this->view->proyectos == -1)
			$this->view->msj_error = msj_error_operacion;

		$this->view->clientes = $this->model->getClientesDelUsuario();
		if ($this->view->clientes == -1)
			$this->view->msj_error = msj_error_operacion;

		$this->view->render("proyectos/index");
	}

	function asignar_tareas_proyecto($proyecto)
	{
		$noasignadas = $_POST["noasignadas"];
		if (!empty($noasignadas))
		{
			$this->view->estado = $this->model->asignarTareasAlProyecto($noasignadas, $proyecto);

			if ($this->view->estado == 1)
			{	
				$this->view->msj_exito = msj_tarea_asignada;
				$this->view->nombre_proyecto = $proyecto;
				$this->view->datos_proyecto = $this->model->getProyecto($proyecto);
				if (isset($this->view->datos_proyecto) && $this->view->datos_proyecto == -1)
					$this->view->msj_error = msj_error_operacion;

				$this->view->tareas_asignadas = $this->model->getTareasDelProyecto($proyecto);
				if (isset($this->view->tareas_asignadas) && $this->view->tareas_asignadas == -1)
					$this->view->msj_error = msj_error_operacion;

				$this->view->tareas_no_asignadas = $this->model->getTareasNoAsignadasAlProyecto($proyecto);
				if (isset($this->view->tareas_no_asignadas) && $this->view->tareas_no_asignadas == -1)
					$this->view->msj_error = msj_error_operacion;
					
			}
			else
				$this->view->msj_error = msj_error_asignar_tarea;

			$this->view->render("proyectos/ver");
		}
		else
			Utils::redirect(BASE_URL."proyectos/");
	}

	function desasignar_tareas_proyecto($proyecto)
	{
		$tarea_asignada = filter_input(INPUT_POST, "tarea_asignada", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		if (isset($tarea_asignada))
		{
			$this->view->estado = $this->model->desasignarTareaAlProyecto($tarea_asignada, $proyecto);


			// Dependiendo del código de estado de la operación ponemos un mensaje u otro.
			if ($this->view->estado == 1)
				$this->view->msj_exito = msj_tarea_desasignada;
			else
				$this->view->msj_error = msj_error_desasignar_tarea;


			if ($this->view->estado == 1)
			{	
				$this->view->nombre_proyecto = $proyecto;
				
				$this->view->datos_proyecto = $this->model->getProyecto($proyecto);
				if (isset($this->view->datos_proyecto) && $this->view->datos_proyecto == -1)
					$this->view->msj_error = msj_error_operacion;

				$this->view->tareas_asignadas = $this->model->getTareasDelProyecto($proyecto);
				if (isset($this->view->tareas_asignadas) && $this->view->tareas_asignadas == -1)
					$this->view->msj_error = msj_error_operacion;

				$this->view->tareas_no_asignadas = $this->model->getTareasNoAsignadasAlProyecto($proyecto);
				if (isset($this->view->tareas_no_asignadas) && $this->view->tareas_no_asignadas == -1)
					$this->view->msj_error = msj_error_operacion;

				$this->view->render("proyectos/ver");
			}
		}
		else
			Utils::redirect(BASE_URL."proyectos/");
	}
}
?>