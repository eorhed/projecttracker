<?php
class Informes extends Controller
{
	function __construct()
	{
		parent::__construct();
		if (!Session::isLogged())
			Utils::redirect(BASE_URL . "identificarse/");
		else if (!Session::manageSession())
			Utils::redirect(BASE_URL . "identificarse/sesion_finalizada");

		$this->view->pagina = titulo_pagina_informes;
	}

	/**
	 * Muestra la lista de informes de seguimiento de los proyectos del usuario
	 *
	 * @return void
	 */
	function index()
	{
		$this->view->informes = $this->model->getResumenInformes();

		if (isset($this->views->informes) && $this->view->informes == -1)
			$this->view->msj_error = msj_error_operacion;
			
		$this->view->render("informes/index");
	}

	/**
	 * Muestra el proyecto de forma desglosada en tareas, fechas y costes
	 *
	 * @param string $proyecto
	 * @return void
	 */
    function ver($proyecto)
    {
        $this->view->pagina = titulo_pagina_ver_informe;
        $this->view->proyecto = $proyecto;
		$this->view->informes = $this->model->getDesgloseTareasProyecto($proyecto);
		if (isset($this->views->informes) && $this->view->informes == -1)
			$this->view->msj_error = msj_error_operacion;

        $this->view->sesiones = $this->model->getSesiones($proyecto);
		if (isset($this->views->sesiones) && $this->view->sesiones == -1)
			$this->view->msj_error = msj_error_operacion;

		$this->view->fechas = $this->model->getFechasProyecto($proyecto);
		if (isset($this->views->fechas) && $this->view->fechas == -1)
			$this->view->msj_error = msj_error_operacion;

		$this->view->render("informes/ver");
    }
}
?>