<?php

/* GENERALES */
define("app","Projecttracker");
define("email_admin","eorhed@gmail.com");
define("msj_sin_resultados","No se han encontrado resultados");
define("msj_error_form", "Se han producido algunos errores al rellenar el formulario");

/* CONTACTO */
define("msj_asunto_email","Consulta desde Projectracker");
define("msj_error_email_no_valido","Dirección de email no válida.");
define("msj_error_envio_email","El email no se ha podido enviar. Inténtalo más tarde.");
define("msj_exito_envio_email","El email se ha enviado correctamente.Su consulta será respondida lo antes posible.");

/* INSTALACION */
define("titulo_pagina_instalacion","Instalación de la aplicación");

/* CLIENTES */
define("titulo_pagina_clientes", "Clientes");
define("msj_cliente_eliminado","El cliente se ha eliminado correctamente");
define("msj_error_eliminar_cliente","Error: No se ha podido eliminar el cliente debido a un error interno");

define("msj_error_existe_cliente","Error: Este cliente ya existe en el sistema");
define("msj_error_agregar_cliente","Error: No se ha podido agregar el cliente a la BD");
define("msj_error_operacion","Error: No se ha podido realizar la operación. Inténtalo más tarde.");
define("msj_error_obtener_clientes", "Error: No se ha podido cargar la lista de clientes.");
define("msj_error_obtener_datos_cliente","Error: No se han podido obtener los datos del cliente.");
define("msj_cliente_editado", "El cliente se ha editado correctamente.");
define("msj_error_nombre_cliente_vacio", "El campo cliente del formulario no puede estar vacío");
// define("msj_error_operacion","Error: No se ha podido realizar la operaci&oacute;n. Int&eacute;ntalo m&aacute;s tarde.");
// define("msj_error_existe_cliente","Error: Este cliente ya existe en el sistema");

define("msj_cliente_agregado","El cliente se ha agregado correctamente.");

/* CONTACTO */
define("titulo_pagina_contacto","Contacto");
define("msj_error_enviar_consulta","No se ha podido enviar la consulta. Inténtalo más tarde.");

/* ERROR */
define("titulo_pagina_error","Error");

/* IDENTIFICARSE */
define("titulo_pagina_identificarse","Identificarse");
define("msj_error_login","No existe el usuario o clave incorrecta");
define("msj_info_sesion_finalizada", "La sesión ha caducado. Vuelve a iniciar sesión en el sistema.");

/* INDEX */
define("titulo_pagina_inicio","Inicio");

/* DASHBOARD */
define("titulo_pagina_dashboard","Panel de control");

/* INFORMES */
define("titulo_pagina_informes","Informes");
define("titulo_pagina_ver_informe","Ver informe proyecto");

/* PROYECTOS */
define("titulo_pagina_proyectos","Proyectos");
define("titulo_pagina_ver_proyecto","Ver datos proyecto");
define("msj_error_existe_proyecto","Este proyecto ya existe en el sistema");
define("msj_error_agregar_proyecto","No se ha podido agregar el proyecto a la BD");
// define("msj_error_operacion","Error: No se ha podido realizar la operaci&oacute;n. Inténtalo más tarde.");
define("msj_proyecto_editado","El proyecto se ha editado correctamente");
define("msj_proyecto_finalizado","El proyecto se ha marcado como finalizado correctamente");
define("msj_proyecto_finalizado_anteriormente","El proyecto ya estaba marcado como finalizado anteriormente");
define("msj_error_cerrar_proyecto","El proyecto no se ha podido cerrar correctamente");

define("msj_proyecto_abierto","El proyecto se ha abierto de nuevo correctamente");
define("msj_info_proyecto_ya_abierto","No es posible reabrir el proyecto puesto que ya se encontraba abierto antes de esta operación");
define("msj_error_abrir_proyecto","El proyecto no se ha podido abrir correctamente");

define("msj_error_eliminar_proyecto","No se ha podido eliminar correctamente el proyecto");

define("msj_proyecto_eliminado","El proyecto ha sido eliminado correctamente.");
define("msj_proyecto_creado","El proyecto se ha creado correctamente.");

define("msj_tarea_asignada","Las tareas se han asignado correctamente.");
define("msj_error_obtener_asignadas","No se han podido obtener las tareas que ha asignado el usuario para este proyecto.");
define("msj_error_obtener_noasignadas","No se han podido obtener las tareas que no ha asignado el usuario para este proyecto.");
define("msj_error_asignar_tarea","Debido a un error interno la tarea no se ha podido asignar. Inténtalo más tarde.");

define("msj_tarea_desasignada","La tarea se ha desasignado del proyecto correctamente");
define("msj_error_desasignar_tarea","Debido a un error interno la tarea no se ha podido desasignar. Inténtalo más tarde.");

/* REGISTRARSE */
define("titulo_pagina_registrarse","Registrarse");
define("msj_error_existe_usuario","Este usuario ya existe en el sistema");
define("msj_usuario_registrado","El registro se ha realizado correctamente. Identifícate para entrar en el sistema.");

/* SALIR */
define("titulo_pagina_salir","Salir");
define("msj_desconectando","Saliendo del sistema...");

/* SESIONES */
define("titulo_pagina_sesiones","Sesiones");
define("msj_error_obtener_sesiones", "Error: No se ha podido cargar el registro de sesiones de seguimiento del usuario");

/* TAREAS */
define("titulo_pagina_tareas","Tareas");
define("msj_error_existe_tarea","Error: Esta tarea ya existe en el sistema");
define("msj_error_agregar_tarea","Error: No se ha podido agregar la tarea a la BD");

define("msj_tarea_editada","La tarea se ha editado correctamente");
define("msj_error_existe_tarea2","Existe en el sistema otra tarea con el mismo nombre que el elegido");
define("msj_error_operacion_no_permitida","Operación no permitida.");

define("msj_tarea_habilitada","La tarea se ha habilitado correctamente. Ya puedes asignar esta tarea a los proyectos");
define("msj_error_habilitar_tarea","La tarea a habilitar no existe o no pertenece a este usuario");

define("msj_tarea_deshabilitada","La tarea se ha deshabilitado correctamente. No puedes asignar esta tarea a los proyectos hasta que no la vuelvas a habilitar.");
define("msj_error_deshabilitar_tarea","La tarea a deshabilitar no existe o no pertenece a este usuario");

define("msj_tarea_creada","La tarea se ha creado correctamente.");

/* TRACKEAR */
define("titulo_pagina_trackear","Trackear");
define("msj_error_obtener_proyectos","Debido a un error interno no se ha podido obtener tu lista de proyectos. Inténtalo más tarde.");
define("msj_error_obtener_tareas","Error al obtener lista de tareas del usuario");
define("msj_error_no_hay_proyectos","No hay ningún proyecto creado todavía. Crea tu primer proyecto desde el menú Administrar->Proyectos");


/* PREFERENCIAS */
define("titulo_pagina_preferencias","Preferencias");
define("msj_exito_guardar_preferencias","Las preferencias se han guardado correctamente");
define("msj_error_guardar_preferencias","Error al guardar preferencias usuario");
define("msj_error_subir_foto", "Lo siento pero la foto no ha podido ser actualizada");
define("msj_error_formato_fichero_invalido","");
define("msj_error_fichero_ok_db_error","La imagen se ha subido pero el nombre de archivo no se ha actualizado");
?>