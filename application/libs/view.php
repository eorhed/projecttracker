<?php 
class View
{
	function __construct()
	{
		/*echo "Vista principal<br />";*/
	}

	function render($name, $noInclude = false)
	{
		if ($noInclude == true)
			include_once("application/views/" . $name . ".php");
		else
		{
			include_once("application/views/header.php");
			include_once("application/views/" . $name . ".php");
			include_once("application/views/footer.php");
		}
		
	}
}
?>