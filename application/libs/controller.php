<?php 
class Controller
{
	function __construct()
	{
		$this->view = new View();

	}

	public function cargarModelo($nombre)
	{
		$archivo = 'application/models/' . $nombre . '_model.php';

		if (is_file($archivo))
		{
			include_once($archivo);
			$nombreModelo = $nombre . '_model';
			$this->model = new $nombreModelo;
		}
	}
}
?>