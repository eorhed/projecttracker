<?php
class MiLog
{
    private static $instancia;
    private $log;
    private $numMensajes;

    function __construct()
	{
		$this->log = array();
        $this->numMensajes = 0;
	}

    /**
     * Obtiene la única instancia de la clase Log (MAE)
     *
     * @return void
     */
    static function getInstance()
    {
        if (!isset(self::$instancia))
        {
            $clase = __CLASS__;
            self::$instancia = new $clase;
        }
        return self::$instancia;
    }

    /**
     * Obtiene el array con los registros del Log
     *
     * @return void
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Muestra por pantalla el evento ocurrido registrado en el Log del sistema
     *
     * @return void
     */
    public function showLog()
    {
        $response = "<div class='clearfix'></div><div id='log'><h3>Errores (" . $this->getNumMensajes() . "):</h3><ul>";
        foreach ($this->log as $registro)
            $response .= "<li>" .$registro["mensaje"] . "<br><br>Fecha: " . substr($registro["fecha"],0,-3) . "h</li>";

        $response .= "</ul></div>";
        
        return $response;
    }

    /**
     * Agrega el evento (alerta/error) como nuevo registro al Log del sistema
     *
     * @param string $mensaje
     * @return void
     */
    public function add($mensaje)
    {
        $registro = array("fecha" => date("Y-m-d H:i:s"), "mensaje" => $mensaje);
        $this->log[] = $registro;   // array_push($this->log,$registro);
        $this->numMensajes++;
        
        $this->save($registro);
    }

    /**
     * Guarda en un archivo la nueva entrada del log
     *
     * @param array<string> $registro
     * @return void
     */
    private function save($registro)
    {
        try
        {
            $file = fopen("application/logs/log.dat","a");
            fwrite($file,$registro["fecha"] . "\t" . $registro["mensaje"] . "\r\n");
            fclose($file);
        }
        catch(IOException $e)
        {
            // $this->add($e->getMessage());
            die("Error de escritura del archivo de log");
        }
    }

    /**
     * Vacía la lista de entradas del log
     *
     * @return void
     */
    public function clean()
    {
        $this->log = [];
    }


    /**
     * Comprueba si hay mensajes guardados en el Log del sistema
     *
     * @return void
     */
    public function hayMensajes()
    {
        return ($this->numMensajes > 0);
    }

    /**
     * Obtiene el número del mensajes guardados en el Log del sistema
     *
     * @return void
     */
    public function getNumMensajes()
    {
        return $this->numMensajes;
    }
}
?>