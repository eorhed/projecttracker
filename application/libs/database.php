<?php
class Database extends PDO
{
	function __construct()
	{
		$fich_config = "application/config/database.ini.php";
		if (file_exists($fich_config))
		{
			$config = parse_ini_file($fich_config);
			// $host = DB_TYPE.":host=".DB_HOST.";dbname=".DB_NAME;
			// parent::__construct($host,DB_USER,DB_PASSWORD);
			$host = $config["db_driver"].":host=".$config["db_host"].";dbname=".$config["db_name"];
			parent::__construct($host, $config["db_user"], $config["db_password"]);
		}
	}
}
?>