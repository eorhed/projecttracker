<?php namespace Email;

class Email
{
    private $cabeceras;
    private $remitente;
    private $destinatario;
    private $asunto;
    private $cuerpo_mensaje;

    
    public function __construct()
    {
        $this->remitente = "";
        $this->destinatario = email_admin;      // Obtenido de mensajes.php
        $this->asunto = msj_asunto_email;       // Obtenido de mensajes.php
        $this->cuerpo_mensaje = "";
        $this->cabeceras = [];
    }

    /**
	 * Página por defecto de clientes donde se listan los clientes del usuario
	 * @param array $datos_email
	 * @return bool $estado_operacion true si se ha enviado el email correctamente
	 */
    public function enviarEmail($datos_email)
    {
        if (!empty($datos_email))
        {
            $this->remitente = $datos_email["email"]['valor'];
            $this->cuerpo_mensaje = "Contacto: " . $datos_email['nombre']['valor'] . "\r\n".
                                    "Empresa: " . $datos_email['empresa']['valor'] == "" ? "-" : $datos_email["empresa"]['valor'] . "\r\n".
                                    "Email contacto: " . $datos_email['email']['valor'] . "\r\n".
                                    "Fecha envío: " . date("d-m-Y") . "\r\n".
                                    "****************************************\r\n".
                                    "Mensaje:\r\n" . $datos_email['mensaje']['valor'] . "\r\n";
                                    

            
            $this->cabeceras[] = 'From: ' . $datos_email['nombre']['valor'] . '<' . $datos_email["email"]['valor'] . '>';
            $this->cabeceras[] = 'Reply-To: ' . $datos_email["email"]['valor'];
            $this->cabeceras[] = 'MIME-Version: 1.0';
            $this->cabeceras[] = 'Content-type: text/plain; charset=iso-8859-1';
            $this->cabeceras[] = 'X-Mailer: PHP/' . phpversion();

            
            try{$success = mail($this->destinatario, $this->asunto, $this->cuerpo_mensaje, implode("\r\n", $this->cabeceras));}

            catch(Exception $e){
                throw new \Exception(error_get_last()['message']);
            }

            if (!$success)
                return false;
            else
			    return true;	
        }
    }

    /**
	 * Valida los datos del formulario de contacto para su posterior envío por email
	 * @param array $datos_email
	 * @return array Mensajes de error de cada campo del formulario
	 */
    public function validarEmail($datos_email)
    {
        $resultado_validacion = [];
        $num_errores = 0;

        foreach ($datos_email as $nombre_campo => $datos) 
        {
            $resultado_validacion[] = array();
            

            // Validamos que los campos obligatorios no estén vacios
            if (isset($datos["required"]) && $datos["required"])
            {
                if (empty($datos["valor"]))
                {
                    $resultado_validacion[$nombre_campo]["errores"][] = "El campo $nombre_campo no debe estar vacío";
                    $num_errores++;
                }
            }
            
            // Validamos que no sobrepase la longitud máxima
            $long_campo = strlen($datos["valor"]);
            $long_max = $datos["longitud_max"];
            if (isset($long_max) && $long_max > 0 && $long_campo > $long_max)
            {
                    $resultado_validacion[$nombre_campo]["errores"][] = "El campo $nombre_campo no debe sobrepasar los $long_max caracteres";
                    $num_errores++;
            }

            // Validamos el tipo de dato
            $tipo_dato = strtolower($datos["tipo"]);
            
            // FILTER_VALIDATE_EMAIL no acepta ñ, si las hubiese en email, las sustituimos por n para pasar validacion
            $email_filtrado = preg_replace("[ñ]","n", $datos["valor"]);  
            
            if ($tipo_dato == "email")
                if (!filter_var($email_filtrado, FILTER_VALIDATE_EMAIL))
                {
                    $resultado_validacion[$nombre_campo]["errores"][] = "El campo $nombre_campo está mal escrito o no es aceptado.";
                    $num_errores++;
                }
        }

        $resultado_validacion["num_errores"] = $num_errores;

        return $resultado_validacion;
    }

    // es_posible_enviar()
    // Médidas de seguridad: Antes de enviar comprobamos si el usuario ha enviado 1 correo hace menos de 15min
    // Así evitamos email flooding a través del formulario
    public function esPosibleEnviar()
    {
        $enviar_email = true;
        
        if (is_file("form.dat"))
        {
            $fecha_hora_actual = time();
            //echo "Fecha hora actual: $fecha_hora_actual<br />";


                // Leemos el archivo
                $envios = file("form.dat");

                // Limpiamos el archivo dejando solo las entradas de hace 15 minutos o menos 
                // a la fechahora actual
                foreach ($envios as $envio)
                {
                    $e = explode(" ", $envio);
                    $fecha_envio = $e[0];
                    $ip_envio = $e[1];

                    // Si esta dentro del rango de los 10min
                    if ( $fecha_hora_actual - (int) $fecha_envio < 60 * 10 )
                    {
                        // La guardamos todavía para usarla en futuras comprobaciones
                        $envios_aux[] = $envio;
                        
                        if ($ip_envio == $ip)
                        {
                            $enviar_email = false;
                            return false;
                        }
                    }
                }


                
                $fichero_salida = fopen("form.dat","w");   //Creamos el archivo y anotamos el envio (ip,fecha)
                $marca_tiempo = time();     // Fechahora actual en formato UNIX

                

                // Si no se ha enviado en los x min anteriores añadimos la marca de envio
                if ($enviar_email)
                    $envios_aux[] = $marca_tiempo . " " . $ip;
                    
                // Escribimos en el fichero los registros de emails enviados hasta 10min antes de la fecha actual
                fwrite($fichero_salida, implode("\n",$envios_aux));
                fclose($fichero_salida);

                return true;

                //echo "<br /><br />******************<br />";
                //echo var_dump($envios_aux);
                        
        }else
        {
            //Creamos el archivo y anotamos el envio (ip,fecha)
            $fichero_salida = fopen("form.dat","w");

            // Fechahora actual en formato UNIX
            $fecha_hora_actual = time();

            //echo $fecha_hora_actual."-".$ip;

            fwrite($fichero_salida, $fecha_hora_actual . " " . $ip);
            fclose($fichero_salida);

            return true;

            //echo "Enviado";
        }

            /*
            $envios = fopen("form.dat","r");
            while (!feof($envios)) {
                $lineas[] = fgets($fichero_salida);
            }*/
    }  
}

/*
//Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
header('Content-type: application/json; charset=utf-8'); // Imprescindible para que se recoja el JSON en el .js

echo json_encode($resultado);   // Devolvemos el resultado en formato JSON a la petición AJAX
 */


?>