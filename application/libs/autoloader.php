<?php
class Autoloader
{
    function __construct($modulos)
    {
        foreach ($modulos as $modulo)
        {
            if ($modulo == "paths" || $modulo == "mensajes")
                require_once("application/config/" . $modulo . ".php");
            else
                include_once("application/libs/" . $modulo . ".php");
        }
    }
}
?>