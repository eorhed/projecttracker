<?php
class Model
{
	function __construct()
	{
		$this->db = new Database();
		if ($this->db)
		{
			# Desactivamos preparaciones emuladas
			# https://es.stackoverflow.com/questions/18232/c%C3%B3mo-evitar-la-inyecci%C3%B3n-sql-en-php
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

			# Gestión de errores de la DB via Excepciones
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
		}
	}

	/**
	 * Realiza una operacion select en la BD y devuelve los resultados (de haberlos) en un array
	 * si no los hubiera devuelve null
	 *
	 * @param string $sql
	 * @param array<string> $datos
	 * @param string $orden
	 * @param string $limite
	 * @return array<string>|null
	 */
	protected function select($sql, $datos, $orden = null, $limite = null)
	{
		try
		{
			$stmt = $this->db->prepare($sql);
			$stmt->execute($datos);

			$data = $stmt->fetchAll();	// Almacenamos los datos
				

			if ($stmt->rowCount($data))
				return $data;
			else
				return null;
		}
		catch(PDOException $e)
		{
			if (class_exists("MiLog"))
				MiLog::getInstance()->add($e->getMessage());	// Enviamos el mensaje de error a la cola de errores de la clase MiLog

			return -1;
		}
	}

	/**
	 * Realiza una operación update en la BD
	 * Si realiza bien la operación devuelve codigo de estado 1
	 * Si se produce un fallo al ejecutar update devuelve codigo de error 0
	 * Si se ha producido un error (excepcion capturada) devuelve codigo de error -1
	 *
	 * @param string $nombre_tabla
	 * @param array<string> $datos array_asociativo datos del SET
	 * @param array<string> $condiciones array_asociativo condiciones del WHERE
	 * @return int codigo estado
	 */
	protected function update($nombre_tabla, $datos, $condiciones)
	{
		try
		{
			$sql = "UPDATE " . $nombre_tabla . " SET ";

			$input = array();

			$cont = 0;
			foreach ($datos as $clave => $valor){
				if ($cont == 0)
					$sql .= $clave . " = :" . $clave;
				else
					$sql .= ", " . $clave . " = :" . $clave;
					
				$input[$clave] = $valor;
				$cont++;
			}

			$sql .= " WHERE ";

			$cont = 0;
			foreach ($condiciones as $clave => $valor) {
				if ($cont == 0)
					$sql .= $clave . " = :" . $clave;
				else
					$sql .= " AND " . $clave . " = :" . $clave;
				$input[$clave] = $valor;
			
				$cont++;
			}

			
			$stmt = $this->db->prepare($sql);
			return $stmt->execute($input);
		}
		catch(PDOException $e)
		{
			if (class_exists("MiLog"))
				MiLog::getInstance()->add($e->getMessage());	// Enviamos el mensaje de error a la cola de errores de la clase MiLog
				
			return -1;
		}
		
	}

	/**
	 * Realiza una operación insert en la BD
	 * Si realiza bien la operación devuelve codigo de estado 1
	 * Si se produce un fallo al ejecutar insert devuelve codio de error 0
	 * Si se ha producido un error (excepcion capturada) devuelve codigo de error -1
	 *
	 * @param string $nombre_tabla
	 * @param array<string> $datos
	 * @return int codigo estado
	 */
	protected function insert($nombre_tabla, $datos)
	{
		try
		{
			$sql = "INSERT INTO " . $nombre_tabla . " (";

			foreach ($datos as $clave => $valor)
				$sql .= "" . $clave . ",";
			
			$sql = substr($sql,0,strlen($sql)-1);	//Quitamos la ultima coma
			$sql .= ") VALUES (";

			foreach ($datos as $clave => $valor)
				$sql .= ":" . $clave . ",";

			$sql = substr($sql,0,strlen($sql)-1);	//Quitamos la ultima coma
			$sql.= ");";

			$stmt = $this->db->prepare($sql);
			
			return $stmt->execute($datos);
			// $lastID = $this->db->lastInsertId();
		}
		catch(PDOException $e)
		{
			if (class_exists("MiLog"))
				MiLog::getInstance()->add($e->getMessage());	// Enviamos el mensaje de error a la cola de errores de la clase MiLog

			return -1;
		}
	}

	/**
	 * Realiza una operación delete en la BD
	 * Si realiza bien la operación devuelve codigo de estado 1
	 * Si se produce un fallo al ejecutar insert devuelve codio de error 0
	 * Si se ha producido un error (excepcion capturada) devuelve codigo de error -1
	 *
	 * @param string $nombre_tabla
	 * @param array<string> $condiciones condiciones del WHERE
	 * @return int codigo estado
	 */
	protected function delete($nombre_tabla,$condiciones)
	{
		try
		{
			$sql = "DELETE FROM " . $nombre_tabla . " WHERE ";

			$cont = 0;
			foreach ($condiciones as $clave => $valor) {
				if ($cont == 0)
					$sql .= $clave . " = :" . $clave;
				else
					$sql .= " AND " . $clave . " = :" . $clave;
			
				$cont++;
			}

			$stmt = $this->db->prepare($sql);
			return $stmt->execute($condiciones);
		}
		catch(PDOException $e)
		{
			if (class_exists("MiLog"))
				MiLog::getInstance()->add($e->getMessage());	// Enviamos el mensaje de error a la cola de errores de la clase MiLog

			return -1;
		}
	}
}
?>