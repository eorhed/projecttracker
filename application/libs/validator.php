<?php
abstract class Validator{

    /**
	 * Valida los datos del formulario y devuelve los mensajes de error de cada campo del form
	 *
	 * @param array $datosForm
	 * @return array string errores
	 */
	static function validarForm($datosForm)
	{
		$resultado_validacion = [];
        $num_errores = 0;

        foreach ($datosForm as $nombre_campo => $dato) 
        {
            //$resultado_validacion[] = array();
            

            // Validamos que los campos obligatorios no estén vacios
            if (!empty($dato["required"]) && $dato["required"])
            {
                if (empty($dato["valor"]))
                {
                    $resultado_validacion[$nombre_campo]["errores"][] = "El campo $nombre_campo no debe estar vacío";
                    $num_errores++;
                }
            }
            
            // Validamos que no sobrepase la longitud máxima
            $long_campo = strlen($dato["valor"]);
            $long_max = $dato["longitud_max"];
            if (!empty($long_max) && $long_max > 0 && $long_campo > $long_max)
            {
                    $resultado_validacion[$nombre_campo]["errores"][] = "El campo $nombre_campo no debe sobrepasar los $long_max caracteres";
                    $num_errores++;
            }

            // Validamos el tipo de dato
            $tipo_dato = strtolower($dato["tipo"]);
            
            // FILTER_VALIDATE_EMAIL no acepta ñ, si las hubiese en email, las sustituimos por n para pasar validacion
            $email_filtrado = preg_replace("[ñ]","n", $dato["valor"]);  
            
            if ($tipo_dato == "email")
            {
                if (!filter_var($email_filtrado, FILTER_VALIDATE_EMAIL))
                {
                    $resultado_validacion[$nombre_campo]["errores"][] = "El campo $nombre_campo está mal escrito o no es aceptado.";
                    $num_errores++;
                }
            }
        }

        $resultado_validacion["num_errores"] = $num_errores;

        return $resultado_validacion;
	}
}