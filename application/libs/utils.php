<?php
class Utils
{
	/**
	 * Redirige al usuario a la página pasada como parámetro
	 *
	 * @param string $location
	 * @return void
	 */
	static function redirect($location) {
		header("Location: $location");
		exit();
	}

	/**
	 * Devuelve el texto escapado y libre de caracteres especiales
	 *
	 * @param string $text
	 * @return string
	 */
	static function print($text)
	{
		echo filter_var($text, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	}

}

abstract class State
{
	const SUCCESS = 1;
	const NO_SUCCESS = 0;
	const ERROR_DB = -1;
	const ERROR_NO_PERMISSION = -2;
}
?>