<?php 
class Bootstrap
{
	function  __construct()
	{
		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, "/");
		$url = explode('/', $url);


		if (empty($url[0]))
		{
			if (!file_exists("application/config/database.ini.php"))
			{
				include_once("application/controllers/install.php");
				Util::redirect("install");
			}
			else
			{
				include_once("application/controllers/index.php");
				$controller = new Index();
				header("Location:index");
				$controller->index();
			}
		}
		
		// Importamos el controlador correspondiente a lo pasado como primer valor en la barra de direcciones
		$file = "application/controllers/" . $url[0] . ".php";

		if (file_exists($file))
			include_once($file);
		else
		{
			// include_once("application/controllers/error.php");
			// $controller = new Error();
			// $controller->index("La página a la que intentas acceder no existe");
			$this->render_error();
			return false;
		}
			
			

		// Instanciamos el objeto de la clase importada arriba
		$controller = new $url[0];
		$controller->cargarModelo($url[0]);

		// Si existe un tercer valor en la barra de direcciones de la forma /controlador/metodo/argumento
		if (isset($url[2]))
		{
			if (method_exists($controller, $url[1]))
		  		$controller->{$url[1]}($url[2]);       // Cargamos el método pasado del controlador instanciado pasándole el argumento pasado en la url
		  	else
		  		$this->render_error();
		}
		else
		{
			// Si no se ha pasado en la url ningun argumento, solo el controlador y el metodo: /controlador/metodo
			if (isset($url[1]))
			{
			  	if (method_exists($controller, $url[1]))
					$controller->{$url[1]}();           // Cargamos solo el método pasado del controlador instanciado sin pasar ningún parámetro
			  	else
			  		$this->render_error();
			}
			else
			  	$controller->index();
		}
	}

	public function render_error()
	{
		include_once("application/controllers/error.php");
		$controller = new PTError();
		$controller->index("La página a la que intentas acceder no existe");
		return false;
	}
}
?>