<?php 
class Informes_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}

        /**
        * Devuelve la lista de informes de seguimiento de los proyectos del usuario
        *
        * @return array
        */
	function getResumenInformes()
	{
		$input["usuario"] = Session::get("usuario");

                $sql = "SELECT sum(S.duracion_segs) as duracion_total, sum(S.duracion_segs / 3600 * S.precio_x_hora) as precio_total, P.nombre, P.fecha_inicio, T.nombre as tarea 
                        FROM sesiones_tarea_proyecto as S JOIN proyectos as P JOIN tareas as T 
                        WHERE P.usuario = :usuario and S.idproyecto = P.idproyecto and S.idtarea = T.idtarea
                        GROUP BY P.idproyecto";
            
                return $this->select($sql,$input);
	}

        /**
        * Devuelve un resumen de horas y costes(precio) de cada tarea del proyecto
        *
        * @param string $proyecto
        * @return array
        */
	function getDesgloseTareasProyecto($proyecto)
	{
                $input["usuario"] = Session::get("usuario");
                $input["proyecto"] = $proyecto;

                $sql = "SELECT T.idtarea,T.nombre as tarea, SUM(S.duracion_segs) as duracion_total, COUNT(S.idtarea) as num_sesiones, SUM(S.duracion_segs / 3600 * S.precio_x_hora) as precio_total 
                        FROM sesiones_tarea_proyecto as S JOIN tareas as T JOIN proyectos as P 
                        ON S.idtarea = T.idtarea and S.idproyecto = P.idproyecto 
                        WHERE P.nombre = :proyecto AND P.usuario = :usuario 
                        GROUP BY T.idtarea";

                return $this->select($sql, $input);
	}

        /**
        * Devuelve las sesiones de las tareas ordenadas por el nombre de tarea
        *
        * @param string $proyecto
        * @return array
        */
        function getSesiones($proyecto)
        {
	        $input["usuario"] = Session::get("usuario");
                $input["proyecto"] = $proyecto;

                $sql = "SELECT T.nombre as tarea, S.fecha, SUM(S.duracion_segs) AS duracion_segs, ROUND(AVG(S.precio_x_hora),2) as precio_x_hora
                        FROM sesiones_tarea_proyecto as S JOIN tareas as T JOIN proyectos as P
                        ON S.idtarea = T.idtarea and S.idproyecto = P.idproyecto 
                        WHERE P.nombre = :proyecto and P.usuario = :usuario 
                        GROUP BY T.idtarea ";

                return $this->select($sql, $input);
	}

        /**
        * Devuelve las fechas (fecha inicio y fecha fin) en las que el proyecto fue desarrollado
        *
        * @param string $proyecto
        * @return array
        */
        function getFechasProyecto($proyecto)
	{
		$input["usuario"] = Session::get("usuario");
                $input["proyecto"] = $proyecto;

                $sql = "SELECT min(S.fecha) as fecha_primera_sesion, max(S.fecha) as fecha_ultima_sesion, max(S.fecha) - min(S.fecha) as diferencia
                        FROM sesiones_tarea_proyecto as S JOIN proyectos as P
                        ON S.idproyecto = P.idproyecto
                        WHERE P.nombre = :proyecto and P.usuario = :usuario";

                return $this->select($sql, $input);
	}
}
?>