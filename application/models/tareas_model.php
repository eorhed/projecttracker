<?php 
class Tareas_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Devuelve todas las tareas del usuario (si las hubiera)
	 * si no hay resultados se devuelve null
	 *
	 * @return array<string>|null
	 */
	function getTareas()
	{
		$input["usuario"] = Session::get("usuario");
		
        // Obtenemos las tareas creadas por el usuario en la BD
		$sql = "SELECT * FROM tareas WHERE usuario = :usuario";
		$data = $this->select($sql,$input);

		return $data;
	}

	
	/**
	 * Inserta una tarea(nombre,comentarios) en la tabla tareas de la BD
	 * 
	 * @param array tarea(nombre,comentarios,usuario,fecha_creacion)
	 * @return int codigo estado
	 */
	function agregarTarea($tarea)
	{
		// Primero comprobamos si la tarea de ese usuario existe ya en la BD
		$sql = "SELECT nombre FROM tareas WHERE usuario = :usuario AND nombre = :nombre";
		$data = $this->select($sql, array("usuario" => $tarea["usuario"], "nombre" => $tarea["nombre"]));

		// Si el usuario tiene la misma tarea en la BD devolvemos error 0
		// Si se ha producido un error un -1
		// Si no insertamos la tarea en la BD
		if (isset($data))
		{
			if (is_array($data))
				return State::NO_SUCCESS;
			else 
				return State::ERROR_DB;
		}
		else
			return $this->insert("tareas", $tarea);
	}


	/**
	 * Devuelve los datos de una determinada tarea
	 *
	 * @param int $tarea
	 * @return array<string>
	 */
	function getTarea($tarea)
	{
		$input["tarea"] = $tarea;
		$input["usuario"] = Session::get("usuario");

			
		$sql = "SELECT * FROM tareas
				WHERE usuario = :usuario and idtarea = :tarea";
		$datos = $this->select($sql, $input);

		return $datos;
	}


	/**
	 * Dados los datos de la tarea pasados por formulario 
	 * edita dichos datos en el registro de la tabla tareas de la BD
	 *
	 * @param array tarea (idtarea,nombre,comentarios,usuario)
	 * @return int codigo_estado_operacion
	 */
	function editarTarea($tarea)
	{
		$usuario = $tarea["usuario"];
		$idtarea = $tarea["idtarea"];
		$nombre = $tarea["nombre"];
		
		// Primero comprobamos que la tarea a editar pertenece a dicho usuario
		$sql = "SELECT nombre FROM tareas 
				WHERE usuario = :usuario and idtarea = :idtarea";
		$datos = $this->select($sql, array("usuario" => $usuario, "idtarea" => $idtarea));

		if (empty($datos))
			return State::ERROR_NO_PERMISSION;	// La tarea no pertenece al usuario o no existe
		else if ($datos == State::ERROR_DB)
			return State::ERROR_DB;	// Error consulta BD
		else
		{
			// Primero comprobamos si hay otra tarea que se llama igual que la editada
			$sql = "SELECT nombre FROM tareas 
					WHERE usuario = :usuario and nombre = :nombre and idtarea != :idtarea";
			$datos = $this->select($sql, array("usuario" => $usuario, "idtarea" => $idtarea, "nombre" => $nombre));

			// Si el usuario tiene la misma tarea en la BD devolvemos codigo estado 0
			if (!empty($datos))
			{
				if (is_array($datos))
					return State::NO_SUCCESS;
				else if ($datos === State::ERROR_DB)
					return State::ERROR_DB;
			}
			else
			{
				$input["nombre"] = $nombre;
				$input["comentarios"] = $tarea["comentarios"];

				$condiciones["idtarea"] = $idtarea;
				$condiciones["usuario"] = $usuario;

				return $this->update("tareas", $input, $condiciones);
			}
		}
	}


	/**
	 * Pone a 1 el campo habilitado de la tarea seleccionada,
	 * es decir, habilita la tarea en cuestión
	 *
	 * @param array tarea (idtarea,nombre,comentarios,usuario)
	 * @return int codigo_estado_operacion
	 */
	function habilitarTarea($tarea)
	{
		$input["idtarea"] = $tarea["idtarea"];
		$input["usuario"] = $tarea["usuario"];

		// Primero comprobamos si la tarea existe en la BD y pertenece al usuario que solicita habilitarla
		$sql = "SELECT idtarea FROM tareas 
				WHERE usuario = :usuario and idtarea = :idtarea";
			
		$datos = $this->select($sql, $input);
			
		// Si el usuario no tiene esa tarea o no existe en la BD devolvemos 0 (error)
		if (!isset($datos))
			return State::NO_SUCCESS;  // No existe o no pertenece al usuario dicha tarea
		else
		{
			// Actualizamos en la BD la tarea poniendo habilitado = 1
			$condiciones = $input;
			return $this->update("tareas", array("habilitado" => "1"), $condiciones);
		}
	}


	/**
	 * Pone a 0 el campo habilitado de la tarea seleccionada,
	 * es decir, deshabilita la tarea en cuestión
	 *
	 * @param array tarea (idtarea,nombre,comentarios,usuario)
	 * @return int codigo_estado_operacion
	 */
	function deshabilitarTarea($tarea)
	{
		$input["idtarea"] = $tarea["idtarea"];
		$input["usuario"] = $tarea["usuario"];

		// Primero comprobamos si la tarea existe en la BD y pertenece al usuario que solicita habilitarla
		$sql = "SELECT idtarea FROM tareas 
				WHERE usuario = :usuario and idtarea = :idtarea";
			
		$datos = $this->select($sql, $input);
			
		// Si el usuario no tiene esa tarea o no existe en la BD devolvemos 0 (error)
		if (!isset($datos))
			return State::NO_SUCCESS;  // No existe o no pertenece al usuario dicha tarea
		else
		{
			// Actualizamos en la BD la tarea poniendo habilitado = 0
			$condiciones = $input;
			return $this->update("tareas", array("habilitado" => "0"), $condiciones);
		}
	}
}
?>