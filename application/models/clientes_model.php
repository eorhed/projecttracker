<?php
class Clientes_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Devuelve la lista de clientes del usuario logueado
	 *
	 * @return array<string> $data Lista de clientes del usuario 
	 */
	function getListaClientes()
	{
		$usuario = Session::get("usuario");

		// Primero comprobamos que dicho cliente no existe ya en la BD
		$sql = "SELECT * FROM clientes WHERE cliente_de = :usuario and habilitado = '1'";

		return $this->select($sql, array("usuario" => $usuario));
	}

	/**
	 * Añade un cliente junto con sus datos a la tabla clientes de la BD
	 *
	 * @return int $estado Estado de la operacion de insercion
	 */
	function agregarCliente()
	{
		//Limpiamos los datos pasados del formulario
		$input = array(
						"contacto" => filter_input(INPUT_POST, "contacto", FILTER_SANITIZE_STRING),
						"empresa" => filter_input(INPUT_POST, "empresa", FILTER_SANITIZE_STRING),
						"email" => filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL),
						"telefono" => filter_input(INPUT_POST, "telefono", FILTER_SANITIZE_STRING),
						"direccion" => filter_input(INPUT_POST, "direccion", FILTER_SANITIZE_STRING),
						"municipio" => filter_input(INPUT_POST, "municipio", FILTER_SANITIZE_STRING),
						"cliente_desde" => date("Y-m-d H:i:s"),
						"cliente_de" => Session::get("usuario"),
			);

		if (!empty($input["contacto"]))
		{

			$usuario = Session::get("usuario");

			// Primero comprobamos que dicho cliente no existe ya en la BD
			$sql = "SELECT * FROM clientes 
					WHERE cliente_de = :usuario AND contacto = :contacto AND empresa = :empresa";
			$data = $this->select($sql, array
											("usuario" => $usuario,
											"contacto" => $input["contacto"],
											"empresa" => $input["empresa"]
											));

					
			if (isset($data))
			{
				if (is_array($data))
					return State::NO_SUCCESS;	// Existe un cliente del usuario con el mismo nombre
				else
					return State::ERROR_DB;	// En caso de error
			}
			else
				return $this->insert("clientes", $input);
		}
		
	}

	/**
	 * Devuelve todos los datos del cliente pasado como parámetro
	 *
	 * @param string $cliente Nombre del cliente
	 * @return array<string> Informacion de contacto del cliente
	 */
	function getCliente($cliente)
	{
		$input["cliente"] = $cliente;
		$input["usuario"] = Session::get("usuario");

		// Primero comprobamos que dicho usuario no existe ya en la BD
		$sql = "SELECT * FROM clientes WHERE contacto = :cliente AND cliente_de = :usuario";
		
		return $this->select($sql, $input);
	}

	 /**
	  * Edita los datos de un cliente en la BD
	  *
	  * @return int $estado Estado de la operacion
	  */
	function editarCliente()
	{
		
		//Limpiamos los datos pasados del formulario
		$input = array(
				"nombre_nuevo" => filter_input(INPUT_POST, "contacto", FILTER_SANITIZE_STRING),
				"contacto" => filter_input(INPUT_POST, "nombre_anterior", FILTER_SANITIZE_STRING),
				"empresa" => filter_input(INPUT_POST, "empresa", FILTER_SANITIZE_STRING),
				"email" => filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL),
				"telefono" => filter_input(INPUT_POST, "telefono", FILTER_SANITIZE_STRING),
				"direccion" => filter_input(INPUT_POST, "direccion", FILTER_SANITIZE_STRING),
				"municipio" => filter_input(INPUT_POST, "municipio", FILTER_SANITIZE_STRING),
				"cliente_de" => Session::get("usuario"),
				"idcliente" => filter_input(INPUT_POST, "idcliente", FILTER_SANITIZE_STRING)
			);

		// Primero comprobamos que el nombre introducido no pertenece a otro cliente del usuario
		$sql = "SELECT * FROM clientes 
				WHERE cliente_de = :cliente_de AND contacto = :nombre_nuevo 
				AND empresa = :empresa AND idcliente != :idcliente";
			
		$datos = array
					("cliente_de" => $input["cliente_de"],
					"nombre_nuevo" => $input["nombre_nuevo"],
					"empresa" => $input["empresa"],
					"idcliente" => $input["idcliente"]
					);

		$data = $this->select($sql, $datos);


		if (isset($data))
		{
			if (is_array($data))
				return State::NO_SUCCESS; // Existe un cliente igual de este usuario en la BD
			else if ($data === State::ERROR_DB)
				return State::ERROR_DB;
		}
		else
		{
			$sql = "UPDATE clientes 
					SET contacto = :contacto, empresa = :empresa, email = :email, telefono = :telefono, 
					direccion = :direccion, municipio = :municipio 
					WHERE cliente_de = :cliente_de AND contacto = :nombre_anterior";

			$datos = array
						("contacto" => $input["nombre_nuevo"],
						"empresa" => $input["empresa"],
						"email" => $input["email"],
						"telefono" => $input["telefono"],
						"direccion" => $input["direccion"],
						"municipio" => $input["municipio"]);
					
			$condiciones = array(
							"cliente_de" => $input["cliente_de"],
							"idcliente" => $input["idcliente"]);
					
			return $this->update("clientes", $datos, $condiciones);
		}
	}

	/**
	  * Elimina un determinado cliente del usuario
	  *	poniendo campo habilitado a 0 y sustituyendo sus datos por el caracter "-"
	  *
	  * @return int $estado Estado de la operacion
	  */
	function eliminarCliente($idcliente)
	{
		if (!empty($idcliente))
		{
			$datos = array
				   ("empresa" => "-",
					"contacto" => "-", 
					"email" => "-", 
					"telefono" => "-", 
					"direccion" => "-", 
					"municipio" => "-", 
					"cliente_desde" => "0000-00-00 00:00:00", 
					"habilitado" => "0");

		
			$condiciones["cliente_de"] = Session::get("usuario");
			$condiciones["idcliente"] = $idcliente;

			return $this->update("clientes", $datos, $condiciones);
		}
		else
			return State::ERROR_DB;		
	}
}
?>