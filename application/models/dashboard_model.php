<?php 
class Dashboard_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}

	function getNumClientesUsuario()
	{
		$usuario = Session::get("usuario");

		$sql = "SELECT COUNT(idcliente) AS num_clientes FROM clientes WHERE cliente_de = :usuario AND habilitado = 1";
		return $this->select($sql, array('usuario' => $usuario));
	}

	function getNumProyectosUsuario()
	{
		$usuario = Session::get("usuario");

		$sql = "SELECT COUNT(idproyecto) as num_proyectos FROM proyectos WHERE usuario = :usuario AND habilitado = 1";
		return $this->select($sql, array('usuario' => $usuario));
	}

	function getNumTareasUsuario()
	{
		$usuario = Session::get("usuario");

		$sql = "SELECT COUNT(idtarea) as num_tareas FROM tareas WHERE usuario = :usuario";
		return $this->select($sql, array('usuario' => $usuario));
	}

	function getNumHorasTotalesYDineroGanado()
	{
		$usuario = Session::get("usuario");

		$sql = "SELECT SUM(s.duracion_segs) AS duracion_segs, SUM(s.duracion_segs / 3600 * s.precio_x_hora) AS 	total_ganado FROM proyectos p JOIN sesiones_tarea_proyecto s
				ON p.idproyecto = s.idproyecto
				WHERE p.usuario = :usuario";
		return $this->select($sql, array('usuario' => $usuario));
	}

    // function getProyectosDelUsuario()
	// {
	// 	$usuario = Session::get("usuario");

    //     // Obtenemos las tareas creadas por el usuario en la BD
    //     $sql = "SELECT DISTINCT P.idproyecto, P.nombre 
    //             FROM proyectos as P JOIN tareas as T
    //             WHERE P.usuario = :usuario and P.fecha_fin = '0000-00-00 00:00:00' and P.habilitado = '1'";
            
    //     return $this->select($sql, array('usuario' => $usuario));
	// }
}
?>