<?php 
class Registrar_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}


	/**
	 * Si no existe ese usuario, inserta un nuevo usuario en tabla usuarios de la BD
	 *
	 * @return int $codigo_estado
	 */
	function run()
	{
		
		// Limpiamos los datos del formulario
		$input = array (
						"usuario" => filter_var($_POST["usuario"],FILTER_SANITIZE_STRING),
						"clave" => filter_var($_POST["clave"],FILTER_SANITIZE_STRING),
						"email" => filter_var($_POST["email"],FILTER_SANITIZE_EMAIL)
						);

		//Generamos el hash de 60 caracteres para la clave
		$options = ["cost" => "12"];
		$claveHash = password_hash($input["clave"], PASSWORD_BCRYPT, $options);

		// Primero comprobamos que dicho usuario no existe ya en la BD
		$sql = "SELECT * FROM usuarios WHERE usuario = :usuario";
		$data = $this->select($sql, array('usuario' => $input["usuario"]));

		if (isset($data))
		{
			if (is_array($data))
				return 0;	// Se ha encontrado usuario
			else
				return -1;	// Error al operar con BD
		}
		else
		{
				// Insertamos el nuevo usuario en la BD
				$fecha_registro = date("Y-m-d");
				$datos = array(
							'usuario' => $input["usuario"],
							'clave' => $claveHash, 
							'email' => $input["email"], 
							'fecha_registro' => $fecha_registro);

				$estado = $this->insert("usuarios", $datos);

				// Si se ha creado bien el usuario en la BD:
				// Creamos el cliente que es el propio usuario, para proyectos personales
				// Si no, devolvemos error -1
				if ($estado == 1)
				{
					$datos = array('contacto' => $input["usuario"],
								   'cliente_de' => $input["usuario"],
								   'cliente_desde' => $fecha_registro,
								   'empresa' => 'Uso personal');

					return $this->insert("clientes", $datos);
				}
				else
					return -1;
		}
	}
}
?>