<?php 
class Trackear_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}

    function getProyectosDelUsuario()
	{
		$usuario = Session::get("usuario");

        // Obtenemos las tareas creadas por el usuario en la BD
        $sql = "SELECT DISTINCT P.idproyecto, P.nombre 
                FROM proyectos as P JOIN tareas as T
                WHERE P.usuario = :usuario and P.fecha_fin = '0000-00-00 00:00:00' and P.habilitado = '1'";
            
        return $this->select($sql, array('usuario' => $usuario));
	}
}

	
?>