<?php 
class Identificarse_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}

	function run()
	{
		// Cogemos y limpiamos los datos del formulario
		$input["usuario"] = filter_var($_POST["usuario"],FILTER_SANITIZE_STRING);

		// Primero comprobamos que dicho usuario existe en la BD
		$sql = "SELECT * FROM usuarios WHERE usuario = :usuario";
		$data = $this->select($sql, $input);
		
		$input["clave"] = filter_var(trim($_POST["clave"]));
		if (isset($data))
		{
			// Si el usuario existe y la clave coincide con el hash almacenado en la BD, actualizamos la sesión
			if (is_array($data) && count($data) == 1 && password_verify($input["clave"], $data[0]["clave"]))
			{
				$datos["ultima_sesion"] = date("Y-m-d H:i:s");
				$condiciones["usuario"] = $input["usuario"];
					
				$estado = $this->update("usuarios", $datos, $condiciones);

				if ($estado == 1)
					return $data;
				else
					return -1;
			}
			else
				return -1;
		}
		else
			return null;
	}
}
?>