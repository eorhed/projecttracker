<?php 
class Preferencias_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Devuelve las preferencias del usuario en la app
	 *
	 * @return array $codigo_estado
	 */
	function index()
	{
		
	}

	/**
	 * Actualiza las preferencias del usuario en la BD y devuelve si la operación
	 * se ha producido con éxito o si se ha producido un error durante la misma
	 *
	 * @return int $codigo_estado
	 */
	function getPreferenciasUsuario()
	{
		$usuario = Session::get("usuario");

		// Obtenemos los proyectos del usuario de la BD
		$sql = "SELECT usuario, email, foto, precio_hora, moneda, tiempo_max_sesion, idioma
				FROM usuarios
				WHERE usuario = :usuario";

		return $this->select($sql, array('usuario' => $usuario));
	}

	/**
	 * Actualiza las preferencias del usuario en la BD y devuelve si la operación
	 * se ha producido con éxito o si se ha producido un error durante la misma
	 *
	 * @param array $preferencias preferencias del usuario
	 * @return int $codigo_estado true si se han actualizado correctamente los datos en la BD
	 */
	function actualizarDatosUsuario($preferencias)
	{
		$datos = array(
						"email" => $preferencias["email"]["valor"],
						"precio_hora" => $preferencias["precio_hora"]["valor"],
						"moneda" => $preferencias["moneda"]["valor"],
						"tiempo_max_sesion" => $preferencias["tiempo_max_sesion"]["valor"],
						"idioma" => $preferencias["idioma"]["valor"],
					  );

		$condiciones = array("usuario" => $preferencias["usuario"]["valor"]);
		$estado = $this->update("usuarios", $datos, $condiciones);

		$warning = "";
		if (!empty($preferencias["foto_perfil"]["valor"]["name"]))
		{
			$fichero_foto = $preferencias["foto_perfil"]["valor"];
			//$nombre_foto = $fichero_foto["name"];
			$tipo_archivo = $fichero_foto["type"];
			
			if ($tipo_archivo == "image/jpeg" || $tipo_archivo == "image/png")
			{
				$extension = ($tipo_archivo == "image/jpeg") ? ".jpg" : ".png";
				$archivo = Session::get('usuario') . $extension;

					    
				if (is_uploaded_file($fichero_foto['tmp_name']))
				{
					$ruta_destino = $_SERVER["DOCUMENT_ROOT"]."/projecttracker/application/public/img/user/".$archivo;
					if (!move_uploaded_file($fichero_foto['tmp_name'], $ruta_destino))
				 		$warning = msj_error_subir_foto;
				}

				$datos = array("foto" => $archivo);
				$condiciones = array("usuario" => $preferencias["usuario"]["valor"]);
				$estado = $this->update("usuarios", $datos, $condiciones);

				if ($estado === -1)
					$warning = msj_error_fichero_ok_db_error;
			}
			else
				$warning = msj_error_formato_invalido_invalido;
		}

		return $warning;
	}
}
?>