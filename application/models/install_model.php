<?php 
class Install_Model extends Model
{
	function __construct()
	{
		
	}
	function run()
	{}
	
	function install($datos_db)
	{
		try
		{
			# Llamamos a Database para que abra una conexion con la DB
			$datos_conexion = "{$datos_db["tipoServidorDB"]}:host={$datos_db["servidor"]};port={$datos_db["puerto"]}";
			$db = new PDO($datos_conexion,$datos_db["usuario"],$datos_db["clave"]);

			# Con los datos facilitados creamos la BD de projecttracker
			$sql = "CREATE DATABASE IF NOT EXISTS `$dbname` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
					USE `$dbname`;
					CREATE TABLE `clientes` (
						`idcliente` int(5) NOT NULL,
						`empresa` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
						`contacto` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
						`email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
						`telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
						`direccion` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
						`municipio` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
						`cliente_desde` datetime NOT NULL,
						`cliente_de` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
						`habilitado` tinyint(4) NOT NULL DEFAULT '1'
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

					CREATE TABLE `proyectos` (
						`idproyecto` int(11) NOT NULL,
						`nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
						`usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
						`cliente` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
						`comentarios` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
						`fecha_inicio` datetime NOT NULL,
						`fecha_fin` datetime NOT NULL,
						`precio_x_hora` float NOT NULL DEFAULT '25',
						`precio_estimado` float NOT NULL,
						`horas_estimadas` int(3) NOT NULL,
						`habilitado` tinyint(4) NOT NULL DEFAULT '1'
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

					CREATE TABLE `sesiones_tarea_proyecto` (
						`idtarea_proyecto` int(11) NOT NULL,
						`idtarea` int(11) NOT NULL,
						`idproyecto` int(11) NOT NULL,
						`fecha` datetime NOT NULL,
						`comentarios` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
						`precio_x_hora` int(11) NOT NULL,
						`duracion` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
						`duracion_segs` int(11) NOT NULL,
						`usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

					CREATE TABLE `tareas` (
						`idtarea` int(11) NOT NULL,
						`usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
						`nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,`precio_x_hora` float NOT NULL DEFAULT '25',
						`habilitado` tinyint(1) NOT NULL DEFAULT '1',
						`fecha_creacion` datetime NOT NULL,
						`comentarios` varchar(300) COLLATE utf8_spanish_ci NOT NULL
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
					
					CREATE TABLE `tareas_proyecto` (
						`idtarea_proyecto` int(11) NOT NULL,
						`idtarea` int(11) NOT NULL,
						`idproyecto` int(11) NOT NULL,
						`asignado` tinyint(1) NOT NULL DEFAULT '1'
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

					CREATE TABLE `usuarios` (
						`idusuario` int(5) NOT NULL,
						`usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
						`clave` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
						`fecha_registro` date NOT NULL,
						`ultima_sesion` datetime NOT NULL,
						`email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
						`foto` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'default.png'
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

						ALTER TABLE `clientes`
						ADD PRIMARY KEY (`idcliente`);

						ALTER TABLE `proyectos`
						ADD PRIMARY KEY (`idproyecto`);

						ALTER TABLE `sesiones_tarea_proyecto`
						ADD PRIMARY KEY (`idtarea_proyecto`);

						ALTER TABLE `tareas`
						ADD PRIMARY KEY (`idtarea`);

						ALTER TABLE `tareas_proyecto`
						ADD PRIMARY KEY (`idtarea_proyecto`);

						ALTER TABLE `usuarios`
						ADD PRIMARY KEY (`idusuario`);

						ALTER TABLE `clientes`
						MODIFY `idcliente` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

						ALTER TABLE `proyectos`
						MODIFY `idproyecto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

						ALTER TABLE `sesiones_tarea_proyecto`
						MODIFY `idtarea_proyecto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

						ALTER TABLE `tareas`
						MODIFY `idtarea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

						ALTER TABLE `tareas_proyecto`
						MODIFY `idtarea_proyecto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

						ALTER TABLE `usuarios`
						MODIFY `idusuario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

						
					";

				
			$stmt = $db->prepare($sql);
			// $datos = array("db" => $input["nombreDB"], "db2" => $input["nombreDB"]);
			$stmt->execute();


			# Creamos el usuario anonimo(select,insert) para que los usuarios de forma indirecta obtengan e inserten datos en la BD
			$sql = "CREATE USER 'PT_anonymous'@:servidor IDENTIFIED BY :clave;
					GRANT SELECT, INSERT ON `$dbname`.* TO 'PT_anonymous'@:servidor2;
					FLUSH PRIVILEGES;";

			$options = ["cost" => "12"];
			$claveHash = password_hash(time(), PASSWORD_BCRYPT, $options);
			$claveHash = substr($claveHash,7,8);
			

			$stmt = $db->prepare($sql);
			$datos = array("clave" => $claveHash, "servidor" => $input["servidor"], "servidor2" => $input["servidor"]);
			$stmt->execute($datos);

			# Construimos un nuevo archivo de configuración
			$fichero = fopen("application/config/database.ini.php","w");
			$datos = ";<?php\n;/*\n[configBD]\ndb_driver=\"{$input["tipo_servidor"]}\"\ndb_host=\"{$input["servidor"]}\"\ndb_name=\"{$dbname}\"\ndb_user=\"{$input["usuario"]}\"\ndb_password=\"{$input["clave"]}\"\ndb_anom_user=\"PT_anonymous\"\ndb_anom_password=\"$claveHash\"\n;?>";
			fwrite($fichero, $datos);
			fclose($fichero);

			// die($claveHash);

			return 1;
		}
		catch(Exception $e)
		{
			if (class_exists("MiLog"))
				MiLog::getInstance()->add($e->getMessage());	// Enviamos el mensaje de error a la cola de errores de la clase MiLog
			return 0;
		}
	}
}
?>