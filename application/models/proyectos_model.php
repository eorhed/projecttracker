<?php 
/**
 * @autor Eriz Villalba <eorhed@gmail.com>
 */

/**
 * Clase que se encarga de gestionar los datos de los proyectos del usuario en la BD
 */
class Proyectos_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Devuelve los datos de los proyectos(nombres de clientes incluidos) del usuario
	 *
	 * @return void
	 */
	public function getListaProyectos()
	{
		$usuario = Session::get("usuario");

		// Obtenemos los proyectos del usuario de la BD
		$sql = "SELECT P.nombre,C.contacto,P.fecha_inicio,P.fecha_fin 
				FROM proyectos as P JOIN clientes as C
				WHERE usuario = :usuario and P.cliente = C.idcliente and P.habilitado = '1'";

		return $this->select($sql, array("usuario" => $usuario));
	}

	
	/**
	 * Devuelve los clientes del usuario
	 *
	 * @return array $clientes | -1 | null
	 */
	public function getClientesDelUsuario()
	{
		
		$usuario = Session::get("usuario");

		// Obtenemos los clientes del usuario logueado
		$sql = "SELECT idcliente, contacto FROM clientes WHERE cliente_de = :usuario and habilitado = '1'";

		return $this->select($sql, array("usuario" => $usuario));
	}


	/** 
	 *	Inserta el nuevo proyecto del usuario junto con sus datos en la tabla proyectos de la BD
	 *  y devuelve codigo de estado en función de si se ha podido realizar (o no) la operación correctamente
	 * 
	 *  @param void
	 *	@return int codigo_estado
	 */
	public function agregarProyecto($proyecto)
	{
		// Primero comprobamos si el usuario ya tiene un proyecto en la BD con el mismo nombre
		$sql = "SELECT nombre FROM proyectos WHERE usuario = :usuario and nombre = :nombre";
		$data = $this->select($sql, array("usuario" => $proyecto["usuario"], "nombre" => $proyecto["nombre"]));
		

		// Si se han encontrado resultados en la BD, devolvemos codigo error 0
		// Si no existe el proyecto del usuario entonces lo insertamos en la BD
		if (isset($data))
		{
			if (is_array($data))
				return 0;
			else if ($data == -1)
				return -1;	// En caso de error interno al operar
		}
		else
			return $this->insert("proyectos", $proyecto);	// Devuelve 1 si ha hecho bien el insert, -1 en caso de error
	}


	/** 
	 *	Devuelve los datos del proyecto pasado como parametro
	 *
	 *  @param string $nombre_proyecto
	 */
	 public function getProyecto($proyecto)
	 {
		$usuario = Session::get("usuario");

		// Primero comprobamos que dicho usuario no existe ya en la BD
		$sql = "SELECT P.idproyecto, P.nombre, C.contacto, P.comentarios, P.fecha_inicio, P.fecha_fin
				FROM proyectos as P JOIN clientes as C
				ON P.cliente = C.idcliente
				WHERE C.cliente_de = :usuario and P.usuario = :usuario2 and P.nombre = :proyecto and P.habilitado = '1'";
		$input = array("usuario" => $usuario, "usuario2" => $usuario, "proyecto" => $proyecto);

		return $this->select($sql,$input);
	 }

	
	/** 
	 *	Devuelve todas las tareas que todavía NO están asignadas al proyecto pasado como parámetro
	 *
	 *  @param string $nombre_proyecto
	 */
	public function getTareasNoAsignadasAlProyecto($proyecto)
	{
		$usuario = Session::get("usuario");

        // Obtenemos las tareas creadas por el usuario en la BD que no tiene asignadas
		$sql = "SELECT idtarea,nombre,comentarios,fecha_creacion 
					FROM tareas 
					WHERE usuario = :usuario
					AND habilitado = '1'
					AND idtarea NOT IN (SELECT idtarea 
						   FROM tareas_proyecto as TP JOIN proyectos as P 
						   WHERE TP.asignado = '1' AND P.usuario = :usuario2 AND P.nombre = :proyecto AND TP.idproyecto = P.idproyecto AND P.habilitado = '1')";

			
		$input = array("usuario" => $usuario, "usuario2" => $usuario, "proyecto" => $proyecto);

		return $this->select($sql,$input);
	}


	/** 
	 *	Devuelve todas las tareas asignadas al proyecto pasado como parametro
	 *
	 *  @param type String nombre_proyecto
	 */
	public function getTareasDelProyecto($proyecto)
	{
		$usuario = Session::get("usuario");

		// Obtenemos las tareas asignadas al proyecto
		$sql = "SELECT TP.idtarea_proyecto, T.nombre, T.comentarios
				FROM tareas_proyecto as TP JOIN proyectos as P JOIN tareas as T
				WHERE TP.asignado = '1' AND TP.idtarea = T.idtarea and TP.idproyecto = P.idproyecto and P.usuario = :usuario
				AND TP.idproyecto IN (SELECT idproyecto FROM proyectos WHERE usuario = :usuario2 AND nombre = :proyecto)";

			
		$input = array("usuario" => $usuario, "usuario2" => $usuario, "proyecto" => $proyecto);

		return $this->select($sql,$input);
	 }


	/** 
	 *	Edita en la BD los datos de un proyecto concreto
	 * y devuelve codigo de estado en función de si se ha podido realizar (1) la operación correctamente o no (0 o -1)
	 *  
	 * @return int codigo_estado
	 */
	public function editarProyecto()
	{
		//Limpiamos los datos pasados del formulario
		$input["nombre"] = filter_input(INPUT_POST, "nombre", FILTER_SANITIZE_FULL_SPECIAL_CHARS);  
		$input["cliente"] = filter_input(INPUT_POST, "cliente", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		$input["comentarios"] = filter_input(INPUT_POST, "comentarios", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		$input["idproyecto"] = filter_input(INPUT_POST, "idproyecto", FILTER_SANITIZE_NUMBER_INT);
		$input["usuario"] = Session::get("usuario");



		// Comprobamos si ya existe otro proyecto del usuario con el mismo nombre que el introducido
		$sql = "SELECT nombre FROM proyectos WHERE usuario = :usuario and nombre = :nombre and idproyecto != :idproyecto";
		$datos = array("usuario" => $input["usuario"], "nombre" => $input["nombre"], "idproyecto"=>$input["idproyecto"]);
		$data = $this->select($sql, $datos);


		// Si el usuario ya tiene otro proyecto con el nombre elegido devolvemos 0
		// Si no hay otro proyecto con el nombre elegido, actualizamos
		if (isset($data))
		{
			if (is_array($data))
				return 0;	// El proyecto ya existe
			else
				return -1;	// Error al operar en la BD
		}
		else
		{
			$datos2 = array("nombre" => $input["nombre"], "cliente" => $input["cliente"], "comentarios" => $input["comentarios"]);
			$condiciones = array("idproyecto" => $input["idproyecto"], "usuario" => $input["usuario"]);

			return $this->update("proyectos", $datos2, $condiciones);	// 1 si hace bien el update, -1 en caso de error
		}
	}


	/**
	 * Función que marca como finalizado/cerrado un proyecto,
	 * devuelve código de estado en función de si se ha podido realizar (1) la operación correctamente o no (0 o -1)
	 *
	 * @param string $proyecto
	 * @return int codigo_estado
	 */ 
	public function finalizar($proyecto)
	{
		// Consultamos si ya se encuentra cerrado el proyecto
		$sql = "SELECT nombre FROM proyectos WHERE fecha_fin = '0000-00-00 00:00:00' AND usuario = :usuario AND nombre = :proyecto";
		$data = $this->select($sql,array
			("usuario" => Session::get("usuario"),
			 "proyecto" => $proyecto
			));


		// Si el proyecto esta abierto lo cerramos y devolvemos el codigo de estado 1
		// Sino, el proyecto ya estaba finalizado, luego devolvemos el codigo de estado 0
		if (isset($data))
		{
			if (is_array($data))
			{
				$input["fecha_actual"] = date("Y-m-d H:i:s");
				$input["usuario"] = Session::get("usuario");

				// Actualizamos marcando la fecha de fin en el registro del proyecto
				$datos = array("fecha_fin" => $input["fecha_actual"]);
				$condiciones = array("usuario" => $input["usuario"], "nombre" => $proyecto);
				return $this->update("proyectos", $datos, $condiciones);	// 1 si hace bien el update, -1 si error
			}
			else
				return -1;
		}
		else
			return 0;	// No se ha efectuado ninguna operacion porque ya estaba el proyecto cerrado
	}


	 /**
	  * Función que dado un proyecto pasado como parámetro lo marca como abierto en la BD,
	  * devuelve código de estado en función de si se ha podido realizar (1) la operación correctamente o no (0 o -1)
	  *
	  * @param string $proyecto
	  * @return int codigo_estado
	  */ 
	 public function reabrir($proyecto)
	 {
		// Consultamos si ya se encuentra abierto el proyecto
		$sql = "SELECT nombre FROM proyectos WHERE fecha_fin NOT LIKE '0000-00-00 00:00:00' AND usuario = :usuario AND nombre = :proyecto";
		$data = $this->select($sql, array
			("usuario" => Session::get("usuario"),
			 "proyecto" => $proyecto
			));


		// Si el proyecto está cerrado lo abrimos y devolvemos el codigo de estado 1
		// Sino, el proyecto ya estaba abierto, luego devolvemos el codigo de estado 0
		if (isset($data))
		{
			if (is_array($data))
			{
				$input["fecha_actual"] = "0000-00-00 00:00:00";
				$input["usuario"] = Session::get("usuario");

				// Actualizamos marcando la fecha de fin en el registro del proyecto
				$estado = $this->update("proyectos", array("fecha_fin" => $input["fecha_actual"]),
										array("usuario" => $input["usuario"], "nombre" => $proyecto));

				return $estado;
			}
			else
				return -1;
		}
		else
			return 0;	// No se ha efectuado ninguna operacion porque ya estaba el proyecto abierto
	 }


	 /**
	  * Función que dado un proyecto pasado como parámetro lo elimina de la BD (habilitado = 0),
	  * devuelve código de estado en función de si se ha podido realizar (1) la operación correctamente o no (0 o -1)
	  *
	  * @param string $proyecto
	  * @return int codigo_estado
	  */ 
	 public function eliminarProyecto($proyecto)
	 {
		// Consultamos si el usuario tiene el proyecto
		$sql = "SELECT nombre, idproyecto FROM proyectos WHERE usuario = :usuario and nombre = :proyecto";
		$data = $this->select($sql,array
									("usuario" => Session::get("usuario"),
									 "proyecto" => $proyecto
							 ));


		// Si se encuentra el proyecto del usuario y lo marcamos como deshabilitado y devolvemos codigo de estado 1
		// Sino, el proyecto del usuario no se encuetra, devolvemos el codigo de error 0
		if (!empty($data))
		{
			if (is_array($data))
			{
				$idproyecto = $data[0]["idproyecto"];

				$estado = $this->delete("proyectos", array("idproyecto" => $idproyecto));

				$estado2 = $this->delete("tareas_proyecto", array("idproyecto" => $idproyecto));

				$estado3 = $this->delete("sesiones_tarea_proyecto", array("idproyecto" => $idproyecto));


				if ($estado == 1 && $estado2 == 1 && $estado3 == 1)
					return $estado;  // Todo ha ido bien, devolvemos un 1
				else
					return -1;	// Uno o varios procesos de borrado a fallado
			}
			else
				return -1;
		}
		else
			return 0;	// No se ha efectuado ninguna operacion porque no se ha encontrado ningun resultado
	 }


	/**
	 * Función que dado un array de tareas las asigna al proyecto pasado como parametro
	 * devuelve código de estado en función de si se ha podido realizar la operación correctamente (1) o no (0 o -1)
	 *
	 * @param array<String> $tareas_a_asignar
	 * @param string $proyecto
	 * @return int codigo_estado
	 */ 
	public function asignarTareasAlProyecto($tareas_a_asignar, $proyecto)
	{
		$usuario = Session::get("usuario");

		$sql = "SELECT idproyecto FROM proyectos WHERE usuario = :usuario AND nombre = :proyecto";
		$data = $this->select($sql, array
				("usuario" => $usuario,
				 "proyecto" => $proyecto
				));

		if (isset($data))
		{
			if (is_array($data))
			{
				$idproyecto = $data[0]["idproyecto"];

				foreach ($tareas_a_asignar as $idtarea)
				{
					$estado = 1;

					// Buscamos si la tarea ya se encuentra asignada a ese proyecto

					$sql = "SELECT TP.idtarea_proyecto as idtarea_proyecto
							FROM tareas_proyecto as TP JOIN tareas as T 
							ON TP.idtarea = T.idtarea 
							WHERE TP.idtarea = :idtarea AND TP.idproyecto = :idproyecto AND T.usuario = :usuario";
							
					$data2 = $this->select($sql,array("idtarea" => $idtarea, "idproyecto" => $idproyecto, "usuario" => $usuario));
					
					// Si la tarea existe actualizamos su campo habilitado a 1
					// Si no existe creamos un nuevo registro en la tabla tareas-proyecto en la BD
					if (isset($data2))
					{
						if (is_array($data2))
						{
							$idtarea_proyecto = $data2[0]["idtarea_proyecto"];
							$datos = array("asignado" => 1);
							$condiciones = array("idtarea_proyecto" => $idtarea_proyecto);
							$estado = $this->update("tareas_proyecto", $datos, $condiciones);


							if ($estado === -1)
							{
								return -1;	// Error al operar con la BD
								break;
							}
						}
						else
						{
							return -1;	// Error al operar con la BD
							break;
						}
					}
					else
					{
						$datos = array("idtarea" => $idtarea, "idproyecto" => $idproyecto);
						$estado = $this->insert("tareas_proyecto", $datos);
							
						if ($estado === -1)
						{
							break;
							return -1;	// Error al operar con la BD
						}
					}
				}

				return 1;	// Se realiza bien la operación
			}
			else
				return -1; // Error al operar con la BD
		}
		else
			return 0;	// No se ha encontrado el proyecto
	}

	/**
	 * Desasigna la tarea del proyecto de la tabla tareas_proyecto
	 * devuelve código de estado en función de si se ha podido realizar (1) la operación correctamente o no (-1)
	 *
	 * @param string $tareas_a_asignar
	 * @param string $proyecto
	 * @return int codigo_estado
	 */
	public function desasignarTareaAlProyecto($idtareaproyecto_asignada,$proyecto)
	{
		// Ponemos a 0 el campo asignado del registro en cuestión de la tabla tareas_proyecto
		$datos = array("asignado" => "0");
		$condiciones = array("idtarea_proyecto" => $idtareaproyecto_asignada);
			
		return $this->update("tareas_proyecto", $datos, $condiciones);	// 1 si hace bien el update, -1 si error
	}
}
?>