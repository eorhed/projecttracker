<?php 
class Sesiones_Model extends Model
{
	function __construct()
	{
		parent::__construct();
	}

	function run()
	{
		
	}

    /**
     * Devuelve la lista de tareas creadas por el usuario en la BD
     *
     * @return array
     */
    function getSesiones()
	{
		$input["usuario"] = Session::get("usuario");
        
        $sql = "SELECT P.nombre as nombre_proyecto, T.nombre as nombre_tarea, S.duracion, S.duracion_segs, S.fecha, S.precio_x_hora 
                FROM sesiones_tarea_proyecto as S JOIN proyectos as P JOIN tareas as T 
                ON S.idtarea = T.idtarea and P.idproyecto = S.idproyecto 
                WHERE P.usuario = :usuario
                ORDER BY P.nombre ASC, S.fecha ASC";

        return $this->select($sql,$input);
    }
}

	
?>