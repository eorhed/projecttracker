<main>
  <div class="container">
    <div class="alert alert-info" role="alert" style="margin-top: 100px;">
        <img src="<?php Utils::print(BASE_URL) ?>application/public/img/cargando.gif">
        <h2><?php !empty($this->msj) ? Utils::print($this->msj) : null; ?></h2>
    </div>
</main>

<meta http-equiv='refresh' content='2;URL=index'>