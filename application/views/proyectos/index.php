<main>
    <section id="contenido">
        <div class="container">
                    <div class="row">
                        <div class="col-lg-9">
                            <h1><i class="fa fa-file-code"></i> Proyectos</h1>
                        </div>
                        <div class="col-lg-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb navegacion">
                                    <li class="breadcrumb-item">
                                        <i class="fa fa-home"></i>
                                        <a href="<?php Utils::print(BASE_URL) ?>dashboard"> Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Proyectos</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="btn-toolbar toolbar" role="toolbar">
                                <button class="btn btn-default btn-primary" onclick="document.getElementById('agregar-proyecto').style.display = 'block';">
                                    <i class="fa fa-plus"></i> Agregar nuevo proyecto
                                </button>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h2>Lista de proyectos</h2>
                                </div>

                                <!-- <div class="panel"><div class="panel-header"><div class="panel-body"> -->
                                <div class="card-body">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <td width="20">#</td>
                                                <td>PROYECTO</td>
                                                <td align="center" width="200">FECHA CREACI&Oacute;N</td>
                                                <td align="center" width="200">FECHA FINALIZACI&Oacute;N</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if (isset($this->proyectos)):

                                                    $cont = 1;
                                                    foreach($this->proyectos as $proyecto):
                                                    
                                                    ?>
                                                        <tr onclick="irA('<?php Utils::print(BASE_URL); ?>proyectos/ver/<?php Utils::print($proyecto["nombre"]); ?>');">
                                                    
                                                                <td><?php Utils::print($cont); ?></td>
                                                                <td><?php Utils::print($proyecto["nombre"]); ?></td>

                                                            <?php
                                                                include_once "application/public/php/func_fechas.php"; // Para usar funciones de fechas
                                                                if (class_exists("MiDate"))
                                                                    $mDate = MiDate::getInstance();
                                                                else
                                                                    return;
                                                            ?>
                                                                <td align="center"><?php !empty($mDate) ? Utils::print($mDate->formatearFecha($proyecto["fecha_inicio"])) : "-" ?></td>

                                                        <?php
                                                        if ($proyecto["fecha_fin"] == "0000-00-00 00:00:00"): ?>
                                                            <td align="center">-</td>
                                                        <?php
                                                        else:
                                                        ?>
                                                            <td align="center"><?php !empty($mDate) ? Utils::print($mDate->formatearFecha($proyecto["fecha_fin"])) : "-" ?></td>
                                                        <?php
                                                        endif;
                                                        ?>

                                                        </tr>

                                                        <?php $cont++;
                                                    
                                                    endforeach;
                                                else: ?>
                                                    <tr><td colspan="4"><?php Utils::print(msj_sin_resultados) ?></td></tr>
                                                <?php
                                                endif;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-footer">
                                    <div class="num-resultados">
                                        <?php if (isset($cont)): ?>
                                                <?php Utils::print($cont-1 . " resultados"); 
                                              endif; ?>
                                    </div>
                                    <div class="card-footer-info">
                                        <i>(Haz click en un proyecto para editar sus datos o asignar tareas)</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="agregar-proyecto">
                        <div class="card">
                            <div class="card-header">
                                <h2>Agregar nuevo proyecto</h2>
                            </div>
                            <div class="card-body">
                                <form method="post" action="<?php Utils::print(BASE_URL) ?>proyectos/agregar_proyecto">
                                    <div class="col-lg-12">
                                        <div id="campo-nombre" class="form-group">
                                            <label for="nombre">Proyecto</label>
                                            <input class="form-control" type="text" name="nombre" id="nombre" placeholder="Proyecto">
                                        </div>
                                        <div id="campo-cliente" class="form-group">
                                            <label for="cliente">Cliente</label>
                                            <select class="form-control" name="cliente" id="cliente">
                                                <?php
                                                    if (isset($this->clientes)):
                                                        foreach ($this->clientes as $cliente): 
                                                            
                                                            if ($cont == 1): ?>
                                                                <option value="<?php Utils::print($cliente["idcliente"]) ?>" selected><?php Utils::print($cliente["contacto"]) ?></option>
                                                    <?php else: ?>
                                                                <option value="<?php Utils::print($cliente["idcliente"]) ?>"><?php Utils::print($cliente["contacto"]) ?></option>
                                                    <?php endif;

                                                        endforeach;
                                                    endif;                                
                                                    ?>
                                            </select>
                                        </div>
                                        <div id="campo-comentarios" class="form-group">
                                            <label for="comentarios">Comentarios</label>
                                            <textarea class="form-control" name="comentarios" id="comentarios" placeholder="Comentarios"></textarea>
                                        </div>
                                        <div class="btn-toolbar" role="toolbar">
                                            <button class="btn btn-success centrado">
                                                <span class="fa fa-plus"></span> Agregar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer"></div>
                        </div>
                    </div>
            </div>
    </section>
</main>