<main>
    <section id="contenido">
        <div class="container">
                <?php
                if (!empty($this->datos_proyecto)):
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Datos del proyecto <span class="color-primario"><?php !empty($this->nombre_proyecto) ? Utils::print($this->nombre_proyecto) : null; ?></span></h1>
                        <hr>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px;">
                    <div class="col-lg-6">
                        <ul>
                            <li>
                                <div class="cabecera_campo">CLIENTE</div>
                                <div class="contenido_campo"><?php Utils::print($this->datos_proyecto[0]["contacto"]); ?></div>
                            </li>
                            <li>
                                <div class="cabecera_campo">FECHA CREACI&Oacute;N</div>
                                <div class="contenido_campo">
                                    <?php
                                    include_once "application/public/php/func_fechas.php"; // Para usar funciones de fechas
                                    if (class_exists("MiDate"))
                                        $mDate = MiDate::getInstance();
                                    else
                                        return;?>
                                <?php Utils::print($mDate->formatearFechaHora($this->datos_proyecto[0]["fecha_inicio"])); ?></div>
                            </li>
                            <li>
                                <div class="cabecera_campo">FECHA FINALIZACI&Oacute;N</div>
                                <div class="contenido_campo">
                                    <?php if ($this->datos_proyecto[0]["fecha_fin"] == "0000-00-00 00:00:00"): ?>
                                            <?php Utils::print("Sin finalizar"); ?>
                                    <?php else: ?>
                                            <?php Utils::print($mDate->formatearFechaHora($this->datos_proyecto[0]["fecha_fin"]));
                                    endif; ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <li class="comentarios">
                                <div class="cabecera_campo">COMENTARIOS</div>
                                <div class="contenido_campo">
                                    <?php !empty($this->datos_proyecto[0]["comentarios"]) ? Utils::print($this->datos_proyecto[0]["comentarios"]) : Utils::print("Sin comentarios");
                                ?></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="btn-toolbar toolbar float-right" role="toolbar">
                    <form method="POST" name="form" id="form">
                        <input type="hidden" name="operacion" id="operacion" value="">
                        
                        <button class="btn btn-danger" id="btnEliminar" name="btnEliminar" value="eliminar" onclick="document.getElementById('operacion').value='borrar';">
                            <i class="fas fa-trash"></i> Eliminar
                        </button>
                        <button type="button" class="btn btn-default" id="btnEditar" name="btnEditar" value="editar" onclick="irA('<?php Utils::print(BASE_URL."proyectos/editar/".$this->nombre_proyecto); ?>');" style="margin-left:10px;">
                            <i class="fas fa-edit"></i> Editar datos
                        </button>
                        
                        <?php
                        if ($this->datos_proyecto[0]["fecha_fin"] == "0000-00-00 00:00:00"):
                        ?>
                            <button type="submit" onclick="document.getElementById('operacion').value='finalizar';" class="btn btn-success" style="margin-left:10px;">
                                <i class="fas fa-lock"></i> Finalizar proyecto
                            </button>
                        <?php
                        else:
                        ?>
                            <button type="submit" onclick="document.getElementById('operacion').value='reabrir';" class="btn btn-success" style="margin-left:10px;">
                                <i class="fas fa-lock-open"></i> Reabrir proyecto
                            </button>
                        <?php
                        endif;
                        ?>
                    </form>
                </div>
                <?php else: ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-danger" role="alert">
                                    No se han podido recuperar los datos o el proyecto ha sido eliminado.
                                    <a href="#" class="alert-link"></a>
                                </div>
                            </div>
                        </div>
                <?php endif; 

                if (isset($this->datos_proyecto)):
                ?>
                <div id="tareas-proyecto">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>Tareas del proyecto</h1>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="btn-toolbar toolbar" role="toolbar">
                                    <button class="btn btn-default" onclick="document.getElementById('tareas_no_asignadas').style.display = 'block';">
                                    <span class="fas fa-plus"></span> Asignar tareas al proyecto
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <form method="POST" name="form" id="form" action="<?php Utils::print(BASE_URL . "proyectos/asignar_tareas_proyecto/" . $this->datos_proyecto[0]['nombre']) ?>">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <td width="20"></td>
                                                <td width="400">TAREA</td>
                                                <td>COMENTARIOS</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($this->tareas_no_asignadas)):
                                            
                                                if (is_array($this->tareas_no_asignadas)):

                                                    $cont = 1;

                                                    foreach($this->tareas_no_asignadas as $tarea):      ?>
                                                        <tr>
                                                            <td><input type="checkbox" name="noasignadas[]" id="noasignadas[]" value="<?php Utils::print($tarea["idtarea"]) ?>"></td>
                                                            <td><?php Utils::print($tarea["nombre"]) ?></td>
                                                            <td><?php Utils::print($tarea["comentarios"]) ?></td>
                                                        </tr>
                                                        <?php 
                                                        $cont++;
                                                    endforeach;
                                                else: ?>
                                                    <tr><td class="danger" colspan="3">Error: <?php Utils::print(msj_error_obtener_noasignadas); ?></td></tr>
                                          <?php endif;
                                            else: ?>
                                                    <tr><td colspan="3">No se han encontrado resultados</td></tr>
                                      <?php endif; ?>
                                        </tbody>
                                    </table>
                                    
                                    <div class="btn-toolbar toolbar pull-right" role="toolbar">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fas fa-plus"></i> Asignar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <form method="POST" name="form-desasignar" id="form-desasignar" action="<?php Utils::print(BASE_URL . "proyectos/desasignar_tareas_proyecto/" . $this->datos_proyecto[0]["nombre"]); ?>">
                                    <input type="hidden" name="tarea_asignada" id="tarea_asignada" value="">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <td width="20">#</td>
                                                <td width="400">TAREA</td>
                                                <td>COMENTARIOS</td>
                                                <td width="100"></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($this->tareas_asignadas)):
                                                if (is_array($this->tareas_asignadas)):
                                                    $cont = 1;
                                                    foreach($this->tareas_asignadas as $tarea):
                                                    ?>
                                                    <tr>
                                                        <td><?php Utils::print($cont); ?></td>
                                                        <td><?php Utils::print($tarea["nombre"]); ?></td>
                                                        <td><?php Utils::print($tarea["comentarios"]); ?></td>
                                                        <td>
                                                            <button class="btn btn-default" onclick="desasignarTarea('<?php Utils::print($tarea["idtarea_proyecto"]); ?>');" title="Quitar asignación">
                                                                <i class="fas fa-remove"></i> Quitar
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <?php $cont++;
                                                    endforeach;
                                                else: 
                                                ?>
                                                <tr><td class="danger" colspan="4">Error: <?php Utils::print(msj_error_obtener_asignadas); ?></td></tr>
                                                <?php  
                                                endif;
                                            else: 
                                            ?>
                                                <tr><td colspan="4"><?php Utils::print(msj_sin_resultados) ?></td></tr>
                                            <?php 
                                            endif;
                                            ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </section>
</main>