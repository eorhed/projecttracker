<main>
    <?php 

    if (class_exists("MiLog"))
    {
        $log = MiLog::getInstance();
        if ($log->hayMensajes())
            echo $log->showLog();
    }
?>
    <section id="contenido">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Editar proyecto <?php !empty($this->datos_proyecto) ? Utils::print($this->datos_proyecto[0]["nombre"]) : null ?></h1>
                    <hr>
                </div>
            </div>
            <div class="row">
                <form method="post" action="<?php Utils::print(BASE_URL); ?>proyectos/editar";?>
                    <div class="col-lg-6">
                        <div id="campo-nombre" class="form-group">
                            <label for="nombre">Proyecto</label>
                                <input class="form-control" type="text" name="nombre" id="nombre" placeholder="Proyecto" value="<?php !empty($this->datos_proyecto) ? Utils::print($this->datos_proyecto[0]["nombre"]) : null ?>">
                        </div>
                        <div id="campo-cliente" class="form-group">
                            <label for="cliente">Cliente</label>
                            <select class="form-control" name="cliente" id="cliente">
                            <?php
                                if (!empty($this->clientes) && !empty($this->datos_proyecto)):
                                    foreach ($this->clientes as $cliente):
                                        
                                        $idcliente = $cliente["idcliente"];
                                        $contacto = $cliente["contacto"];
                                        
                                        if ($this->datos_proyecto[0]["contacto"] == $contacto): ?>
                                            <option value="<?php Utils::print($idcliente) ?>" selected><?php Utils::print($contacto) ?></option>
                                  <?php else: ?>
                                            <option value="<?php Utils::print($idcliente) ?>"><?php Utils::print($contacto) ?></option>
                                  <?php endif;
                                    endforeach;
                                endif;
                            ?>
                                    
                            </select>
                        </div>
                        <div id="campo-comentarios" class="form-group">
                            <label for="comentarios">Comentarios</label>
                            <textarea class="form-control" name="comentarios" id="comentarios" placeholder="Comentarios"><?php !empty($this->datos_proyecto) ? Utils::print($this->datos_proyecto[0]["comentarios"]) : null ?></textarea>
                        </div>
                        <div class="btn-toolbar pull-right" role="toolbar">
                            <input type="hidden" name="nombre_anterior" id="nombre_anterior" value="<?php !empty($this->datos_proyecto) ? Utils::print($this->datos_proyecto[0]["nombre"]) : null; ?>">
                            <input type="hidden" name="idproyecto" id="idproyecto" value="<?php !empty($this->datos_proyecto) ? Utils::print($this->datos_proyecto[0]["idproyecto"]) : null; ?>">
                            <button class="btn btn-success btn-agregar">
                                <span class="glyphicon glyphicon-edit"></span> Editar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>