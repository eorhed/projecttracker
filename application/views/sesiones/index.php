<main>
    <?php
    if (class_exists("MiLog"))
    {
        $log = MiLog::getInstance();
        if ($log->hayMensajes())
            echo $log->showLog();
    }
    ?>
    <section id="contenido">
        <div class="container">
                    <div class="row">
                        <div class="col-lg-9">
                            <h1>
                                <i class="fa fa-file-code"></i> Sesiones</h1>
                        </div>
                        <div class="col-lg-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb navegacion">
                                    <li class="breadcrumb-item">
                                        <i class="fa fa-home"></i>
                                        <a href="<?php Utils::print(BASE_URL) ?>dashboard"> Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Sesiones</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Proyecto</td>
                                        <td>Tarea</td>
                                        <td align="center">Fecha</td>
                                        <td align="center">Duraci&oacute;n</td>
                                        <td align="center">Duraci&oacute;n (min)</td>
                                        <td align="center">Coste hora (€/h)</td>
                                        <td width="20">#</td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    
                                    $cont = 1;

                                    if (isset($this->sesiones)):
                                    
                                        if (is_array($this->sesiones)):
                                        
                                            $proyecto_actual = null;
                                            // $tarea_actual = null;
                                            include_once('application/public/php/func_fechas.php');
                                            if (class_exists("MiDate"))
                                                $mDate = MiDate::getInstance();
                                            else
                                                return;
                                            
                                            foreach($this->sesiones as $sesion):

                                                $duracion_min = floor($sesion["duracion_segs"] / 60);
                                                $duracion_segs = $sesion["duracion_segs"] % 60;
                                                $fecha = substr($mDate->formatearFechaHora($sesion["fecha"]),0,16);
                                            
                                                if ($sesion["nombre_proyecto"] != $proyecto_actual): 
                                                    
                                                    $proyecto_actual = $sesion["nombre_proyecto"];
                                                    $tarea_actual = $sesion["nombre_tarea"];
                                                ?>
                                                    
                                                    <tr class='proyecto'>
                                                        <td><?php Utils::print($sesion["nombre_proyecto"]) ?></td>
                                                        
                                          <?php else: ?>
                                                    <tr>
                                                        <td></td>
                                          <?php endif; ?>

                                                        <td><?php Utils::print($sesion["nombre_tarea"]) ?></td>
                                                        <td align="center"><?php Utils::print($fecha) ?></td>
                                                        <td align="center"><?php Utils::print($sesion["duracion"]) ?></td>
                                                        <td align="center"><?php Utils::print($duracion_min) ?> min <?php Utils::print($duracion_segs) ?> s</td>
                                                        <td align="center"><?php Utils::print($sesion["precio_x_hora"]) ?></td>
                                                        <td><?php Utils::print($cont) ?></td>
                                                    </tr>
                                                
                                                
                                            <?php
                                                $cont++;

                                            endforeach;

                                        else: ?>
                                            <tr><td class="alert alert-danger" colspan="6"><?php Utils::print(msj_error_obtener_sesiones) ?></td></tr>
                                  <?php endif;
                                    
                                    else: ?>
                                        <tr><td colspan="6"><?php Utils::print(msj_sin_resultados) ?></td></tr>
                              <?php endif;
                                ?>
                                </tbody> 
                            </table>
                            <div class="float-right"><h2><?php isset($cont) ? Utils::print($cont-1) : 0 ?> resultados</h2></div>
                        </div>
                </div>
        </div>
    </section>
</main>