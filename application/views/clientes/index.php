<main>
    <section id="contenido">
        <div class="container">
            <div class="row">
                <div class="col-lg-10">
                    <h1>
                        <i class="fa fa-users"></i>
                        Clientes
                    </h1>
                </div>
                <div class="col-lg-2">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb navegacion">
                            <li class="breadcrumb-item">
                                <i class="fas fa-home"></i> <a href="<?php Utils::print(BASE_URL) ?>dashboard">Inicio</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Clientes</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>Lista de clientes</h2>
                            </div>
                            <div class="card-body">
                                <div class="btn-toolbar toolbar" role="toolbar">
                                    <button class="btn btn-default btn-primary"
                                        onclick="irA('<?php Utils::print(BASE_URL) ?>clientes/agregar')">
                                        <span class="glyphicon glyphicon-plus"></span> Agregar nuevo cliente
                                    </button>
                                </div>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <td width="50">#</td>
                                            <td>CONTACTO</td>
                                            <td>EMPRESA</td>
                                            <td>EMAIL</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                                if (isset($this->clientes)):
                                                
                                                    if (is_array($this->clientes)):

                                                        $cont = 1;
                                                        foreach($this->clientes as $cliente): 
                                                        ?>
                                                            <tr
                                                                onclick="irA('<?php Utils::print(BASE_URL) ?>clientes/editar/<?php Utils::print($cliente["contacto"]) ?>');">
                                                                <td><?php Utils::print($cont) ?></td>
                                                                <td><?php Utils::print($cliente["contacto"]) ?></td>
                                                                <td><?php Utils::print($cliente["empresa"]) ?></td>
                                                                <td><?php Utils::print($cliente["email"]) ?></td>
                                                            </tr>

                                                            <?php $cont++;
                                                        endforeach;
                                                    else:
                                            ?>
                                                    <tr>
                                                        <td class="alert alert-danger" colspan="4">
                                                            <?php Utils::print(msj_error_obtener_clientes) ?></td>
                                                    </tr>
                                                <?php endif;
                                                else:
                                                ?>
                                        <tr>
                                            <td colspan="4"><?php Utils::print(msj_sin_resultados) ?></td>
                                        </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                                <div class="num-resultados">
                                    <?php if (isset($cont)) ?>
                                    <?php Utils::print($cont-1 . " resultados"); ?>
                                </div>
                                <div class="card-footer-info">
                                    <i>(Haz click en un cliente para editar sus datos)</i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>