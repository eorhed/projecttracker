<main>
    <section id="contenido">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Editar cliente <span class="color-primario"><?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["contacto"]) : null;?></span></h1>
                    <hr>
                </div>
            </div>
            <form method="POST" name="form" id="form" action="<?php Utils::print(BASE_URL) ?>clientes/editar">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header"></div>
                            <div class="card-body">
                                <div id="campo-contacto" class="form-group">
                                    <label for="contacto">Persona de contacto</label>
                                    <input class="form-control" type="text" name="contacto" id="contacto" placeholder="Persona de contacto"
                                        value="<?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["contacto"]) : null; ?>">
                                </div>
                                <div id="campo-empresa" class="form-group">
                                    <label for="empresa">Empresa</label>
                                    <input class="form-control" type="text" name="empresa" id="empresa" placeholder="Empresa" 
                                        value="<?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["empresa"]) : null; ?>">
                                </div>
                                <div id="campo-telefono" class="form-group">
                                    <label for="telefono">Tel&eacute;fono</label>
                                    <input class="form-control" type="text" name="telefono" id="telefono" placeholder="Tel&eacute;fono" 
                                        value="<?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["telefono"]) : null; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header"></div>
                            <div class="card-body">
                                <div id="campo-email" class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" type="email" name="email" id="email" placeholder="Email" 
                                    value="<?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["email"]) : null; ?>">
                                </div>
                                <div id="campo-direccion" class="form-group">
                                    <label for="direccion">Direcci&oacute;n</label>
                                <input class="form-control" type="text" name="direccion" id="direccion" placeholder="Direcci&oacute;n" 
                                value="<?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["direccion"]) : null; ?>">
                                </div>
                                <div id="campo-municipio" class="form-group">
                                    <label for="municipio">Municipio</label>
                                    <input class="form-control" type="text" name="municipio" id="municipio" placeholder="Municipio" 
                                    value="<?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["municipio"]) : null; ?>">
                                </div>
                                <div class="btn-toolbar pull-right" role="toolbar">
                                    <input type="hidden" name="nombre_anterior" id="nombre_anterior" value="<?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["contacto"]) : null; ?>">
                                    <input type="hidden" name="idcliente" id="idcliente" value="<?php isset($this->datos_cliente) ? Utils::print($this->datos_cliente[0]["idcliente"]) : null; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="btn-toolbar">
                            <button class="btn btn-lg btn-danger" name="opEliminar" id="opEliminar" value="eliminar" onclick="eliminarCliente();">
                                <span class="fas fa-trash"></span> Eliminar
                            </button>
                            <button class="btn btn-lg btn-success ml-20" id="opGuardar" name="opGuardar" value="guardar">
                                <span class="fas fa-edit"></span> Guardar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</main>