<main>
    <section id="contenido">
        <div class="container">
            <section id="agregar-cliente">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header">
                            <h2>Agregar nuevo cliente</h2>
                        </div>
                        <div class="card-body">
                            <form method="post" action="<?php Utils::print(BASE_URL) ?>clientes/agregar">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div id="campo-contacto" class="form-group">
                                            <label for="contacto">Persona de contacto * <span class="color-error">
                                                    <?php !empty($this->errores["contacto"]["errores"]) ? Utils::print($this->errores["contacto"]["errores"][0]) : "" ?></span>
                                            </label>
                                            <input class="form-control" type="text" name="contacto" id="contacto" placeholder="Persona de contacto" maxlength="100" value="<?php !empty($_POST) ? filter_input(INPUT_POST, "contacto", FILTER_SANITIZE_FULL_SPECIAL_CHARS) : null ?>" required>
                                        </div>
                                        <div id="campo-empresa" class="form-group">
                                            <label for="empresa">Empresa <span class="color-error">
                                                    <?php !empty($this->errores["empresa"]["errores"]) ? Utils::print($this->errores["empresa"]["errores"][0]) : "" ?></span>
                                            </label>
                                            <input class="form-control" type="text" name="empresa" id="empresa" placeholder="Empresa" maxlength="100" value="<?php !empty($_POST) ? filter_input(INPUT_POST, "empresa", FILTER_SANITIZE_FULL_SPECIAL_CHARS) : null;?>">
                                        </div>
                                        <div id="campo-telefono" class="form-group">
                                            <label for="telefono">Tel&eacute;fono <span class="color-error">
                                                    <?php !empty($this->errores["contacto"]["telefono"]) ? Utils::print($this->errores["telefono"]["errores"][0]) : "" ?></span>
                                            </label>
                                            <input class="form-control" type="text" name="telefono" id="telefono" placeholder="Tel&eacute;fono" maxlength="20" value="<?php !empty($_POST) ? filter_input(INPUT_POST, "telefono", FILTER_SANITIZE_FULL_SPECIAL_CHARS) : null;?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div id="campo-email" class="form-group">
                                            <label for="email">Email * <span class="color-error">
                                                    <?php !empty($this->errores["email"]["errores"]) ? Utils::print($this->errores["email"]["errores"][0]) : "" ?></span></label>
                                            <input class="form-control" type="email" name="email" id="email" placeholder="Correo electr&oacute;nico" maxlength="100" value="<?php !empty($_POST) ? filter_input(INPUT_POST, "email", FILTER_SANITIZE_FULL_SPECIAL_CHARS) : null;?>">
                                        </div>
                                        <div id="campo-direccion" class="form-group">
                                            <label for="direccion">Direcci&oacute;n <span class="color-error">
                                                    <?php !empty($this->errores["direccion"]["errores"]) ? Utils::print($this->errores["direccion"]["errores"][0]) : "" ?></span>
                                            </label>
                                            <input class="form-control" type="text" name="direccion" id="direccion" placeholder="Direcci&oacute;n" maxlength="300" value="<?php !empty($_POST) ? filter_input(INPUT_POST, "direccion", FILTER_SANITIZE_FULL_SPECIAL_CHARS) : null;?>">
                                        </div>
                                        <div id="campo-municipio" class="form-group">
                                            <label for="municipio">Municipio <span class="color-error">
                                                    <?php !empty($this->errores["municipio"]["errores"]) ? Utils::print($this->errores["municipio"]["errores"][0]) : "" ?></span>
                                            </label>
                                            <input class="form-control" type="text" name="municipio" id="municipio" placeholder="Municipio" maxlength="100" value="<?php !empty($_POST) ? filter_input(INPUT_POST, "municipio", FILTER_SANITIZE_FULL_SPECIAL_CHARS) : null;?>">
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="btn-toolbar" role="toolbar">
                                            <button class="btn btn-lg btn-success centrado" name="opAgregar" id="opAgregar" value="agregar">
                                                <span class="fa fa-plus"></span> Agregar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                            <div class="card-footer-info">
                                <i>Rellena los datos del formulario para agregar el nuevo cliente</i>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</main>