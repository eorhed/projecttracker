<main>
    <?php     
    if (isset($this->proyectos) && is_array($this->proyectos)) :
?>
    <section id="contenido">
        <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <h1><i class="fa fa-copy"></i> Agregar registro</h1>
                    </div>
                    <div class="col-lg-3 centrado">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb navegacion">
                                <li class="breadcrumb-item">
                                    <i class="fa fa-home"></i><a href="<?php Utils::print(BASE_URL) ?>dashboard">
                                        Inicio</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Agregar registro</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h2>Lista de informes</h2>
                    </div>
                    <div class="card-body">
                        <form method="post" id="form"
                            action="<?php Utils::print(BASE_URL) ?>tareas/agregar_seguimiento">
                            <div id="campo-proyecto" class="form-group">
                                <label for="proyecto">Proyecto</label>

                                    <?php
                                    if (isset($this->proyectos)):
                                            
                                        if (is_array($this->proyectos)): ?>

                                            <select class="form-control" name="proyecto" id="proyecto" onchange="cargarTareasProyecto();">

                                                <?php
                                                    $cont = 1;
                                                    foreach ($this->proyectos as $proyecto):
                                                    ?>
                                                        <option value="<?php Utils::print($proyecto["idproyecto"]) ?>"<?php ($cont == 1) ? " selected" : null ?>>
                                                            <?php Utils::print($proyecto["nombre"]) ?>
                                                        </option>

                                                        <?php $cont++;
                                                    
                                                    endforeach; ?>
                                            </select>
                                <?php endif;
                                        else: ?>
                                            <div class="alert alert-danger"><?php Utils::print(msj_error_no_hay_proyectos); ?></div>
                                <?php endif; ?>
                            </div>
                            <div id="campo-tarea" class="form-group">
                                <label for="tarea">Tarea</label>
                                <select class="form-control" name="tarea" id="tarea">
                                    <?php
                                        if (isset($this->tareas)):
                                        
                                            if (is_array($this->proyectos)):
                                            
                                                $cont = 1;
                                                foreach ($this->tareas as $tarea):
                                                    $idtarea = $tarea["idtarea"];
                                                    $tarea = $tarea["nombre"];
                                                
                                                    if ($cont == 1): ?>
                                    <option value="<?php Utils::print($idtarea); ?>" selected>
                                        <?php Utils::print($tarea); ?></option>
                                    <?php else: ?>
                                    <option value="<?php Utils::print($idtarea); ?>"><?php Utils::print($tarea); ?>
                                    </option>
                                    <?php endif;

                                                    $cont++;

                                                endforeach;
                                            else: ?>
                                    <div class='alert alert-danger'><?php Utils::print(msj_error_obtener_tareas); ?>
                                    </div>
                                    <?php endif;

                                        endif;
                                    ?>
                                </select>
                            </div>
                            <div id="campo-precio_por_hora" class="form-group">
                                <label for="precio_por_hora">Euros la hora</label>
                                <input class="form-control" type="range" name="precio_por_hora" id="precio_por_hora"
                                    min="0" max="35" step="1" value="15" placeholder="Euros la hora"
                                    onChange="actualizarPrecioHora(this.value);">
                                <div class="euros_hora">15 €</div>
                            </div>
                            <div id="campo-duracion_sesion" class="form-group">
                                <label for="duracion_sesion">Duraci&oacute;n de sesi&oacute;n (en minutos)</label>
                                <input class="form-control" type="number" name="duracion_sesion" id="duracion_sesion"
                                    placeholder="Minutos" value="1">
                            </div>
                            <div id="campo-comentarios" class="form-group">
                                <label for="comentarios">Comentarios</label>
                                <textarea class="form-control" name="comentarios" id="comentarios"
                                    placeholder="Comentarios"></textarea>
                            </div>
                            <div class="btn-toolbar">
                                <button type="button" class="btn btn-lg btn-primary btn-agregar centrado"
                                    onclick="guardar_sesion_tarea();">
                                    <span class="fa fa-plus"></span> Agregar sesi&oacute;n
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            <div id="info" class="marginT20"></div>
        </div>
    </section>

    <?php
    else:
        if (isset($this->proyectos) && !is_array($this->proyectos) && $this->proyectos == -1): ?>
            <div class="alert alert-danger" style="font-size:1.5rem; width:80%; margin:0 auto; margin-top: 25px;">
                <span class="glyphicon glyphicon-remove"></span> <?php Utils::print(msj_error_obtener_proyectos) ?>
            </div>
    <?php else: ?>
            <div class="alert alert-info" style="font-size:1.5rem; width:80%; margin:0 auto; margin-top: 50px;">
                <span class="glyphicon glyphicon-info-sign" style="font-size:2rem; margin-right: 10px;"></span>
                <?php Utils::print(msj_error_no_hay_proyectos) ?>
            </div>
    <?php endif;
    endif;
?>
</main>