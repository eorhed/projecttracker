<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>PROJECTTRACKER</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="all" />
    <meta name="keywords" content="time tracking seguimiento tareas gestion proyectos" />
    <meta name="description" content="Gestión y seguimiento de tareas en proyectos" />
    <link rel="shortcut icon" href="application/public/img/favicon.png">
    <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,400,700" rel="stylesheet">  -->
    <script src="<?php Utils::print(BASE_URL) ?>application/public/js/utils.js"></script>
    <script src="<?php Utils::print(BASE_URL) ?>application/public/js/chart.min.js"></script>
</head>

<body>
    <header id="main-header">
        <!-- <nav class="navbar navbar-expand-lg sticky-top"> -->
        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="<?php Utils::print(BASE_URL) ?>">PR<img class="logo" src="<?php Utils::print(BASE_URL) ?>application/public/img/logo3.png" alt="Logo Projecttracker"
                    />JEC<span class="">TT</span>RACKER</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrincipal" aria-controls="navbarPrincipal"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarPrincipal">
                    <?php if (Session::isLogged()): ?>
                    <form action="#" method="post" class="sidebar-form">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php Utils::print(BASE_URL) ?>dashboard"><i class="fa fa-tachometer-alt"></i> Panel de control</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false"><i class="fa fa-stopwatch"></i> Trackear</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php Utils::print(BASE_URL) ?>trackear"><i class="fa fa-stopwatch"></i> Tracking automático</a>
                                    <a class="dropdown-item" href="<?php Utils::print(BASE_URL) ?>agregar_registro"><i class="fa fa-plus"></i> Agregar registro</a>
                                    <!-- <div class="dropdown-divider"></div> -->
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php Utils::print(BASE_URL) ?>informes"><i class="fa fa-copy"></i> Informes</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false"><i class="fa fa-cog"></i> Administrar</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php Utils::print(BASE_URL) ?>clientes"><i class="fa fa-user"></i> Clientes</a>
                                    <a class="dropdown-item" href="<?php Utils::print(BASE_URL) ?>proyectos"><i class="fa fa-file-code"></i> Proyectos</a>
                                    <a class="dropdown-item" href="<?php Utils::print(BASE_URL) ?>tareas"><i class="fa fa-tasks"></i> Tareas</a>
                                    <a class="dropdown-item" href="<?php Utils::print(BASE_URL) ?>sesiones"><i class="fa fa-clock"></i> Sesiones</a>
                                    <!-- <div class="dropdown-divider"></div> -->
                                </div>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link disabled" href="#">Disabled</a>
                            </li> -->
                        </ul>
                        <!-- <div class="input-group">
                            <input name="q" class="form-control" placeholder="Buscar..." type="search">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div> -->
                    </form>
                    <?php else: ?>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php Utils::print(BASE_URL) ?>registrar"><i class="fa fa-user-plus"></i> Registrarse</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php Utils::print(BASE_URL) ?>identificarse"><i class="fa fa-sign-in-alt"></i> Identificarse</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li> -->
                    </ul>
                    <?php endif; ?>
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php Utils::print(BASE_URL) ?>ayuda"><i class="fa fa-question-circle"></i> Ayuda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php Utils::print(BASE_URL) ?>contacto"><i class="fa fa-envelope"></i> Contacto</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link disabled" href="#">Disabled</a>
                        </li> -->
                    </ul>

                    <?php if (Session::isLogged()): ?>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <img src="<?php Utils::print(BASE_URL) ?>application/public/img/user/<?php Utils::print(Session::get("foto")); ?>" alt="Foto perfil" width="32" />
                                <?php Utils::print(Session::get('usuario')); ?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php Utils::print(BASE_URL) ?>preferencias"><i class="fa fa-cogs"></i> Preferencias</a>
                                <a class="dropdown-item" href="<?php Utils::print(BASE_URL) ?>salir"><i class="fa fa-sign-out-alt"></i> Salir</a>
                                <!-- <div class="dropdown-divider"></div> -->
                            </div>
                        </li>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </nav>
    </header>

    <!-- Mensajes de notificación al usuario -->
    <?php if (isset($this->msj_error)): ?>
        <div class="mensaje-info alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-remove"></span> <?php Utils::print($this->msj_error); ?>
            <a href="#" class="alert-link" onclick="cerrarMensajeInfo();"> X</a>
        </div>
    <?php elseif (isset($this->msj_exito)): ?>
        <div class="mensaje-info alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok"></span> <?php Utils::print($this->msj_exito); ?>
            <a href="#" class="alert-link" onclick="cerrarMensajeInfo();"> X</a>
        </div>
    <?php endif; ?>