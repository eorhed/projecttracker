<main>
	<section id="contenido">
		<div class="contenedor">
			<div class="centrado">
				<div id="mensaje_error">
					<h1>404</h1>
					<h2><?php Utils::print($this->msg) ?></h2>
				</div>
			</div>
		</div>
	</section>
</main>