<main>
    <section id="contenido">
        <div class="container">
            <section id="grid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1><i class="fa fa-home"></i> Escritorio</h1>
                    </div>
                </div>
                <div id="estado-general" class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="cuadro-info bg-rojo">
                                <h2>
                                    <i class="fa fa-user"></i>
                                    <br><?php isset($this->num_clientes) ? Utils::print($this->num_clientes) : "-"; ?></h2>
                                <div class="cuadro-info-cuerpo">Tienes
                                    <?php isset($this->num_clientes) ? Utils::print($this->num_clientes) : "-"; ?> clientes
                                </div>
                                <div class="cuadro-info-footer">
                                    <a href="<?php Utils::print(BASE_URL) ?>clientes">M&aacute;s info
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="cuadro-info bg-verde">
                                <h2>
                                    <i class="fa fa-copy"></i>
                                    <br><?php isset($this->num_proyectos) ? Utils::print($this->num_proyectos) : "-"; ?></h2>
                                <div class="cuadro-info-cuerpo">Gestionas
                                    <?php isset($this->num_proyectos) ? Utils::print($this->num_proyectos) : "-"; ?>
                                    proyectos</div>
                                <div class="cuadro-info-footer">
                                    <a href="<?php Utils::print(BASE_URL) ?>proyectos">M&aacute;s info
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="cuadro-info bg-azul">
                                <h2>
                                    <i class="fa fa-clock"></i>
                                    <br><?php isset($this->num_horas_totales) ? Utils::print($this->num_horas_totales) : "-"; ?>
                                </h2>
                                <div class="cuadro-info-cuerpo">Trackeadas
                                    <?php isset($this->num_horas_totales) ? Utils::print($this->num_horas_totales) : "-"; ?>
                                    horas en total</div>
                                <div class="cuadro-info-footer">
                                    <a href="<?php Utils::print(BASE_URL); ?>sesiones">M&aacute;s info
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="cuadro-info bg-morado">
                                <h2>
                                    <i class="fa fa-user"></i>
                                    <br><?php isset($this->dinero_ganado) ? Utils::print($this->dinero_ganado) : "-"; ?></h2>
                                <div class="cuadro-info-cuerpo">Has generado
                                    <?php isset($this->dinero_ganado) ? Utils::print($this->dinero_ganado) : "-"; ?> euros
                                </div>
                                <div class="cuadro-info-footer">
                                    <a href="<?php Utils::print(BASE_URL); ?>informes">M&aacute;s info
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="graficos">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="card">
                                <div class="card-header bg-azul">
                                    <h5>Horas registradas</h5>
                                </div>
                                <div class="card-body">
                                    <div id="canvas-holder">
                                        <canvas id="chart-line"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header bg-azul">
                                    <h5>Horas registradas</h5>
                                </div>
                                <div class="card-body">
                                    <div id="canvas-holder">
                                        <canvas id="chart-area"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</main>