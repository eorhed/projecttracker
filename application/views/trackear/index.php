<main>
    <section id="contenido">
        <div class="container">
            
                <?php if (isset($this->proyectos) && is_array($this->proyectos)) : ?>
                <!-- <div class="row">
                    <div class="col-lg-12"><div class="migas-pan"><a href="#">Inicio</a> > <a href="#">Agregar registro</a></div></div>
                </div> -->
                <div id="area-configurar-trackeo">
                    <div class="row">
                        <div class="col-lg-9"><h1><img src="application/public/img/logo2.png">Trackear</h1></div>
                        <div class="col-lg-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb navegacion">
                                    <li class="breadcrumb-item">
                                        <a href="<?php Utils::print(BASE_URL) ?>">Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Trackeo automático</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <section id="form-configurar-trackeo">
                        <div class="card">
                            <div class="card-header"><h2>Configurar seguimiento automático</h2></div>
                            <div class="card-body">
                                <form method="post" id="form" action="<?php Utils::print(BASE_URL) ?>tareas/agregar_seguimiento">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div id="campo-proyecto" class="form-group">
                                                <label for="proyecto">Proyecto</label>
                                                
                                                <?php
                                    if (isset($this->proyectos)):
                                            
                                        if (is_array($this->proyectos)): ?>

                                            <select class="form-control" name="proyecto" id="proyecto" onchange="cargarTareasProyecto();">

                                                <?php
                                                    $cont = 1;
                                                    foreach ($this->proyectos as $proyecto):
                                                    ?>
                                                        <option value="<?php Utils::print($proyecto["idproyecto"]); ?>"<?php ($cont == 1) ? " selected" : null ?>>
                                                            <?php Utils::print($proyecto["nombre"]); ?>
                                                        </option>

                                                        <?php $cont++;
                                                    
                                                    endforeach; ?>
                                            </select>
                                <?php endif;
                                        else: ?>
                                            <div class="alert alert-danger"><?php Utils::print(msj_error_no_hay_proyectos); ?></div>
                                <?php endif; ?>
                                            </div>
                                            <div id="campo-tarea" class="form-group">
                                                <label for="tarea">Tarea</label>
                                                <select class="form-control" name="tarea" id="tarea">
                                                <?php
                                                    if (isset($this->tareas)):
                                                    
                                                        if (is_array($this->tareas)):
                                                        
                                                            $cont = 1;
                                                            foreach ($this->tareas as $tarea):
                                                            ?> 
                                                                <option value="<?php Utils::print($tarea["idtarea"]); ?>"<?php ($cont == 1) ? " selected" : null ?>>
                                                                    <?php Utils::print($tarea["nombre"]); ?>
                                                                </option>
                                                                
                                                              <?php  $cont++;

                                                            endforeach;
                                                        
                                                        else: ?>
                                                            <div class='alert alert-danger'><?php Utils::print(msj_error_obtener_tareas) ?></div>
                                                  <?php endif;

                                                    endif;
                                                ?>
                                                </select>
                                            </div>
                                            <div id="campo-precio_por_hora" class="form-group">
                                                <label for="precio_por_hora">Euros la hora</label>
                                                <input class="form-control" type="range" name="precio_por_hora" id="precio_por_hora" min="0" max="35" step="1" value="15" placeholder="Euros la hora" onChange="actualizarPrecioHora(this.value);">
                                                <div class="euros_hora">15 €</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div id="campo-comentarios" class="form-group">
                                                <label for="comentarios">Comentarios</label>
                                                <textarea class="form-control" name="comentarios" id="comentarios" placeholder="Comentarios"></textarea>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-lg btn-primary centrado" onclick="comenzar_seguimiento();">
                                            <i class="fa fa-play"></i> Comenzar seguimiento
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                    <section id="seguimientos">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1>Sesiones de la tarea</h1>
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul>        
                                        <li class="sin-registros">No hay registros de seguimiento en la sesi&oacute;n actual</li>               
                                    </ul>
                                    <div class="btn-toolbar pull-right" role="toolbar">
                                        <button type="button" class="btn btn-default btn-pausar" onclick="pausar_seguimiento();">
                                            <span class="fa fa-pause"></span> Pausar
                                        </button>
                                        <button type="button" class="btn btn-success btn-guardar" onclick="guardar_seguimiento();">
                                            <span class="fa fa-stop"></span> Finalizar y guardar
                                        </button>
                                        <button type="button" class="btn btn-primary btn-comenzar2" onclick="comenzar_seguimiento();">
                                                <span class="fa fa-play"></span> Nuevo seguimiento
                                        </button>
                                    </div>
                                    <!-- <div id="info"></div> -->
                                </div>
                            </div>
                        </div>
                    </section>
                    <div id="info"></div>
                </div>
                
                <?php
                    else:
                        if (isset($this->proyectos) && !is_array($this->proyectos) && $this->proyectos == -1): ?>
                            <div class="alert alert-danger" style="font-size:1.5rem; width:80%; margin:0 auto; margin-top: 25px;">
                                <span class="glyphicon glyphicon-remove"></span><?php Utils::print(msj_error_obtener_proyectos)?></div>
                  <?php else: ?>
                            <div class="alert alert-info" style="font-size:1.5rem; width:80%; margin:0 auto; margin-top: 50px;">
                                <span class="glyphicon glyphicon-info-sign" style="font-size:2rem; margin-right: 10px;"></span> <?php Utils::print(msj_error_no_hay_proyectos) ?></div>
                  <?php endif;
                    endif; ?>
            </div>
    </section>
</main>