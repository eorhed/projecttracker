<main>
    <section id="registrar">
        <h1>Registrar usuario</h1>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <form method="POST" name="form" id="form" action="<?php Utils::print(BASE_URL) ?>registrar/run">
                        <div id="campo-usuario" class="input-group">
                            <!-- <label for="usuario">Usuario</label> -->
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                            <input class="form-control" type="text" name="usuario" id="usuario" placeholder="Usuario"
                            onblur="validarExisteUsuario();">
                        </div>
                        <div id="campo-email" class="input-group">
                            <!-- <label for="email">Email</label> -->
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-envelope"></i>
                                </span>
                            </div>
                            <input class="form-control" type="email" name="email" id="email" placeholder="Correo electr&oacute;nico">
                        </div>
                        <div id="campo-clave" class="input-group">
                            <!-- <label for="clave">Clave</label> -->
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </div>
                            <input class="form-control" type="password" name="clave" id="clave" placeholder="Clave">
                        </div>
                        <div id="campo-repetir-clave" class="input-group">
                            <!-- <label for="repetir-clave">Repetir clave</label> -->
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </div>
                            <input class="form-control" type="password" name="repetir-clave" id="repetir-clave"
                            placeholder="Repetir clave">
                        </div>
                        <div class="btn-toolbar" role="toolbar">
                            <button class="btn btn-lg btn-primary mx-auto" onclick="return validarFormRegistro();">
                            <i class="fa fa-check">&nbsp;</i>Enviar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>