<main>
    <section id="inicio">
      <div class="container">
        <div class="row">
          <div class="col-lg-10"></div>
          <div class="col-lg-2"></div>
        </div>  
        <div class="desplazador">
            <a href="#caracteristicas"><img src="<?php Utils::print(BASE_URL) ?>application/public/img/arrows-red.svg" width="50" height="50" /></a>
        </div> 
      </div><!-- /.container -->
    </section>
    <section id="caracteristicas">
      <div class="container">
        <h1>El software de gestión de tareas de un proyecto</h1>
        <div class="row caracteristica">
          <div class="col-lg-5 descripcion">Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus eum sit deleniti ipsam autem maiores dicta ullam eius necessitatibus? Odit mollitia doloremque officiis eveniet iste porro veritatis nam rem quasi.</div>
          <div class="col-lg-7"><img src="<?php Utils::print(BASE_URL) ?>application/public/img/2b.png"></div>
        </div>
      </div>
    </section>
</main>