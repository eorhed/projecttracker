<main>
    <section id="contenido">
        <div class="container">
                    <div class="row">
                        <div class="col-lg-9">
                            <h1><i class="fa fa-copy"></i> Informes</h1>
                        </div>
                        <div class="col-lg-3">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb navegacion">
                                    <li class="breadcrumb-item">
                                        <i class="fa fa-home"></i> <a href="<?php Utils::print(BASE_URL) ?>dashboard"> Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Informes</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h2>Lista de informes</h2>
                        </div>
                        <div class="card-body">
                            <!-- <h5 class="card-title">Lista de informes</h5> -->
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td>PROYECTO</td>
                                        <td width="120" align="center">FECHA</td>
                                        <td width="120" align="center">DURACI&Oacute;N</td>
                                        <td width="120" align="center">PRECIO</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                // Pedimos la instancia de la clase MiDate para realizar 
                                // algunas operaciones con fechas
                                include_once "application/public/php/func_fechas.php";
                                if (class_exists("MiDate"))
                                    $mDate = MiDate::getInstance();
                                else
                                    return;

                                $cont = 0;
                                if (isset($this->informes)) :
                                    foreach($this->informes as $informe) :
                                ?>
                                        
                                        <tr onclick="irA('<?php BASE_URL ?>informes/ver/<?php Utils::print($informe["nombre"]); ?>');">
                                            <td><?php Utils::print($informe["nombre"]); ?></td>
                                            <td><?php Utils::print($mDate->formatearFecha($informe["fecha_inicio"])); ?></td>
                                            <td align="center"><?php Utils::print($mDate->obtTiempoHMS($informe["duracion_total"],"h:m")); ?></td>
                                            <td align="center"><?php Utils::print(round($informe["precio_total"],2)); ?> €</td>
                                        </tr>
                                        <?php 

                                        $cont++;

                                    endforeach;
                                else: ?>
                                    <tr><td colspan="3">No hay informes de seguimientos de todav&iacute;a<td></div>
                                <?php
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="num-resultados"><?php if (isset($cont)) ?><?php Utils::print($cont) ?> resultado/s</div>
                        </div>
                    </div>
                </div>
    </section>
</main>