<main>
    <section id="contenido">
        <div class="container">
                <section id="ver-informe-proyecto">
                    <div class="row">
                        <div class="col-lg-10">
                            <h1>
                                <i class="fa fa-copy"></i> Ver informe</h1>
                        </div>
                        <div class="col-lg-2">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb navegacion">
                                    <li class="breadcrumb-item">
                                        <i class="fa fa-home"></i> 
                                        <a href="<?php Utils::print(BASE_URL) ?>dashboard"> Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Informe</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="btn-toolbar mt-4" role="toolbar" aria-label="Acciones de informe">
                        <div class="btn-group mr-2" role="group" aria-label="First group">
                            <button type="button" class="btn btn-sm btn-primary"><i class="fa fa-file-pdf"></i>&nbsp; Crear PDF</button>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h2>
                                <span class="titulo_proyecto">
                                    <?php !empty($this->proyecto) ? Utils::print($this->proyecto) : null; ?>
                                </span>
                            </h2>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td width="500">TAREA</td>
                                        <td align="center">N&deg; SESIONES</td>
                                        <td align="center">DURACI&Oacute;N</td>
                                        <td align="center">PRECIO</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    // Pedimos la instancia de la clase MiDate para realizar 
                                    // algunas operaciones con fechas
                                    include_once "application/public/php/func_fechas.php";
                                    if (class_exists("MiDate"))
                                        $mDate = MiDate::getInstance();
                                    else
                                        return;
                                        

                                    if (isset($this->informes)):

                                        $precio_final = 0;
                                        foreach($this->informes as $informe):

                                            $precio_final += $informe["precio_total"]; ?>

                                            <tr>
                                                <td><?php Utils::print($informe["tarea"]) ?></td>
                                                <td align="center"><?php Utils::print($informe["num_sesiones"]) ?></td>
                                                <td align="center"><?php Utils::print($mDate->obtTiempoHMS($informe["duracion_total"])) ?></td>
                                                <td align="center"><?php Utils::print(round($informe["precio_total"],3)) ?> €</td>
                                            </tr>

                                    <?php endforeach;
                                    endif;
                                ?>
                                </tbody>
                            </table>
                            <div class="precio_final">Total:
                            <?php Utils::print(round($precio_final,2)) ?> €
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Distribuci&oacute;n de horas y sesiones</h5>
                                </div>
                                <div class="card-body">
                                    <ul class="nav nav-tabs" id="panel-distribuciones">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#" onclick="mostrarGrafica('Gantt')">Diagrama Gantt</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" onclick="mostrarGrafica('Sesiones')">Distribución de tareas</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" onclick="mostrarGrafica('Proporciones')">Distribución en proporciones</a>
                                        </li>
                                    </ul>
                                    <div id="diagrama-gantt">
                                        <?php

                                                if (isset($this->fechas) && isset($this->sesiones)):
                                                
                                                    // Primero obtenemos la fecha inicial y la final del proyecto
                                                    $fecha_inicio = substr($this->fechas[0]["fecha_primera_sesion"], 0, 10);
                                                    $fecha_fin = substr($this->fechas[0]["fecha_ultima_sesion"], 0, 10);
                                                        

                                                    $dias = $mDate->diferencia_dias_entre_fechas($fecha_inicio, $fecha_fin);
                                                
                                                    // Generamos la cabecera de la tabla con las fechas obtenidas
                                                ?>
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td>TAREAS</td>

                                                    <?php    
                                                    $time_fecha_inicio = mktime(0,0,0,substr($fecha_inicio,5,2),substr($fecha_inicio,8,2),substr($fecha_inicio,0,4));

                                                    for ($i=0; $i <= $dias; $i++):

                                                        $fecha = date("Y-m-d", $time_fecha_inicio + $i * 60 * 60 * 24);
                                                        $fecha = $mDate->formatearFechaEnDiaMes($fecha);

                                                        // Mostramos todos los dias siguientes a la fecha inicio hasta llegar a fecha fin
                                                        ?>
                                                        <td><?php Utils::print($fecha) ?></td>
                                                    <?php 
                                                    endfor; ?>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                    <?php 
                                                    // echo $intervalo->format('%R%a días');  Resultado: +2 días


                                                    $tarea_actual = null;
                                                    $fecha_actual = null;
                                                    $duracion_acumulada_dia = 0;
                                                    $cont = 1;
                                                    $longitud = count($this->sesiones);

                                                    foreach ($this->sesiones as $sesion):
                                                    
                                                        $fecha_ses = substr($sesion["fecha"], 0, 10);
                                                        $fecha_act = substr($fecha_actual, 0, 10);

                                                        // La tarea actual es distinta a la tarea nueva
                                                        // Ponemos la ultima sesion de la tarea actual y
                                                        // creamos nueva linea de tabla para la nueva tarea
                                                        if ($sesion["tarea"] != $tarea_actual):
                                                        
                                                            if ($cont > 1): ?>
                                                                <td><div class="cuadricula"><?php Utils::print(floor($duracion_acumulada_dia / 60)) ?></div></td>
                                                            <?php 
                                                            endif; ?>
                                                                        
                                                            </tr>
                                                                <tr><td><?php Utils::print($sesion["tarea"]) ?></td>


                                                            <?php
                                                            $tarea_actual = $sesion["tarea"];
                                                            $fecha_actual = $fecha_inicio;

                                                            $duracion_acumulada_dia = 0;
                                                        
                                                        else:
                                                            
                                                            if ($fecha_ses != $fecha_actual):
                                                                if ($duracion_acumulada_dia != 0): ?>
                                                                    <td><div class="cuadricula"><?php Utils::print(round($duracion_acumulada_dia / 60)) ?></div></td>

                                                                    <?php
                                                                        
                                                                    // Obtenemos la diferencia en dias entre la fecha anterior y la que toca en el bucle for
                                                                    $diff = $mDate->diferencia_dias_entre_fechas($fecha_actual, $fecha_ses);

                                                                    //Rellenamos los huecos hasta el dia de la sesion realizada
                                                                    // Si ya hay una sesion de fecha anterior hay que dejar un hueco menos a partir de ahora
                                                                    // ya que empieza a contar ya desde el siguiente dia a fecha actual
                                                                    if ($cont > 2):
                                                                        for ($i=1; $i < $diff; $i++): ?>
                                                                            <td></td>
                                                                        <?php
                                                                        endfor;
                                                                    else:
                                                                        for ($i=1; $i <= $diff; $i++): ?>
                                                                            <td></td>
                                                                        <?php 
                                                                        endfor;
                                                                    endif;

                                                                    $fecha_actual = $fecha_ses;
                                                                    $duracion_acumulada_dia = $sesion["duracion_segs"];
                                                                    $fecha_anterior = $fecha_ses;
                                                                endif;
                                                            
                                                            else:
                                                                $duracion_acumulada_dia = $duracion_acumulada_dia + $sesion["duracion_segs"];
                                                            endif;
                                                        endif;
                                                        
                                                        //Ponemos el ultimo resultado
                                                        $minutos_acumulados_dia = floor($duracion_acumulada_dia / 60);
                                                        if ($cont == $longitud):
                                                            if ($cont == 1): ?>
                                                                <td><div class="cuadricula"><?php Utils::print($minutos_acumulados_dia); ?></div></td>
                                                            
                                                            <?php
                                                            else:
                                                                if ($cont > 2): ?>
                                                                    <td></td><td><div class="cuadricula"><?php Utils::print($minutos_acumulados_dia); ?></div></td>
                                                                
                                                                <?php 
                                                                else: 
                                                                ?>
                                                                    <td><div class="cuadricula"><?php Utils::print($minutos_acumulados_dia); ?></div></td>
                                                                <?php
                                                                endif;
                                                            endif;
                                                        endif;
                                                      
                                                        $cont++;

                                                    endforeach;

                                                    // Finalmente rellenamos las celdas EN BLANCO que quedan de la tarea para que se muestre el fondo de ellas
                                                    $diff = $mDate->diferencia_dias_entre_fechas($fecha_actual, $fecha_fin);
                                                    for ($i=1; $i <= $diff; $i++): 
                                                    ?>
                                                        <td></td>
                                                    <?php 
                                                    endfor;
                                                endif;
                                                ?>
                                            </tbody>
                                            </table>
                                            <div class="alinear-derecha">
                                                <div class="cuadricula">
                                                    <i>duraci&oacute;n en minutos</i>
                                                </div>
                                            </div>
                                    </div>
                                    <div id="diagrama-distribucion-sesiones">
                                        <ul class="distribucion-horas">
                                            <?php
                                                if (isset($this->sesiones)):
                                                                
                                                    $tarea_actual = "";
                                                    foreach ($this->sesiones as $sesion):
                                                            
                                                        // Mostramos cada sesion indicando el numero de horas aproximado (num_segs/3600) que han sido empleados
                                                        $ancho_celda = $sesion["duracion_segs"] / 3600 * 5; // Ancho multiplicamos por 5 para que se vea mayor anchura en la cuadricula
                                                        $duracion_horas = round($sesion["duracion_segs"] / 3600);

                                                        if ($duracion_horas < 1)
                                                            $duracion_horas = 1;

                                                        if ($sesion["tarea"] == $tarea_actual): ?>
                                                            <div class="cuadricula" style="max-width:<?php Utils::print($ancho_celda) ?>px;"><?php Utils::print($duracion_horas) ?> h</div>
                                                        <?php
                                                        else:
                                                            $tarea_actual = $sesion["tarea"];
                                                            // Mostramos la tarea y la primera sesion de dicha tarea indicando el numero de horas aproximado (num_segs/3600) que han sido empleados
                                                            ?>
                                                            </li><li><div class="titulo_tarea"><?php Utils::print($sesion["tarea"]) ?></div><div class="cuadricula" style="max-width:<?php Utils::print($ancho_celda) ?>px;"><?php Utils::print($duracion_horas) ?> h</div>
                                                            <?php
                                                        endif;
                                                    endforeach;
                                                endif;
                                            ?>
                                        </ul>
                                    </div>
                                    <div id="diagrama-proporciones">
                                        <?php
                                            if (isset($this->sesiones)):
                                            
                                                $tarea_actual = null;
                                                $fecha_actual = null;
                                                foreach ($this->sesiones as $sesion):
                                                endforeach;
                                            endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
    </section>
</main>