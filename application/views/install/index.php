<main>
    <section id="contenido">
        <div class="container">
            <div id="instalador">
                <h1><span class="glyphicon glyphicon-home"></span> Pantalla de instalaci&oacute;n</h1>
                <hr>
                <form method="post" name="form" id="form" action="install/run">
                    <ul>
                        <li>
                            <div class="titulo_campo">Servidor</div><input type="text" name="servidor" id="servidor"
                                value="localhost" placeholder="Servidor (localhost, 127.0.0.1)">
                        </li>
                        <li>
                            <div class="titulo_campo">Tipo servidor SQL</div>
                            <select name="tipoServidorDB" id="tipoServidorDB">
                                <option value="mysql">mysql</option>
                            </select>
                        <li>
                            <div class="titulo_campo">Puerto</div><input type="text" name="puerto" id="puerto"
                                value="3306" placeholder="Puerto (3306)">
                        </li>
                        <li>
                            <div class="titulo_campo">Nombre base de datos</div><input type="text" name="nombreDB"
                                id="nombreDB" value="projecttracker">
                        </li>
                        <li>
                            <div class="titulo_campo">Usuario</div><input type="text" name="usuario" id="usuario"
                                value="root">
                        </li>
                        <li>
                            <div class="titulo_campo">Clave</div><input type="password" name="clave" id="clave"
                                value="">
                        </li>
                    </ul>
                    <div class="btn-instalar"><button type="button" class="btn btn-lg btn-primary"
                            onclick="return validar_form_instalacion();"><span class="glyphicon glyphicon-saved"></span>
                            Instalar</button></div>
                </form>
            </div>
        </div>
    </section>
</main>