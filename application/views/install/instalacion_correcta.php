<main>
    <section id="contenido">
        <div class="container">
            <div id="instalador">
                <h1><span class="glyphicon glyphicon-home"></span> Instalación correcta</h1>
                <hr>
                <div class="alert alert-success" role="alert">La instalaci&oacute;n se ha realizado correctamente.</div>
                <div class="btn-instalar"><a href="<?php Utils::print(BASE_URL) ?>"><button type="button"
                            class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-saved"></span> Ir a
                            Projecttracker</button></a></div>
            </div>
        </div>
    </section>
</main>