<main>
    <section id="contenido">
        <div id="instalador">
            <div class="container">
                <h1><span class="glyphicon glyphicon-home"></span> Instalación incorrecta</h1>
                <hr>
                <div class="alert alert-danger" role="alert">La instalaci&oacute;n no se ha realizado correctamente. Inténtalo de nuevo</div>
                <div class="btn-instalar"><a href="<?php Utils::print(BASE_URL); ?>"><button type="button" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-saved"></span> Ir a Projecttracker</button></a></div>
            </div>
        </div>
    </section>
</main>