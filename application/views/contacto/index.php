<main>
    <section id="consulta">
        <div class="container">
            <h1>Cons&uacute;ltanos</h1>
            <hr>
            <div class="row">
                <div class="col-lg-12">
                    <form method="post" name="form" id="form" action="<?php Utils::print(BASE_URL) ?>contacto/enviar">
                        <div id="campo-contacto" class="form-group">
                            <label for="contacto">Persona de contacto * <span class="color-error"><?php isset($this->errores_form["contacto"]) ? Utils::print($this->errores_form["contacto"]["errores"][0]) : null ?></span></label>
                            <input class="form-control" type="text" name="contacto" id="contacto" placeholder="Persona de contacto" size="100" maxlength="100" value="<?php !empty($datos_form["contacto"]) ? Utils::print($datos_form["contacto"]) : null ?>" required>
                        </div>
                        <div id="campo-empresa" class="form-group">
                            <label for="empresa">Empresa <span class="color-error"><?php isset($this->errores_form["empresa"]) ? Utils::print($this->errores_form["empresa"]["errores"][0]) : null ?></span></label>
                            <input class="form-control" type="text" name="empresa" id="empresa" placeholder="Empresa" value="<?php !empty($datos_form["empresa"]) ? Utils::print($datos_form["empresa"]) : null ?>">
                        </div>
                        <div id="campo-email" class="form-group">
                            <label for="email">Email * <span class="color-error"><?php isset($this->errores_form["email"]) ? Utils::print($this->errores_form["email"]["errores"][0]) : null ?></span></label>
                            <input class="form-control" type="text" name="email" id="email" placeholder="Email" size="100" maxlength="100" value="<?php !empty($datos_form["email"]) ? Utils::print($datos_form["email"]) : null ?>" required>
                        </div>
                    
                        <div id="campo-mensaje" class="form-group">
                            <label for="mensaje">Cuerpo del mensaje * <span class="color-error"><?php isset($this->errores_form["mensaje"]) ? Utils::print($this->errores_form["mensaje"]["errores"][0]) : null ?></span></label>
                            <textarea class="form-control" name="mensaje" id="mensaje" placeholder="Escribe aquí la consulta" maxlength="2000" required><?php !empty($datos_form["mensaje"]) ? Utils::print($datos_form["mensaje"]) : null ?></textarea>
                        </div>
                        <div class='aviso-campos-obligatorios'>*Campos obligatorios</div>
                        <div class="btn-toolbar" role="toolbar">
                            <button type="reset" class="btn btn-default" name="limpiar_form" id="limpiar_form">
                                <span class="fas fa-trash"></span>&nbsp; Limpiar
                            </button>
                            <button type="submit" class="btn btn-danger ml-2" name="enviar_form" id="enviar_form">
                                <span class="fas fa-envelope"></span>&nbsp; Enviar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>