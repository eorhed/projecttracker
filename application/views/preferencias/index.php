<main>
    <?php
    /*
    if (class_exists("MiLog"))
    {
        $log = MiLog::getInstance();
        if ($log->hayMensajes())
            echo $log->showLog();
    }
    */
    ?>
    <section id="contenido">
        <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <h1><i class="fa fa-cogs"></i> Preferencias</h1>
                    </div>
                    <div class="col-lg-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb navegacion">
                                <li class="breadcrumb-item">
                                    <i class="fa fa-home"></i>
                                    <a href="<?php Utils::print(BASE_URL) ?>dashboard"> Inicio</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Preferencias</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form method="post" id="form" name="form"
                            action="<?php Utils::print(BASE_URL); ?>preferencias/actualizar" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h2>Datos de perfil</h2>
                                        </div>
                                        <div class="card-body">
                                            <div id="campo-nombre-usuario" class="form-group">
                                                <label for="nombre-usuario">Nombre usuario:
                                                    <?php !empty($this->prefs[0]["usuario"]) ? Utils::print($this->prefs[0]["usuario"]) : "-" ?></label>
                                                <input type="text" name="nombre-usuario"
                                                    value="<?php !empty($this->prefs[0]["usuario"]) ? Utils::print($this->prefs[0]["usuario"]) : "-" ?>"
                                                    disabled>
                                            </div>
                                            <div id="campo-email" class="form-group">
                                                <label for="email">Email: </label>
                                                <input type="text" name="email"
                                                    value="<?php !empty($this->prefs[0]["email"]) ? Utils::print($this->prefs[0]["email"]) : "-" ?>">
                                            </div>
                                            <div id="campo-foto" class="form-group">
                                                <label class="label_foto" for="foto_perfil">Foto: </label> <img
                                                    src="<?php Utils::print(BASE_URL) ?><?php !empty($this->prefs[0]["foto"]) ? Utils::print("application/public/img/user/" . $this->prefs[0]["foto"]) : Utils::print("application/public/img/default.png") ?>"
                                                    width="100" height="100"><br /><br />
                                                <input type="file" name="foto_perfil" id="foto_perfil" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h2>Preferencias seguimiento</h2>
                                        </div>
                                        <div class="card-body">
                                            <div id="campo-moneda" class="form-group">
                                                <label for="moneda">Moneda: </label>
                                                <select id="moneda" name="moneda">
                                                    <option value="€"
                                                        <?= (!empty($this->prefs[0]["moneda"]) && $this->prefs[0]["moneda"] == "€") ? "selected" : "value='€'" ?>>
                                                        €</option>
                                                    <option value="$"
                                                        <?= (!empty($this->prefs[0]["moneda"]) && $this->prefs[0]["moneda"] == "$") ? "selected" : "value='$'" ?>>
                                                        $</option>
                                                </select>
                                            </div>
                                            <div id="campo-precio-por-hora" class="form-group">
                                                <label for="precio_hora">Euros la hora</label>
                                                <input class="form-control" type="range" name="precio_hora"
                                                    id="precio_hora" min="0" max="35" step="1"
                                                    value="<?php !empty($this->prefs[0]["precio_hora"]) ? Utils::print($this->prefs[0]["precio_hora"]) : "15" ?>"
                                                    placeholder="Euros la hora"
                                                    onChange="actualizarPrecioHora(this.value);">
                                                <div class="euros_hora">
                                                    <?php !empty($this->prefs[0]["precio_hora"]) ? Utils::print($this->prefs[0]["precio_hora"]) : "15" ?>
                                                    <?php !empty($this->prefs[0]["moneda"]) ? Utils::print($this->prefs[0]["moneda"]) : "€" ?>
                                                </div>
                                            </div>
                                            <div id="campo-tiempo-max-sesion" class="form-group">
                                                <label for="tiempo_max_sesion">Tiempo máx sesión (min)</label>
                                                <input class="form-control" type="range" name="tiempo_max_sesion"
                                                    id="tiempo_max_sesion" min="0" max="480" step="1"
                                                    value="<?php !empty($this->prefs[0]["tiempo_max_sesion"]) ? Utils::print($this->prefs[0]["tiempo_max_sesion"]) : "15" ?>"
                                                    placeholder="Euros la hora"
                                                    onChange="actualizarTiempoMaxSesion(this.value);">
                                                <div class="info_tiempo_max_sesion">
                                                    <?php !empty($this->prefs[0]["tiempo_max_sesion"]) ? Utils::print($this->prefs[0]["tiempo_max_sesion"]) : "120" ?>
                                                    min</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h2>Preferencias de la interfaz</h2>
                                        </div>
                                        <div class="card-body">
                                            <div id="campo-idioma" class="form-group">
                                                <label for="idioma">Idioma: </label>
                                                <select id="idioma" name="idioma">
                                                    <option value="castellano"
                                                        <?php (!empty($this->prefs[0]["idioma"]) && $this->prefs[0]["idioma"] == "castellano") ? "selected" : null ?>>
                                                        Castellano</option>
                                                    <option value="english"
                                                        <?php (!empty($this->prefs[0]["idioma"]) && $this->prefs[0]["idioma"] == "english") ? "selected" : null ?>>
                                                        English</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="btn-toolbar" role="toolbar"
                                        style="justify-content: center; display: flex;">
                                        <button type="submit" class="btn btn-lg btn-danger ml-2" name="enviar_form"
                                            id="enviar_form">
                                            <i class="fa fa-save"></i>&nbsp; Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </section>
</main>