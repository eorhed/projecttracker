<main>
    <section id="identificarse">
        <h1>Identificarse</h1>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <form method="POST" name="form" id="form" action="<?php Utils::print(BASE_URL) ?>identificarse/run">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Usuario" required>
                            <div class="invalid-feedback">Rellene este campo!</div>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </div>
                            <input type="password" class="form-control" name="clave" id="clave" placeholder="Clave" required>
                            <div class="invalid-feedback">Rellene este campo!</div>
                        </div>
                        <div class="btn-toolbar" role="toolbar">
                            <button class="btn btn-lg btn-primary mx-auto">
                            <i class="fa fa-check">&nbsp;</i>Enviar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>