<main>
<section id="contenido">
    <div class="container">
        <section id="agregar-nueva-tarea">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Agregar nueva tarea</h1>
                    <hr>
                </div>
            </div>
            <div class="row">
                <form method="post" action="<?php Utils::print(BASE_URL) ?>tareas/agregar">
                    <div class="col-lg-12">
                        <div id="campo-nombre" class="form-group">
                            <label for="nombre">Tarea</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" placeholder="Nombre tarea" maxlength="100">
                        </div>
                        <div id="campo-comentarios" class="form-group">
                            <label for="comentarios">Comentarios</label>
                            <textarea class="form-control" name="comentarios" id="comentarios" placeholder="Comentarios" maxlength="300"></textarea>
                        </div>
                        <div class="btn-toolbar pull-right" role="toolbar">
                            <button class="btn btn-default btn-success btn-agregar">
                                <i class="fa fa-plus"></i> Agregar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <section id="tareas">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9">
                        <h1><i class="fa fa-tasks"></i> Tareas</h1>
                    </div>
                    <div class="col-lg-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb navegacion">
                                <li class="breadcrumb-item">
                                    <i class="fa fa-home"></i>
                                    <a href="<?php Utils::print(BASE_URL) ?>dashboard"> Inicio</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Tareas</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="btn-toolbar toolbar" role="toolbar">
                            <button class="btn btn-default btn-primary" onclick="document.getElementById('agregar-nueva-tarea').style.display = 'block';">
                                <i class="fa fa-plus"></i> Agregar nueva tarea
                            </button>
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td width="20">#</td>
                                    <td>TAREAS</td>
                                    <td>COMENTARIOS</td>
                                    <td>ESTADO</td>
                                    <td width="160" align="center">FECHA CREACI&Oacute;N</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if (isset($this->tareas)):
                                
                                    $cont = 1;
                                    foreach($this->tareas as $tarea): ?>
                                        <tr onclick="irA('<?php Utils::print(BASE_URL) ?>tareas/editar/<?php Utils::print($tarea["idtarea"]) ?>');">

                                        <?php include_once $_SERVER['DOCUMENT_ROOT']."/projecttracker/application/public/php/func_fechas.php"; // Para usar funciones de fechas
                                        ?>
                                            <td><?php Utils::print($cont) ?></td>
                                            <td><?php Utils::print($tarea["nombre"]) ?></td>
                                            <td><?php Utils::print($tarea["comentarios"]) ?></td>
                                            <td>
                                                <?php ($tarea["habilitado"] == "1") ? "Habilitado" : "Deshabilitado" ?>
                                            </td>
                                            <?php    
                                                if (class_exists("MiDate"))
                                                    $mDate = MiDate::getInstance();
                                                else
                                                    return;
                                            ?>
                                            
                                            <td align="center">
                                                <?php Utils::print($mDate->formatearFecha($tarea["fecha_creacion"])) ?>
                                            </td>
                                        </tr>

                                <?php $cont++;

                                    endforeach;

                                else: ?>
                                    <tr><td colspan="5"><?php Utils::print(msj_sin_resultados) ?></td></tr>
                        <?php endif; ?>

                            </tbody> 
                        </table>
                        <div class="float-right">
                            <div class="num-resultados">
                            <?php if (isset($cont)): ?>
                                <?php Utils::print($cont-1 . " resultados"); 
                                endif; ?>
                            </div>
                        </div>
                        <div><i>(Haz click en una tarea para editar sus datos)</i></div>
                    </div>
                </div>
            </div>
        </section>
        </div>
    </section>
</main>