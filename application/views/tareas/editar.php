<main>
    <section id="contenido">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Editar tarea: <span class="color-primario"><?php isset($this->datos_tarea) ? Utils::print($this->datos_tarea[0]["nombre"]) : null;?></span></h1>
                    <hr>
                </div>
            </div>
            <div class="row">
                <form method="post">
                    <div class="col-lg-6">
                        <div id="campo-nombre" class="form-group">
                            <label for="nombre">Tarea</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" maxlength="100" placeholder="Tarea" value="<?php isset($this->datos_tarea) ? Utils::print($this->datos_tarea[0]["nombre"]) : null; ?>">
                        </div>
                        <div id="campo-comentarios" class="form-group">
                            <label for="comentarios">Comentarios</label>
                            <textarea class="form-control" name="comentarios" id="comentarios" maxlength="300" placeholder="Comentarios"><?php isset($this->datos_tarea) ? Utils::print($this->datos_tarea[0]["comentarios"]) : null; ?></textarea>
                        </div>
                        <div class="btn-toolbar pull-right" role="toolbar">
                            <input type="hidden" name="tarea" id="tarea" value="<?php $this->datos_tarea[0]["idtarea"]; ?>">
                            <input type="hidden" name="nombre_anterior" id="nombre_anterior" value="<?php isset($this->datos_tarea) ? Utils::print($this->datos_tarea[0]["nombre"]) : null; ?>">
                            <input type="hidden" name="operacion" id="operacion" value="">
                            <?php if (isset($this->datos_tarea)):
                                    if ($this->datos_tarea[0]["habilitado"] == 1): ?>
                                        <button class="btn btn-danger btn-deshabilitar" name="deshabilitar" id="deshabilitar" onclick="cambiarOperacion('deshabilitar')">
                                            <span class="fa fa-trash"></span> Deshabilitar
                                        </button>
                                    <?php elseif ($this->datos_tarea[0]["habilitado"] == 0): ?>
                                        <button class="btn btn-default btn-deshabilitar" name="habilitar" id="habilitar" onclick="cambiarOperacion('habilitar')">
                                            <span class="fa fa-ok"></span> Habilitar
                                        </button>
                                <?php endif; 
                                endif;?>
                            <button class="btn btn-success btn-agregar" name="guardar" id="guardar" onclick="cambiarOperacion('guardar')">
                                <span class="glyphicon glyphicon-edit"></span> Guardar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>