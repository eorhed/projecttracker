<aside id="sidebar">
    <section id="nav-sidebar">
        <h1>MENU</h1>

        <ul class="acorh">
            <li><a href="<?php Utils::print(BASE_URL); ?>"><i class="fa fa-home"></i> Inicio</a></li>
            <li><a href="<?php Utils::print(BASE_URL); ?>dashboard"><i class="fa fa-tachometer-alt"></i> Panel de control</a></li>
            <li id="trackear"><a href="#trackear"><i class="fa fa-history"></i> Trackear <i class="fa fa-caret-down"></i></a>
                <ul>
                    <li><a href="<?php Utils::print(BASE_URL); ?>trackear"><i class="fa fa-stopwatch"></i> Tracking autom&aacute;tico</a></li>
                    <li><a href="<?php Utils::print(BASE_URL); ?>agregar_registro"><i class="fa fa-plus"></i> Agregar registro</a></li>
                </ul>
            </li>
            <li><a href="<?php Utils::print(BASE_URL); ?>informes"><i class="fa fa-copy"></i> Informes</a></li>
            <li id="administrar"><a href="#administrar"><i class="fa fa-cog"></i> Administrar <i class="fa fa-caret-down"></i></a>
                <ul>
                    <li><a href="<?php Utils::print(BASE_URL); ?>clientes"><i class="fa fa-user"></i> Clientes</a></li>
                    <li><a href="<?php Utils::print(BASE_URL); ?>proyectos"><i class="fa fa-file-code"></i> Proyectos</a></li>
                    <li><a href="<?php Utils::print(BASE_URL); ?>tareas"><i class="fa fa-tasks"></i> Tareas</a></li>
                    <li><a href="<?php Utils::print(BASE_URL); ?>sesiones"><i class="fa fa-clock"></i> Sesiones</a></li>
                </ul>
            </li>
            <li><a href="<?php Utils::print(BASE_URL); ?>preferencias"><i class="fa fa-cogs"></i> Preferencias</a></li>
            <li><a href="<?php Utils::print(BASE_URL); ?>salir"><i class="fa fa-sign-out-alt"></i> Salir</a></li>
        </ul>
        <!-- <ul>
            <li class="desplegable">
                    <a href="#navigation">Trackear <i class="fa fa-caret-down"></i></a>
                    <ul id="subopciones">
                        <li><a name="#seguimiento">Seguimiento autom&aacute;tico</a></li>
                        <li><a name="#agregar_registro">Agregar registro manualmente</a></li>
                    </ul>
            </li>
            
            <li><a href="#">Administrar</a></li>
        </ul>
        <div class="row">
            <div class="col-lg-12">
                Otra sección
            </div>
        </div> -->
    </section>
</aside>