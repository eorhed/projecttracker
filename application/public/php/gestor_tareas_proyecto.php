<?php
class GestorTareas extends PDO
{
    function __construct()
	{
        $fich_config = "../../config/database.ini.php";
		if (file_exists($fich_config))
		{
			$config = parse_ini_file($fich_config);
			$host = $config["db_driver"].":host=".$config["db_host"].";dbname=".$config["db_name"];
			parent::__construct($host,$config["db_user"],$config["db_password"]);
        }
	}

    /**
     * Dado un id de proyecto de un usuario devuelve las tareas que el usuario asignó a dicho proyecto
     *
     * @param string $idproyecto
     * @param string $usuario
     * @return array<string> $tareas_asignadas
     */
    function getTareasAsignadasAlProyecto($idproyecto,$usuario)
    {
        try
        {
            // Obtenemos las tareas asignadas por el usuario para el proyecto
            $sql = "SELECT T.idtarea, T.nombre as nombre_tarea
                    FROM tareas_proyecto as TP JOIN proyectos as P JOIN tareas as T
                    ON TP.idproyecto = P.idproyecto and TP.idtarea = T.idtarea 
                    WHERE P.usuario = :usuario and P.idproyecto = :proyecto and TP.asignado = '1'
                    ORDER BY T.nombre ASC";

            $stmt = $this->prepare($sql);
            $stmt->execute(array
            (
                'usuario' => $usuario,
                'proyecto' => $idproyecto
            ));

            $data = $stmt->fetchAll();	// Almacenamos los resultados
            
            if ($stmt->rowCount($data) > 0)
                return $data;   // Se han encontrado tareas asignadas al proyecto en la BD, las devolvemos
            else
                return 0;    //No se ha encontrado ninguna tarea asignada a dicho proyecto del usuario

        }
        catch(PDOException $e)
        {
            // echo $e->getMessage();
            return null;
        }
    }
}    


if (isset($_POST))
{
    $gT = new GestorTareas();

    include_once "../../libs/session.php";
    Session::init();
    $usuario = Session::get("usuario");
    $idproyecto = $_POST["idproyecto"];
    $response = array();
    
    $datos = $gT->getTareasAsignadasAlProyecto($idproyecto,$usuario);
    if (isset($datos) && is_array($datos))
    {
        $response["exito"] = true;
        $response["tareas"] = $datos;
    }
    else if ($datos == 0)
        $response["exito"] = true;
    else
    {
        $response["exito"] = false;
        $response["error"] = "Error";
    }
        
    
    
    //Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
    header('Content-type: application/json; charset=utf-8'); // Imprescindible para que se recoja el JSON en el .js
    echo json_encode($response);
}
?>