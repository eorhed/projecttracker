<?php
class DB extends PDO
{
    function __construct()
	{
		$fich_config = "../../../application/config/database.ini.php";
		if (file_exists($fich_config))
		{
			$config = parse_ini_file($fich_config);
			// $host = DB_TYPE.":host=".DB_HOST.";dbname=".DB_NAME;
			// parent::__construct($host,DB_USER,DB_PASSWORD);
			$host = $config["db_driver"].":host=".$config["db_host"].";dbname=".$config["db_name"];
			parent::__construct($host,$config["db_user"],$config["db_password"]);
		}
	}

    function comprobar_usuario($usuario)
    {
        $sql = "SELECT * FROM usuarios WHERE usuario = :usuario";
		$stmt = $this->prepare($sql);
		$stmt->execute(array
				('usuario' => $usuario
				)) or die("Fallo al consultar nuevo usuario en la BD");

		$data = $stmt->fetchAll();
        
        return ($stmt->rowCount($data) > 0);
    }
}    

$db = new DB();
if (isset($_POST["usuario"]))
{
    if ($db->comprobar_usuario($_POST["usuario"]))
        $data["existe"] = true;
    else
        $data["existe"] = false;
    echo json_encode($data);
}
?>