<?php
class DB extends PDO
{
    function __construct()
	{
        // $host = "mysql:host=localhost;dbname=projecttracker_2017";
        // parent::__construct($host,"root","");
        
        $fich_config = "../../../application/config/database.ini.php";
		if (is_readable($fich_config))
		{
			$config = parse_ini_file($fich_config);
			// $host = DB_TYPE.":host=".DB_HOST.";dbname=".DB_NAME;
			// parent::__construct($host,DB_USER,DB_PASSWORD);
			$host = $config["db_driver"].":host=".$config["db_host"].";dbname=".$config["db_name"];
			parent::__construct($host, $config["db_user"], $config["db_password"]);
		}
	}


    /**
     * Dados los datos de la sesión inserta un nuevo registro en la tabla sesiones_tarea_proyecto de la BD
     * junto con dichos datos
     * 
     * @param array Datos de la sesion a guardar(idproyecto, idtarea, duracion, fecha, precio_hora, comentarios)
     * @return boolean true si inserta bien en la BD, false si no realiza la operacion
     */
    function guardarSesion($sesion)
    {
        $d_segs = explode(":", $sesion["duracion"]);
        $duracion_segs = $d_segs[0] * 3600 + $d_segs[1] * 60 + $d_segs[2];
        
        try
        {
            $sql = "INSERT INTO sesiones_tarea_proyecto (idproyecto, idtarea, fecha, duracion_segs, duracion, precio_x_hora, comentarios) 
                    VALUES (:idproyecto, :idtarea, :fecha, :duracion_segs, :duracion, :precio_hora, :comentarios)";
            $stmt = $this->prepare($sql);
            $stmt->execute(array
                    ("idproyecto" => $sesion["idproyecto"],
                    "idtarea" => $sesion["idtarea"],
                    "fecha" => $sesion["fecha"],
                    "duracion_segs" => $duracion_segs,
                    "duracion" => $sesion["duracion"],
                    "precio_hora" => $sesion["precio_hora"],
                    "comentarios" => $sesion["comentarios"]
                    ));

            $lastID = $this->lastInsertId();

			if ($lastID > 0)
                return true;
            else
                return false;
        }
        catch(PDOException $e)
        {
            // echo $e->getMessage();
            return false;
        }
    }


    /**
     * Comprueba que el idproyecto e idtarea pasados por POST pertenecen al usuario 
     * y además dicha tarea está asignada a dicho proyecto
     * 
     * @param string $idproyecto
     * @param string $idtarea
     * @return boolean true si está bien validado (existe la tarea asignada al proyecto del usuario), false si no
     */
    function validarSesion($sesion)
    {
        require_once "../../../application/libs/session.php";
        Session::init();

        try
        {
            $sql = "SELECT TP.idtarea_proyecto FROM tareas_proyecto as TP JOIN proyectos as P JOIN tareas as T 
                ON TP.idproyecto = P.idproyecto AND TP.idtarea = T.idtarea
                WHERE P.usuario = :usuario AND TP.idproyecto = :idproyecto AND TP.idtarea = :idtarea
                      AND P.habilitado = '1' AND TP.asignado = '1'";

            $stmt = $this->prepare($sql);
            $stmt->execute(array
                    ("idproyecto" => $sesion["idproyecto"],
                    "idtarea" => $sesion["idtarea"],
                    "usuario" => Session::get("usuario")
                    ));

            $data = $stmt->fetchAll();	// Almacenamos los resultados
            
            if ($stmt->rowCount($data) == 1)
                return true;
            else
                return false;
        }
        catch(PDOException $e)
        {
            // echo $e->getMessage();
            return false;
        }
    }
}    

if (!empty($_POST))
{
    $db = new DB();

    $sesion = array(
                    "idproyecto" => filter_input(INPUT_POST, "idproyecto", FILTER_SANITIZE_NUMBER_INT),
                    "idtarea" => filter_input(INPUT_POST, "idtarea", FILTER_SANITIZE_NUMBER_INT),
                    "duracion" => filter_input(INPUT_POST, "duracion", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    "precio_hora" => filter_input(INPUT_POST, "precio_por_hora", FILTER_SANITIZE_NUMBER_INT),
                    "comentarios" => filter_input(INPUT_POST, "comentarios", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    "fecha" => filter_input(INPUT_POST, "fecha", FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    );

    

    // Cambiamos fecha(dd/mm/YYYY) a formato fechaBD (yyyy-mm-dd)
    $dia = substr($sesion["fecha"], 0, 2);
    $mes = substr($sesion["fecha"], 3, 2);
    $anio = substr($sesion["fecha"], 6, 4);
    $hora = substr($sesion["fecha"], 11, 8);
    $fecha = $anio."-".$mes."-".$dia . " " . $hora;

    $sesion["fecha"] = $fecha;

    
    
    if ($db->validarSesion($sesion))
    {
        if ($db->guardarSesion($sesion))
            $response["exito"] = true;
        else
            $response["exito"] = false;
    }
    else
    {
        $response["exito"] = false;
        $response["error_manipulacion_datos"] = true;
    }
    
    //Aunque el content-type no sea un problema en la mayoría de casos, es recomendable especificarlo
    header("Content-type: application/json; charset=utf-8"); // Imprescindible para que se recoja el JSON en el .js
    ?>
    <?= json_encode($response); ?>
<?php
}
?>