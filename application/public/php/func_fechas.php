<?php

class MiDate
{
    /**
     * Variable que almacena la instacia única de la clase MiDate
     *
     * @var MiDate $instancia
     */
    private static $instancia;

    /**
     * Función que devuelve la instancia única de la MAE (Patrón diseño Singleton)
     *
     * @return void
     */
    public static function getInstance()
    {
        if (!isset(self::$instancia))
        {
            $clase = __CLASS__;
            self::$instancia = new $clase;
        }
        return self::$instancia;
    }

    /**
     * Devuelve el numero de días de diferencia que hay entre dos fechas en formato DB
     
     * Dadas dos fechas en formato DB (YYYY-mm-dd)
     * devuelve el numero de días de diferencia que hay entre ellas
     * 
     * @param string $fecha1 formato DB
     * @param string $fecha2 formato DB
     * @return int numero de días de diferencia que hay entre ellas
     */
    function diferencia_dias_entre_fechas($fecha1,$fecha2)
    {
        $datetime1 = new DateTime($fecha1);
        $datetime2 = new DateTime($fecha2);
        $intervalo = $datetime1->diff($datetime2);

        return $intervalo->format('%a');
    }

    /**
     * Función que dada una fecha en formato DB (YYYY-mm-dd)
     * 
     * @return string fecha en formato (dd/mm/YYYY)
     */
    function formatearFecha($fecha)
    {
        return substr($fecha,8,2)."/".substr($fecha,5,2)."/".substr($fecha,0,4);
    }

    /**
     * Función que dada una fecha en formato DB (YYYY-mm-dd)
     * 
     * @return string fecha en formato (dd/mm)
     */
    function formatearFechaEnDiaMes($fecha)
    {
        return substr($fecha,8,2)."/".substr($fecha,5,2);
    }

    /** 
     * Función que dada una fecha en formato DB (YYYY-mm-dd Hrs:mins:segs)
     * 
     * @return string fechahora en formato (dd/mm/YYYY  Hrs:mins:segs)
    */
    function formatearFechaHora($fecha)
    {
        return substr($fecha,8,2)."/".substr($fecha,5,2)."/".substr($fecha,0,4) . " " . substr($fecha,11,8);
    }

    /**
     * Función que dado un tiempo en segundos 
     * 
     * @return string formato h horas m minutos s segundos
     */
    /**
     * Función que dado un tiempo en segundos
     *
     * @param int $tiempo_en_segundos
     * @param string $modo
     * @return void
     */
    function obtTiempoHMS($tiempo_en_segundos, $modo="h:m:s") {
        $horas = floor($tiempo_en_segundos / 3600);
        $minutos = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
        $segundos = $tiempo_en_segundos - ($horas * 3600) - ($minutos * 60);

        if ($modo == "h:m")
            return $horas . ' h ' . $minutos . " m";
        else if ($modo == "h:m:s")
            return $horas . "h " . $minutos . "m " . $segundos . "s";
        else
            return null;
    }
}
?>