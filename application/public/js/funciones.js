/**
 * Dado un nombre de proyecto de un usuario devuelve las tareas que el usuario asignó a dicho proyecto
 *
 * @param string $proyecto
 * @return array<string> $tareas_asignadas
 */
function cargarTareasProyecto() {
    var idproyecto = $("#proyecto").val();

    // var posting = $.post(url,$( "#form" ).serialize(),"json");
    // console.log("Lanzando .post");

    var url = "application/public/php/gestor_tareas_proyecto.php";
    var posting = $.post(url, {
        idproyecto: idproyecto
    }, "json");



    posting.done(function (response) {
        // console.log("Devuelto .post");

        if (response.exito) {
            $("#tarea").html("");
            $("#info-tarea").hide();

            if (response.tareas) {
                // Deshabilitamos los botones de comenzar seguimiento("trackear")
                $("#seguimientos button").prop("disabled", false);
                $("#configurar-trackeo button").prop("disabled", false);

                // Por cada tarea creamos una etiqueta option
                var cont = 0;
                response.tareas.forEach(function (tarea) {
                    if (tarea[0] !== "0") {
                        if (cont == 0){
                            $("#tarea").append("<option value='" + tarea[0] + "' selected> " + tarea[1] + "</option>");
                        }
                        else{
                            $("#tarea").append("<option value='" + tarea[0] + "'> " + tarea[1] + "</option>");
                        }
                    }
                    cont++;
                }, this);
            } else {
                // Habilitamos los botones de comenzar seguimiento("trackear")
                $("#seguimientos button").prop("disabled", true);
                $("#configurar-trackeo button").prop("disabled", true);

                // $("#tarea").append("<option disabled selected>Este proyecto no tiene ninguna tarea asignada todavía</option>");
                $("#tarea").after("<div class='alert alert-warning' id='info-tarea' role='alert'>No puedes trackear porque este proyecto no tiene ninguna tarea asignada todavía.<br/> Puedes crear nuevas tareas desde <a href='tareas'>Administrar->Tareas</a> y luego asignarlas desde <a href='proyectos'>Administrar->Proyectos</a></div>");
            }
        } else {
            if (response){
            // Se ha producido un error al obtener los datos en la BD
            // Mostramos el correspondiente error por pantalla
            $("#tarea").after("<div class='alert alert-danger' id='info' role='alert'>Se ha producido un error al obtener los datos</div>");
            // $("#info").html("<div class='alert alert-danger' id='info' role='alert'>La sesi&oacute;n no se ha podido guardar con &eacute;xito</div>");
            }
        }
    });
}

// $(document).on("ready",function() Obsoleta y eliminada en jquery 3
$(document).ready(function () {

    $(".desplazador a").on("click", function (e) {
        /* Desplazamiento suave cuando se produce desplazamiento en la página al pulsar un enlace local de la web */
        e.preventDefault();
        var link = $(this);
        var anchor = link.attr("href");
        $(this).blur(); //Le quitamos el foco al link para que no se vea en Firefox donde se ha pinchado
        $("html, body").stop().animate({
            scrollTop: $(anchor).offset().top
        }, 1200);
    });

    $("nav.mobile a").on("click", function (e) {
        $("nav.mobile").toggle();
    });

    $("#btn_menu_movil").on("click", function (e) {
        e.preventDefault();
        $("nav.mobile").toggle();
    });


    if ($("#proyecto")) {
        if ($("#proyecto").val() != null) {
            cargarTareasProyecto();
        }
    }
});

/**
 * Valida todos los datos del formulario de registro del sistema
 */
function validarFormRegistro() {
    //Recogemos los datos del formulario
    var miform = document.forms.form;
    var usuario = document.getElementById("usuario").value;
    var email = document.getElementById("email").value;
    var clave = document.getElementById("clave").value;
    var repetirClave = document.getElementById("repetir-clave").value;

    // Expresión regular para validar el formato del email
    //var filtroemail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var filtroemail = /^(([^<>()[\]\.,;:'\s@\"]+(\.[^<>()[\]\.,;:'\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:'\s@\"]+\.)+[^<>()[\]\.,;:'\s@\"]{2,})$/i;


    $(".has-error").removeClass("has-error");

    $(".mensaje_error").remove(); //Borramos todos los divs de mensajes de errores de la validacion anterior

    if (usuario == null || usuario.length == 0 || /^\s+$/.test(usuario)) {
        // Si el campo usuario de contacto está vacío, 
        // lo enmarcamos en un cuadrado rojo y mostramos el mensaje de error
        $("#campo-usuario").addClass("has-error");
        $("#usuario").after("<div class='mensaje_error'>El campo usuario de contacto es obligatorio rellenarlo</div>");

        // $("html, body").stop().animate({scrollTop: $("#usuario_contacto").offset().top-200}, 1200);
        miform.usuario.focus(); //Ponemos el foco en el campo erróneo
        return false;
    } else if (usuario.length < 4) {
        // Si el campo usuario de contacto está vacío, 
        // lo enmarcamos en un cuadrado rojo y mostramos el mensaje de error
        $("#campo-usuario").addClass("has-error");
        $("#usuario").after("<div class='mensaje_error'>El campo usuario de contacto debe tener al menos 4 caracteres</div>");


        // $("html, body").stop().animate({scrollTop: $("#usuario_contacto").offset().top-200}, 1200);
        miform.usuario.focus(); //Ponemos el foco en el campo erróneo
        return false;
    } else if (!isNaN(usuario)) {
        // Si el campo usuario de contacto tiene números,
        // lo enmarcamos en un cuadrado rojo y mostramos el mensaje de error
        $("#campo-usuario").addClass("has-error");
        $("#usuario").after("<div class='mensaje_error'>El campo usuario de contacto no puede contener s&oacute;lo n&uacute;meros</div>");


        miform.usuario.focus(); //Ponemos el foco en el campo erróneo

        return false;
    } else if (email == null || email.length == 0 || /^\s+$/.test(email)) {
        // Si el campo email está vacío, 
        // los campos anteriores que ya están bien rellenados les ponemos un borde verde
        // y a este campo erróneo lo enmarcamos en un cuadrado rojo y mostramos el mensaje de error
        // $("#usuario").css("border","0px");
        // $("#usuario").css("border-bottom","1px solid green");

        $("#campo-usuario").addClass("has-success");

        $("#campo-email").addClass("has-error");
        $("#email").after("<div class='mensaje_error'>El campo email es obligatorio rellenarlo</div>");

        miform.email.focus(); //Ponemos el foco en el campo erróneo

        return false;
    } else if (!filtroemail.test(email)) {
        /* Si el campo email está mal escrito (no tiene el formato correcto),
           los campos anteriores que ya están bien rellenados les ponemos un borde verde
           y a este campo erróneo lo enmarcamos en un cuadrado rojo y mostramos el mensaje de error
        */

        $("#campo-usuario").addClass("has-success");

        $("#campo-email").addClass("has-error");
        $("#email").after("<div class='mensaje_error'>El email escrito no tiene el formato correcto. Ej: xxxxx@xxx.xxx</div>");

        miform.email.focus(); //Ponemos el foco en el campo erróneo

        return false;
    } else if (clave == null || clave.length == 0 || /^\s+$/.test(clave)) {
        $("#campo-usuario").addClass("has-success");
        $("#campo-email").addClass("has-success");

        // Si el campo usuario de contacto está vacío, 
        // lo enmarcamos en un cuadrado rojo y mostramos el mensaje de error
        $("#campo-clave").addClass("has-error");
        $("#clave").after("<div class='mensaje_error'>El campo clave es obligatorio rellenarlo</div>");

        // $("html, body").stop().animate({scrollTop: $("#usuario_contacto").offset().top-200}, 1200);
        miform.clave.focus(); //Ponemos el foco en el campo erróneo
        return false;
    } else if (clave.length < 8) {
        $("#campo-usuario").addClass("has-success");
        $("#campo-email").addClass("has-success");

        // Si el campo usuario de contacto está vacío, 
        // lo enmarcamos en un cuadrado rojo y mostramos el mensaje de error
        $("#campo-clave").addClass("has-error");
        $("#clave").after("<div class='mensaje_error'>El campo clave debe tener al menos 8 caracteres</div>");


        // $("html, body").stop().animate({scrollTop: $("#usuario_contacto").offset().top-200}, 1200);
        miform.clave.focus(); //Ponemos el foco en el campo erróneo
        return false;
    } else if (clave != repetirClave) {

        $("#campo-usuario").addClass("has-success");
        $("#campo-email").addClass("has-success");
        $("#campo-clave").addClass("has-success");

        // Si el campo usuario de contacto está vacío, 
        // lo enmarcamos en un cuadrado rojo y mostramos el mensaje de error
        $("#campo-repetir-clave").addClass("has-error");
        $("#repetir-clave").after("<div class='mensaje_error'>El campo repetir-clave debe coincidir con el contenido del campo clave</div>");


        // $("html, body").stop().animate({scrollTop: $("#usuario_contacto").offset().top-200}, 1200);
        $("#repetir-clave").focus(); //Ponemos el foco en el campo erróneo
    } else {
        $(".form-group").addClass("has-success");
        miform.submit();
    }
}

/**
 * Valida si existe el nombre de usuario en la BD
 */
function validarExisteUsuario() {
    var user = document.getElementById("usuario").value;
    var url = "application/public/php/existe_usuario.php";
    var posting = $.post(url, {
        usuario: user
    });

    posting.done(function (data) {
        if (data == "{\"existe\":true}") {
            $(".mensaje_error").remove();
            $("#campo-usuario").addClass("has-error");
            $("#usuario").after("<div class='mensaje_error'>El usuario ya existe. Prueba con otro.</div>");
        } else {
            $(".mensaje_error").remove();
            $("#campo-usuario").removeClass("has-error");
        }
    });
}

/**
 * Dado un valor pasado como parametro actualizamos la capa euros_hora con el valor actualizado
 */
function actualizarPrecioHora(valor) {
    $(".euros_hora").html(valor + " €");
}

/**
 * Dado un valor pasado como parametro actualizamos la capa tiempo_max_sesion con el valor actualizado
 */
function actualizarTiempoMaxSesion(valor) {
    $(".info_tiempo_max_sesion").html(valor + " min");
}

/**
 * Validar formulario contacto
 */
function validarFormContacto2() {

    //Recogemos los datos introducidos en el formulario
    var miform = document.forms.form_consulta;
    var persona = miform.persona_contacto.value;
    var empresa = miform.nombre_empresa.value;
    var email = miform.email_contacto.value;
    var texto = miform.mensaje.value;
    $("#info").html("");

    $("#info").fadeIn("slow");
}

/**
 * Obtenemos la fechahora actual y la devolvemos en el formato dd/mm/YYYY hh:mm:ss
 */
function obtFechaHoraActual() {
    // Obtenemos los datos de la fecha completa
    var d = new Date();

    var dia = d.getDate();
    var mes = d.getMonth() + 1;
    var anio = d.getFullYear();
    var horas = d.getHours();
    var minutos = d.getMinutes();
    var segundos = d.getSeconds();

    // Antenponemos un 0 si la var es < 10 para que quede la fecha en formato dd/mm/YYYY HH:ii:ss
    dia = dia < 10 ? 0 + dia.toString() : dia;
    mes = mes < 10 ? 0 + mes.toString() : mes;
    horas = horas < 10 ? 0 + horas.toString() : horas;
    minutos = minutos < 10 ? 0 + minutos.toString() : minutos;
    segundos = segundos < 10 ? 0 + segundos.toString() : segundos;

    return dia + "/" + mes + "/" + anio + " " + horas + ":" + minutos + ":" + segundos;
}

tiempo = 0;
sesion = null;

/**
 * Iniciamos el cronómetro con los datos de seguimiento configurados
 */
function comenzar_seguimiento() {

    $("#seguimientos").show();

    tiempo = 0;

    $("li.sin-registros").remove(); // Borramos la fila con mensaje de que no hay registros en la tabla

    $(".btn-comenzar2").hide(); // Hacemos desaparecer el boton Trackear2 de la lista de sesiones

    var tarea_seleccionada = $("#tarea option:selected").html(); // Obtenemos el nombre de la tarea seleccionada

    var fecha = obtFechaHoraActual(); // Obtenemos la fecha y hora actuales

    var num = $("#seguimientos ul > li").length + 1;

    // Abrimos un nuevo elemento de la lista para mostrar la info de la nueva sesion
    $("#seguimientos ul").append("<li><div class='info_sesion'><div class='num'>#" + num + "</div>" +
        "<div class='fecha'>" + fecha + " </div>" +
        "<div class='tarea_seleccionada'>" + tarea_seleccionada + " </div>" +
        "<div class='tiempo'>00:00:00</div></div></li>");
    
    $(".btn-comenzar").prop("disabled", true);
    sesion = setInterval(seguimiento, 1000);
    $(".btn-pausar").html("<span class='glyphicon glyphicon-pause'></span> Pausar");
    $(".btn-pausar").show();
    $(".btn-guardar").show();
    $("#info").empty();
    $("html, body").stop().animate({
        scrollTop: $(".btn-pausar").offset().top
    }, 1200);
}


/**
 * Pausamos la sesión de seguimiento actualmente activa
 */
function pausar_seguimiento() {

    if (sesion) {
        $(".btn-pausar").html("<span class='glyphicon glyphicon-play'></span> Seguir");
        clearInterval(sesion);
        sesion = null;
    } else {
        $(".btn-pausar").html("<span class='glyphicon glyphicon-pause'></span> Pausar");
        sesion = setInterval(seguimiento, 1000);
    }

}

/**
 * Finalizamos la sesión de seguimiento activa y la guardamos
 */
function guardar_seguimiento() {
    //Paramos la sesion y la guardamos en la BD
    if (sesion)
        clearInterval(sesion);

    // Guardamos (y validamos dentro de guardar) la sesion en la BD
    guardar_sesion_tarea();
    $(".btn-pausar").hide();
    $(".btn-guardar").hide();
    $(".btn-comenzar").prop("disabled", false);
    $(".btn-comenzar2").show();
}

/**
 * Suma un segundo al cronómetro en formato hh:mm:ss
 * En caso de que sean correctos, se guarda la sesión en la BD
 */
function seguimiento() {
    tiempo = tiempo + 1;
    var t = transformar_tiempo(tiempo);
    $("ul > li:last-child() .tiempo").html(t);
}


/**
 * Función que dado un tiempo expresado en segundos
 * lo transforma a horas:minutos:segundos. Formato tipo hh:mm:ss
 */
function transformar_tiempo(tiempo) {
    var horas = Math.floor(tiempo / 3600);
    var minutos = Math.floor((tiempo % 3600) / 60);
    var segundos = tiempo % 60;


    horas = horas < 10 ? "0" + horas : horas; //Anteponiendo un 0 a las horas si son menos de 10 
    minutos = minutos < 10 ? "0" + minutos : minutos; //Anteponiendo un 0 a los minutos si son menos de 10 
    segundos = segundos < 10 ? "0" + segundos : segundos; //Anteponiendo un 0 a los segundos si son menos de 10 

    var t = horas + ":" + minutos + ":" + segundos;

    return t;
}


/**
 * Comprueba si el idproyecto y el idtarea obtenidos del formulario son de verdad del propio usuario
 * En caso de que sean correctos, se guarda la sesión en la BD
 */
function guardar_sesion_tarea() {

    $("#info").html("<img class='center-block' src='application/public/img/cargando.gif' width='20' height='20' style='display:inline-block;'> <span  style='display:inline-block;'>Guardando...</span>");
    var url = "application/public/php/guardar_sesion_tarea.php";
    var idproyecto = $("#proyecto").val();
    var idtarea = $("#tarea").val();
    var duracion = "00:00:00";


    // Si existe el campo duracion_sesion porque estamos en la pantalla de agregar registro manualmente
    // Transformamos duracion en segs (minutos*60) en formato 00:00:00
    // Si no, estamos en tracking automatico y lo tenemos que coger del html de la lista ul
    if ($("#duracion_sesion") && !isNaN($("#duracion_sesion").val()) && $("#duracion_sesion").val() > 0) {
        if ($("#error_duracion_isNaN"))
            $("#error_duracion_isNaN").remove();
        tiempo = $("#duracion_sesion").val() * 60; // Variable global tiempo
        duracion = transformar_tiempo(tiempo);
    } else if ($("#duracion_sesion").val() != null && isNaN($("#duracion_sesion"))) {
        $("#info").html("");
        $("#duracion_sesion").after("<div id='error_duracion_isNaN' class='alert alert-danger'>Introduce un n&uacute;mero entero</div>");
        return false;
    } else
        duracion = $("ul > li:last-child() .tiempo").html();

    var fecha = obtFechaHoraActual();
    var precio_por_hora = $("#precio_por_hora").val();
    var comentarios = $("#comentarios").val();

    // console.log("URL: " + url);
    // console.log("FECHA: " + fecha);
    // console.log("IDPROYECTO: " + idproyecto);
    // console.log("IDTAREA: " + idtarea);
    // console.log("DURACIÓN: " + duracion);
    // console.log("PRECIO/HORA: " + precio_por_hora);


    // var posting = $.post(url,$( "#form" ).serialize(),"json");

    var posting = $.post(url, {
        idtarea: idtarea,
        idproyecto: idproyecto,
        duracion: duracion,
        fecha: fecha,
        precio_por_hora: precio_por_hora,
        comentarios: comentarios
    }, "json");

    posting.done(function (response) {
        if (response.exito)
            $("#info").html("<div class='alert alert-success marginT20' role='alert'>La sesi&oacute;n ha sido a&ntilde;adida con &eacute;xito</div>");
        else {
            /*
             * Si se ha producido un error al realizar la operación de guardado en la BD 
             * o porque la validación de los datos del formulario ha fallado
             */

            // Los datos del formulario han sido manipulados adrede cambiando el campo value de los campos proyecto y tarea
            if (response.error_manipulacion_datos)
                $("#info").html("<div class='alert alert-danger marginT20' role='alert'>No se ha podido guardar la sesi&oacute;n.<br>El proyecto y/o la tarea no pertenece/n a este usuario o la tarea seleccionada no est&aacute; asignada a dicho proyecto</div>");
            else
                $("#info").html("<div class='alert alert-danger marginT20' role='alert'>La sesi&oacute;n no se ha podido guardar debido a un error interno</div>");
        }

        $("#info").fadeIn("slow");
    });
}

function mostrarGrafica(tipoGrafica) {
    /*  En la pagina Informes->ver.php cuando se pincha sobre el tipo de grafica a mostrar muestra
        el diagrama correspondiente 
        Gantt-> Muestra un diagrama Gantt con todas las sesiones realizadas de cada tarea en cada dia
        Sesiones-> Por cada tarea muestra las sesiones de forma lineal indicando el numero de horas de cada una
        Proporciones-> Diagrama con forma de "queso" donde se ven que tareas han llevado mas tiempo en el proyecto
    */
    if (tipoGrafica == "Gantt") {
        $("#diagrama-distribucion-sesiones").hide();
        $("#diagrama-proporciones").hide();
        $("#diagrama-gantt").fadeIn("normal");
        $("#panel-distribuciones li").removeClass("active");
        $("#panel-distribuciones li a").removeClass("active");
        $("#panel-distribuciones li:first-child()").addClass("active");
        $("#panel-distribuciones li:first-child() a").addClass("active");
    } else if (tipoGrafica == "Sesiones") {
        $("#diagrama-gantt").hide();
        $("#diagrama-proporciones").hide();
        $("#diagrama-distribucion-sesiones").fadeIn("normal");
        $("#panel-distribuciones li").removeClass("active");
        $("#panel-distribuciones li a").removeClass("active");
        $("#panel-distribuciones li:nth-child(2)").addClass("active");
        $("#panel-distribuciones li:nth-child(2) a").addClass("active");
    } else {
        $("#diagrama-gantt").hide();
        $("#diagrama-distribucion-sesiones").hide();
        $("#diagrama-proporciones").fadeIn("normal");
        $("#panel-distribuciones li").removeClass("active");
        $("#panel-distribuciones li a").removeClass("active");
        $("#panel-distribuciones li:last-child").addClass("active");
        $("#panel-distribuciones li:last-child a").addClass("active");
    }

    $("html, body").stop().animate({
        scrollTop: $("#panel-distribuciones").offset().top
    }, 1200); // Nos desplazamos suavemente hasta la gráfica
}

function irA(url) {
    location.href = url;
}

function desasignarTarea(tarea) {
    document.getElementById('tarea_asignada').value = tarea;
}




/**
 * Lanza el formulario para eliminar el cliente seleccionado por el usuario
 */
function eliminarCliente() {
    if (confirm("¿Realmente quieres eliminar este cliente del sistema?")) {
        form.action = "../../../projecttracker/clientes";
        document.getElementById("op").value = "Eliminar";
        form.submit();
    }
}


// /**
//  * Lanza el formulario para deshabilitar la tarea seleccionada por el usuario
//  */
// function deshabilitarTarea()
// {
//     if (confirm("¿Realmente quieres deshabilitar la tarea?\nRecuerda que si deshabilitas la tarea luego no será asignable en ningún proyecto"))
//     {
//         form.action = "../../../projecttracker/tareas";
//         document.getElementById("op").value="Eliminar";
//         form.submit();
//     }
// }

function cambiarOperacion(operacion)
{
    $operacion = document.getElementById("operacion").value = operacion;
}


function validar_form_instalacion() {
    document.forms.form.submit();
    return 1;
}


/* GRAFICAS */

var MONTHS = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
];
var config = {
    type: 'line',
    data: {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        datasets: [{
            label: 'Horas trabajadas',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            data: [
                186,
                172,
                197,
                145,
                160,
                182,
                177,
                186,
                172,
                197,
                145,
                160,
                182,
                177
            ],
            fill: false,
        }, {
            label: 'Facturación',
            fill: false,
            backgroundColor: window.chartColors.orange,
            borderColor: window.chartColors.orange,
            data: [
                860,
                793,
                937,
                742,
                981,
                1120,
                854,
                860,
                793,
                937,
                742,
                981,
                1120,
                854
            ],
        }, {
            label: 'Media',
            fill: false,
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [
                866,
                883,
                887,
                789,
                845,
                1092,
                897,
                866,
                883,
                887,
                789,
                845,
                1092,
                897
            ],
        }]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'Gŕafico horas trabajadas / euros facturados'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Mes'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Valor'
                }
            }]
        }
    }
};



var randomScalingFactor = function () {
    return Math.round(Math.random() * 100);
};

var config2 = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.green,
                window.chartColors.blue,
            ],
            label: 'Dataset 1'
        }],
        labels: [
            'Prototipado',
            'Diseño',
            'Análisis',
            'Implementación',
            'Pruebas'
        ]
    },
    options: {
        responsive: true
    }
};


window.onload = function () {
    var ctx = document.getElementById('chart-line').getContext('2d');
    if (ctx != null)
        window.myLine = new Chart(ctx, config);

    var ctx2 = document.getElementById('chart-area').getContext('2d');
    if (ctx2 != null)
        window.myPie = new Chart(ctx2, config2);
};


function cerrarMensajeInfo(etiqueta)
{
    var elem = document.getElementsByClassName('mensaje-info');
    elem[0].style.display = 'none';
}


function actualizarFoto()
{
    var foto = document.getElementById('foto').value;
    /* var foto = document.querySelector("#foto").value); // Con querySelector de Javascript */
    var label_foto = document.getElementsByClassName('label_foto')[0].innerHTML = "Foto <img src='"+foto+"'>";
    var filePath=$('#foto').val();
    console.log(filePath)
}